 ![gmp_dbtb icon](gui/GPMdbtb_icon_h48.png)

GermanPowerMarket.database.toolbox (GPM.db.tb)
===============================================


Configuration
-------------
The GPM.db.tb is a collection of a SQLite-database and several tool for data processing and plotting different kinds of figures. Beside the SQLite-database the main functionality is served by python-scripts. The gpm_dbtb is developed and tested under python 3.8.5 and uses the following external packages (not part fo the python core):  

+ pandas
+ numpy
+ scipy
+ matplotlib

The primary data source is the [entsoe-Transparancy Plattform](https://transparency.entsoe.eu/).




Main files and folders
----------------------
1. **files**
    + gpm.db - Database file in SQLite format.
    
    + gpm_db_crt.py - python Module for creating the database scheme and save the statement to the /sql-folder

    + gpm_db_dat.py - python module containing several function for data handling (import csv to data base)

    + gpm_db_set.py - python script which is used for set up the data base including data import

    + gpm_tb_aux.py - python module with auxiliarry functions and variables used accross the wohle toolbox

    + gpm_tb_dia.py - python module with several plotting functions 

    + gpm_tb_gui.py - python module to serve a graphical user interface (gui) based in tkinter

    + README.md - This README file.

    + win_starter_GUI.bat - Windows batch file for starting the GUI directly.

2. **folders**
    + applications - contains applications and examples of the gpm_dbtb

    + csv - contains raw data as csv-files

    + gui - contain supplemental files for the GUI.

    + licence - contain the licence

    + png - default folder for storing results figures

    + sql - contain SQL-statements for the database scheme

    + xls - contains raw data as xls(x)-files


Copyright and licnece
---------------------

The gpm_dbtb is licencend under a Creative Commons Attribution 4.0 International Public License.
The lincence is part of this package and is referring to [creativecommons.org](https://creativecommons.org/licenses/by/4.0/legalcode).  

![CC-BY-4.0 icon](licence/CC_BY_h32.png) Martin Dotzauer, 2022  


Contact information
-------------------
The GPM.db.tb is published online in a [GitLab repository](https://gitlab.com/M.Dotzauer/gpm_dbtb) and the author can be reached via M.Dotzauer@posteo.eu.