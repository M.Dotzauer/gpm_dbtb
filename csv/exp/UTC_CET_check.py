import pandas as pd
import os
import datetime as dt
from matplotlib import pyplot as plt


loc_dir = os.path.dirname(__file__)



gpm_csv_path = os.path.join(loc_dir, 'GPM_sample22.csv')
web_csv_path = os.path.join(loc_dir, 'ENTSOE_2022.csv')




DF                  = pd.read_csv(gpm_csv_path, sep=';', index_col = 'dt_UTC')

DF_web              = pd.read_csv(web_csv_path)
DF_web['str_UTC']    = pd.to_datetime(DF_web['MTU (UTC)'].str.slice(0,16),
                                        format='%d.%m.%Y %H:%M')
DF_web['dt_UTC']    = DF_web['str_UTC'].dt.strftime('%Y-%m-%d %H:%M:%S')
DF_web.index        = DF_web['dt_UTC']

DF['entsoe']        = DF_web['Day-ahead Price [EUR/MWh]']
DF['price']         = pd.to_numeric(DF['price'], errors='coerce')
DF['entsoe']        = pd.to_numeric(DF['entsoe'], errors='coerce')

DF['delta']         = DF['entsoe'] - DF['price']

DF['delta'].plot()
plt.show()