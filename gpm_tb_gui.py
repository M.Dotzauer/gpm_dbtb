"""
GUI for the gpm_dbtb, to access selected fucntions via a window.
"""

#import global libraries
import datetime as dt
import tkinter as tk
from tkinter import ttk
from tkinter import *
import tkinter.font as tkFont
import sqlite3 as sq3
import os


#import local libraries
import gpm_tb_aux as aux
import gpm_tb_dia as dia
import gpm_db_dat as dat


#path to data base
db_path = os.path.join(aux.var_script_dir, aux.var_db_name)


#dir to png-subdir
png_dir = os.path.join(aux.var_script_dir, 'png')


#list of all gen types
gen_types   = list(aux.var_types_en2de.keys())

        
#grid_specs
col_Hnt1    = 0
col_Var1    = 1
col_Hnt2    = 2
col_Var2    = 3
col_Hnt3    = 4
col_Var3    = 5
col_Btns    = 6
col_num     = 7


#font styles
font_stl_0  = ("Yu Gothic UI", 12)
font_stl_1  = ("Yu Gothic UI", 10)
font_stl_1i = ("Yu Gothic UI", 10, "italic")
font_stl_1b = ("Yu Gothic UI", 10, "bold")
font_stl_2  = ("Yu Gothic UI", 9)


def vline(w:int, h:int, row):
    """
    Create a vertial line. 
    
    Parameters:
    ----------
    w:int
        width in pixel
    
    h:int
        heigth in pixel
    
    """
    
    canvas_width = w
    canvas_height = h
    cv1 = Canvas(root, width=canvas_width, height=canvas_height)
    cv1.grid(column = 0, row = row, columnspan=col_num)
    y = int(canvas_height / 2)
    cv1.create_line(0, y, canvas_width, y, fill="#476042")



def db_minmax_timestamp(db_path:str, table:str, minmax:str, attribute:str):
    """
    Query min or max datetime for a certain table. 
    
    Parameters:
    ----------
    db_path:str
        path to the database
    
    table:str
        name of the table
    
    minmax:str
        defining whatever minimum or maximum datetime is requested 
        'min'|'max'
    
    Returns:
    --------
    dt_CET:str
        String for a min / max date.
    
    """
    
    #create database connection and cursor
    con     = sq3.connect(db_path)
    db_cs   = con.cursor()
    
    #create string for the query
    _SQL    = """   SELECT  {mm}(dt_CET) 
                    FROM    {tb}
                    JOIN    T_ts ON {tb}.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE   {atr} IS NOT '-'
                    AND     {atr} IS NOT NULL
                    AND     {atr} IS NOT 'nan' 
                    """.format(mm=minmax, tb=table, atr=attribute)
    
    #execute the SQL query and fetch the date
    db_cs.execute(_SQL)
    qry     = db_cs.fetchone()
    dt_CET  = str(qry[0])
    
    return(dt_CET)



#status message for initialise tkinter
print( '------------------------\n Starting tkinter GUI... \n------------------------')

#initialise tkinter
root        = tk.Tk()

ico_path    = os.path.join(aux.var_script_dir, 'gui', 'GPMdbtb_icon_16.ico')

try:
    root.iconbitmap(ico_path)
except:
    print('EXEPTION: Start tkinter without icon')

root.title('GUI for the GPM.db.tb')
#root.geometry('800x400')



## header
dyn_row=0

# #header text within the root window
# root_lbl = tk.Label(root, text="GUI for using the GPM.db.tb to create diagramms and optional save them to png-files.", font=font_stl_0)
# root_lbl.grid(column = 0, row = dyn_row, columnspan=col_num)
# 
# #top line
# #inserting vertical line
# dyn_row+=1

# #-----------------------------------------------------------------------------------
# vline(1000, 10, row=dyn_row)

## radio buttons and checkboxes for global setting (language and save to png option)
dyn_row+=1

# labels for diagramm and radiobuttons and checkboxes
rbcb_lbl = tk.Label(root, text='global settings', font=font_stl_1b)
rbcb_lbl.grid(column = 0, row = dyn_row, rowspan=2, sticky='nesw')

rb1_lbl = tk.Label(root, text="language='en'", font=font_stl_1)
rb1_lbl.grid(column = 1, row = dyn_row)

rb2_lbl = tk.Label(root, text="language='de'", font=font_stl_1)
rb2_lbl.grid(column = 2, row = dyn_row)

cb1_lbl = tk.Label(root, text="export png", font=font_stl_1)
cb1_lbl.grid(column = 3, row = dyn_row)

cb2_lbl = tk.Label(root, text="show figure", font=font_stl_1)
cb2_lbl.grid(column = 4, row = dyn_row)

rb3_lbl = tk.Label(root, text="market='EPEX'", font=font_stl_1)
rb3_lbl.grid(column = 5, row = dyn_row)

rb3_lbl = tk.Label(root, text="market='EXAA'", font=font_stl_1)
rb3_lbl.grid(column = 6, row = dyn_row)

#radiobutton 1 for selection language = 'en'
dyn_row+=1

rb1         = StringVar()
rb1_1_ca_d  = tk.Radiobutton(root,variable=rb1, value='en').grid(column = 1, row = dyn_row)

#radiobutton 1 for selection language = 'de'
rb1_2_ca_c  = tk.Radiobutton(root,variable=rb1, value='de').grid(column = 2, row = dyn_row)
rb1.set('en')

#Checkbutton for optinal saving to png
cb1         = BooleanVar()
cb1_ca_c    = tk.Checkbutton(root,variable=cb1).grid(column = 3, row = dyn_row)
cb1.set(False)


#Checkbutton for optinal suppress showing the figure
cb2         = BooleanVar()
cb2_ca_c    = tk.Checkbutton(root,variable=cb2).grid(column = 4, row = dyn_row)
cb2.set(True)


#Checkbutton for chosing between different power markets (EPEX | EXAA)
rb3         = StringVar()
rb3_1_ca_d  = tk.Radiobutton(root,variable=rb3, value='EPEX').grid(column = 5, row = dyn_row)

#radiobutton 1 for selection language = 'de'
rb3_2_ca_c  = tk.Radiobutton(root,variable=rb3, value='EXAA').grid(column = 6, row = dyn_row)
rb3.set('EPEX')



#inserting vertical line
dyn_row+=1

#-----------------------------------------------------------------------------------
vline(1000, 10, row=dyn_row)



## +++ load data +++
dyn_row+=1

# label for load data section
latest_loads        = db_minmax_timestamp(db_path, 'T_loads', 'max', 'load')
oldest_loads      = db_minmax_timestamp(db_path, 'T_loads', 'min', 'load')

w1_lbl = tk.Label(root, text=' load data', bg='silver', font=font_stl_1b)
w1_lbl.grid(column = 0, row = dyn_row, sticky='nesw')

w2_lbl = tk.Label(root, text='data range from: ' + oldest_loads + ' until ' \
                    + latest_loads, bg='silver', font=font_stl_1, anchor='w')
w2_lbl.grid(column = 1, row = dyn_row, columnspan=col_num-1, sticky='nesw')



#----------------- 
#annual capacities
dyn_row+=1

#Button for caling from gpm_dbtb_dia.cap_annual()
def exc_cap_timeseries():
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_cap_timeseries.png')
    else:
        png_path = False
        
    dia.cap_timeseries(lang=rb1.get(), show=cb2.get(), save_png=png_path)
    print(f"execute - dia.cap_timeseries(lang='{rb1.get()}', save_png='{cb1.get()}')")

Btn_ca = tk.Button(root, text = 'annual capacities', command = exc_cap_timeseries, font=font_stl_1b)
Btn_ca.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#----------------- 
#annual generation
dyn_row+=1 
 
#Hints for Data Input for annual generation
root_lbl = tk.Label(root, text='year (YYYY)', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)

#Input for load diagramming (timeseries)
year_gen    = tk.StringVar()
In_year_gen = tk.Entry(root, width = 10, textvariable = year_gen)
In_year_gen.grid(column = col_Var3, row = dyn_row)

#Button for caling gpm_tb_dia.gen_annual()
def exc_annual_gen():
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_gensum_' + str(year_gen.get()) + '.png')
    else:
        png_path = False
        
    #calling dia.gen_annual
    dia.gen_annual(year_gen.get(), lang=rb1.get(), show=cb2.get(), save_png=png_path)
    

    print(  f"execute - dia.cap_annual('{year_gen.get()}', lang='{rb1.get()}', "
            f"show='{cb2.get()}', save_png='{cb1.get()}')" )
    
    
Btn_ga = tk.Button(root, text = 'annual generation', command = exc_annual_gen, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------
#resload_heatmap
dyn_row+=1 
 
#Hints for Data Input for vre 
root_lbl = tk.Label(root, text='year (YYYY)', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input year (year_vre) and threshhold for dark doldrums (thrld_dd) for vre heat map 
year_rl     = tk.StringVar()
Inp_year_rl = tk.Entry(root, width = 10, textvariable = year_rl)
Inp_year_rl.grid(column = col_Var3, row = dyn_row)


#Button for caling dia.resload_heatmap()
def exc_rl_heatmap():
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_resload_' + str(year_rl.get()) + '.png')
    else:
        png_path = False
        
    #calling dia.gen_annual
    dia.resload_heatmap(year_rl.get(), lang=rb1.get(), show=cb2.get(), save_png=png_path)
    
    print(  f"execute - dia.resload_heatmap('{year_rl.get()}', lang='{rb1.get()}', show='{cb2.get()}, "
            f"save_png='{cb1.get()}')" )
    

Btn_ga = tk.Button(root, text = 'resload heatmap', command = exc_rl_heatmap, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------
#gen_type_heatmap
dyn_row+=1 
 
#Hints for Data Input for generation types 
root_lbl = tk.Label(root, text='year (YYYY)', font=font_stl_1)
root_lbl.grid(column = col_Hnt2, row = dyn_row, sticky=E)

#Input year (year_vre) and threshhold for dark doldrums (thrld_dd) for vre heat map 
year_gt     = tk.StringVar()
Inp_year_gt = tk.Entry(root, width = 10, textvariable = year_gt)
Inp_year_gt.grid(column = col_Var2, row = dyn_row)

#Option Menue for all generation types
gen_type    = tk.StringVar()
Inp_gen_type= tk.OptionMenu(root, gen_type, *gen_types)
Inp_gen_type.config(font=font_stl_1)
Inp_gen_type.grid(column = col_Var3-1, columnspan=2, row = dyn_row, sticky="ew")
gen_type.set('select generation type')


#Button for caling gpm_tb_dia.gen_heatmap()
def exc_gt_heatmap():
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_gt_heatmap_' + gen_type.get() + str(year_rl.get()) + '.png')
    else:
        png_path = False
        
    #calling dia.gen_annual
    dia.gen_heatmap(year_gt.get(), gen_type.get(),lang=rb1.get(), show=cb2.get(), save_png=png_path)
    
    #print status message
    print(  f"execute - dia.gen_heatmap('{year_gt.get()}', '{gen_type.get()}', lang='{rb1.get()}' "
            f"show='{cb2.get()}', save_png='{cb1.get()}')" )
    

Btn_ga = tk.Button(root, text = 'gen. type heatmap', command = exc_gt_heatmap, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------------------------------------
#variable-renewbale-generation as heatmap plot
dyn_row+=1 
 
#Hints for Data Input for vre 
root_lbl = tk.Label(root, text='year (YYYY)', font=font_stl_1)
root_lbl.grid(column = col_Hnt2, row = dyn_row, sticky=E)

root_lbl = tk.Label(root, text='dark calm limit [%]', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input year (year_vre) and threshhold for dark doldrums (thrld_dd) for vre heat map 
year_vre        = tk.StringVar()
Inp_year_vre    = tk.Entry(root, width = 10, textvariable = year_vre)
Inp_year_vre.grid(column = col_Var2, row = dyn_row)

dc_thrshld      = tk.StringVar()
Inp_dc_thrshld  = tk.Entry(root, width = 10, textvariable = dc_thrshld)
Inp_dc_thrshld.grid(column = col_Var3, row = dyn_row)
dc_thrshld.set(5)

#Button for caling the gen_anulal funtion from entsoE_dat
def exc_vre_heatmap():
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_vrehm_' + str(year_gen.get()) + '.png')
    else:
        png_path = False
        
    #calling dia.gen_annual
    dia.vre_heatmap(year_vre.get(), dark_calm_threshold=int(dc_thrshld.get())/100, lang=rb1.get(), show=cb2.get(), save_png=png_path)
    
    #print status message
    print(  f"execute - dia.vre_heatmap('{year_vre.get()}', dark_calm_threshold={int(dc_thrshld.get())/100}, "
            f"lang='{rb1.get()}', show='{cb2.get()}', save_png='{cb1.get()}')" )
    

Btn_ga = tk.Button(root, text = 'vre heatmap', command = exc_vre_heatmap, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#-----------
# timeseries
dyn_row+=1 

#Hints for Data Input for timeseries
root_lbl = tk.Label(root, text='year (YYYY)', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)

#Input for interactive generation-timeseries diagramm 
year_gen_ts       = tk.StringVar()
Inp_year_gen_ts   = tk.Entry(root, width = 10, textvariable = year_gen_ts)
Inp_year_gen_ts.grid(column = col_Var3, row = dyn_row)


#Button for caling gpm_tb_dia.gen_timeseries()
def exc_generation_timeseries():
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_ts_' + str(Inp_year_gen_ts.get()) + '.png')
    else:
        png_path = False
        

    dia.generation_timeseries(int(Inp_year_gen_ts.get()), lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.gen_timeseries('{Inp_year_gen_ts.get()}', lang='{rb1.get()}', "
            f"show='{cb2.get()}', save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'gen. timeseries', command = exc_generation_timeseries, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------------------
# generation duration curves
dyn_row+=1

#Hints for Data Input for timeseries
root_lbl = tk.Label(root, text='year(s) YYYY, ...', font=font_stl_1)
root_lbl.grid(column = col_Hnt1, row = dyn_row, sticky=E)

root_lbl = tk.Label(root, text='type(s), ...', font=font_stl_1)
root_lbl.grid(column = col_Hnt2, row = dyn_row, sticky=E)

root_lbl = tk.Label(root, text='normalized values', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)

#Input for load diagramming (timeseries)
gen_years       = tk.StringVar()
Inp_gen_year    = tk.Entry(root, width = 10, textvariable = gen_years)
Inp_gen_year.grid(column = col_Var1, row = dyn_row)

gt_strlist      = tk.StringVar()
Inp_types       = tk.Entry(root, width = 10, textvariable = gt_strlist)
Inp_types.grid(column = col_Var2, row = dyn_row)


cb3         = BooleanVar()      #Checkbutton for optinal normalizing values
cb3_ca_c    = tk.Checkbutton(root,variable=cb3).grid(column = col_Var3, row = dyn_row)
cb3.set(False)


#Button for caling gmp_dia.gen_dur_curve()
def exc_gendurcur():
    years_list      = gen_years.get().replace(' ','').split(',')
    years_initials  = '-'.join(['' + i[2:] for i in years_list])
    types_list      = gt_strlist.get().replace(' ','').split(',')
    types_initials  = '-'.join(['' + i[:2] for i in types_list])
    
    #siwtch for declaring 'series' argument depending or length of years and types input
    if len(years_list) > len(types_list):
        series_arg  ='years'
    else:
        series_arg  ='types'
    
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_gdc_' +  types_initials + '_' + years_initials + '.png')
    else:
        png_path = False
        
    
    dia.gen_dur_curve(years_list, types_list, norm=cb3.get(),\
                    series=series_arg, lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.gen_dur_curve('{years_list}', '{types_list}', norm='{cb3.get()}', "
            f"series='{series_arg }', lang='{rb1.get()}', show='{cb2.get()}', save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'gen. dur. curves', command = exc_gendurcur, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



## price data

#inserting vertical line
dyn_row+=1

#-----------------------------------------------------------------------------------
vline(1000, 10, row=dyn_row)


dyn_row+=1

#todo: add latest prices for EXAA
# label for price data section
latest_epex     = db_minmax_timestamp(db_path, 'T_epex_da', 'max', 'price')
oldest_epex     = db_minmax_timestamp(db_path, 'T_epex_da', 'min', 'price')

latest_exaa     = db_minmax_timestamp(db_path, 'T_exaa_da', 'max', 'price')
oldest_exaa     = db_minmax_timestamp(db_path, 'T_exaa_da', 'min', 'price')


w1_lbl          = tk.Label(root, text='price data', bg='silver', font=font_stl_1b)
w1_lbl.grid(column = 0, row = dyn_row, sticky='nesw')

w2_lbl_txt      =   f"data ranges @EPEX: {oldest_epex} - {latest_epex}  &  " \
                    f" @EXAA: {oldest_exaa} - {latest_exaa}"
w2_lbl          = tk.Label(root, text=w2_lbl_txt, bg='silver', font=font_stl_1, anchor='w')
w2_lbl.grid(column = 1, row = dyn_row, columnspan=col_num-1, sticky='nesw')




#---------------------------
# annual price statistics (aps)
dyn_row+=1

#Button for caling gpm_tb_dia.anual_pricestats()
def exc_aps():
    
    
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_annual_stats_{rb3.get()}.png')
    else:
        png_path = False
        
    #calling dia.daily_pricestats
    dia.annual_pricestats(lang=rb1.get(), market=rb3.get(), show=cb2.get(), save_png=png_path)

    #print status message
    print( f"execute - dia.annual_pricestats(lang='{rb1.get()}', market='{rb3.get()}'"
            f", show= '{cb2.get()}', save_png='{cb1.get()}')" )

Btn_ga = tk.Button(root, text = 'annual price stats', command = exc_aps, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')




#---------------------------
# daily price statistics (dps)
dyn_row+=1


#Hints for Data Input for dps
root_lbl = tk.Label(root, text='YYYY', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input for dps
dps_year        = tk.StringVar()
Inp_dps_year    = tk.Entry(root, width = 10, textvariable = dps_year)
Inp_dps_year.grid(column = col_Var3, row = dyn_row)


#Button for caling gpm_tb_dia.daily_pricestats()
def exc_dps():
      
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_daily_stats_{rb3.get()}_{dps_year.get()}.png')
    else:
        png_path = False
        
    dia.daily_pricestats(dps_year.get(), market=rb3.get(), lang=rb1.get(),  
                            show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.daily_pricestats('{dps_year.get()}', market='{rb3.get()}', lang='{rb1.get()}', "
            f"show='{cb2.get()}', save_png='{cb1.get()}' " )


Btn_ga = tk.Button(root, text = 'daily price stats', command = exc_dps, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------------------
# annual price time series (pts)
dyn_row+=1


#Hints for Data Input for dps
root_lbl = tk.Label(root, text='year YYYY', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input for dps
pts_year        = tk.StringVar()
Inp_pts_year    = tk.Entry(root, width = 10, textvariable = pts_year)
Inp_pts_year.grid(column = col_Var3, row = dyn_row)



#Button for caling gpm_tb_dia.price_timeseries()
def exc_pts():
      
    #create list of years from the input string  and a list of initials (subtring for filename)
    year_int        = int(pts_year.get())

    
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_price_timeseries_{rb3.get()}_{year}.png')
    else:
        png_path = False
        
    
    dia.price_timeseries(year_int, market=rb3.get(), lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.price_timeseries('{pts_year.get()}', market='{rb3.get()}', lang='{rb1.get()}',"
            f"show='{cb2.get()}', save_png='{cb1.get()}') ")


Btn_pts = tk.Button(root, text = 'price time series', command = exc_pts, font=font_stl_1b)
Btn_pts.grid(column = col_Btns, row = dyn_row, sticky='nesw')




#---------------------------
# annual price averages (aa)
dyn_row+=1


#Hints for Data Input for dps
root_lbl = tk.Label(root, text='single year YYYY', font=font_stl_1)
root_lbl.grid(column = col_Hnt1, row = dyn_row, sticky=E)

root_lbl = tk.Label(root, text='specs: h, d, w, m, m+, y, y+', font=font_stl_1)
root_lbl.grid(column = col_Hnt2, row = dyn_row, sticky=E)

root_lbl = tk.Label(root, text='benchmark [€/MWh]', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input for dps
pa_year         = tk.StringVar()
Inp_pa_year     = tk.Entry(root, width = 10, textvariable = pa_year)
Inp_pa_year.grid(column = col_Var1, row = dyn_row)

pa_specs        = tk.StringVar()
Inp_pa_specs    = tk.Entry(root, width = 10, textvariable = pa_specs)
Inp_pa_specs.grid(column = col_Var2, row = dyn_row)
pa_specs.set('m, y')

pa_benchmark    = tk.StringVar()
Inp_pa_bm       = tk.Entry(root, width = 10, textvariable = pa_benchmark)
Inp_pa_bm.grid(column = col_Var3, row = dyn_row)


#Button for caling gpm_tb_dia.price_averages()
def exc_pa():
      
    #create list of years from the input string  and a list of initials (subtring for filename)
    year_int        = int(pa_year.get())
    specifiers      = pa_specs.get().replace(' ','').split(',')
    if pa_benchmark.get() != '':
        bm_float        = float(pa_benchmark.get())
    else:
        bm_float        = None
    
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_annual_averages_{rb3.get()}_{years_initials }_png')
    else:
        png_path = False
        
    
    dia.price_averages(year_int, specifiers, benchmark=bm_float, market=rb3.get(), 
                        lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.price_averages('{pa_year.get()}_, lang='{rb1.get()}, market='{rb3.get()}', "
            f"show='{cb2.get()}, save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'price averages', command = exc_pa, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')




#---------------------------
# annual price volatilities (pv)
dyn_row+=1


#Hints for Data Input for dps
root_lbl = tk.Label(root, text='year(s) YYYY', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input for dps
pv_years        = tk.StringVar()
Inp_pv_years    = tk.Entry(root, width = 10, textvariable = pv_years)
Inp_pv_years.grid(column = col_Var3, row = dyn_row)


#Button for caling gpm_tb_dia.price_volatility()
def exc_pv():
      
    #create list of years from the input string  and a list of initials (subtring for filename)
    years_list      = pv_years.get().replace(' ','').split(',')
    years_initials  = '-'.join(['' + i[2:] for i in years_list])
      
    
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_annual_volatilities_{rb3.get()}_{years_initials}_.png')
    else:
        png_path = False
        

    dia.price_volatility(years_list, market=rb3.get(), lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia. price_volatility('{pv_years.get()}', market='{rb3.get()}', lang='{rb1.get()}, "
            f"show='{cb2.get()}', save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'annual volatilities', command = exc_pv, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------------------
# annual price spreads (ps)
dyn_row+=1


root_lbl = tk.Label(root, text='year(s) YYYY', font=font_stl_1)
root_lbl.grid(column = col_Hnt2, row = dyn_row, sticky=E)

root_lbl = tk.Label(root, text='reference (d | w | m | y)', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)

#Input for dps
ps_years        = tk.StringVar()
Inp_ps_years    = tk.Entry(root, width = 10, textvariable = ps_years)
Inp_ps_years.grid(column = col_Var2, row = dyn_row)

ps_ref          = tk.StringVar()
Inp_ps_ref      = tk.Entry(root, width = 10, textvariable = ps_ref)
Inp_ps_ref.grid(column = col_Var3, row = dyn_row)
ps_ref.set('m')

#Button for caling gpm_tb_dia.price_spreads()
def exc_ps():
      
    #create list of years from the input string  and a list of initials (subtring for filename)
    years_list      = ps_years.get().replace(' ','').split(',')
    years_initials  = '-'.join(['' + i[2:] for i in years_list])
      
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_annual_spreads_{rb3.get()}_{years_initials}.png')
    else:
        png_path = False
        
        
    dia.price_spreads(years_list, ps_ref.get(), market=rb3.get(), lang=rb1.get(), show=cb2.get(),
                        save_png=png_path)

    print(  f"execute - dia.price_spreads({years_list}, '{ps_ref.get()}', lang='{rb1.get()}'," 
            + f"show={cb2.get()}, save_png={png_path}")


Btn_ga = tk.Button(root, text = 'annual spreads', command = exc_ps, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------------------
# daily cherry picking (cp)
dyn_row+=1


#Hints for Data Input for dps
root_lbl = tk.Label(root, text='year(s) YYYY', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Option Menu for all time period for averages
cp_which      = tk.StringVar()
Inp_cp_which  = tk.OptionMenu(root, cp_which, *['both', 'expensive', 'cheap'])
Inp_cp_which.config(font=font_stl_1)
Inp_cp_which.grid(column = col_Var2-1, columnspan=2, row = dyn_row, sticky="ew")
cp_which.set('select sort of cherries')


#Input for dps
cp_years        = tk.StringVar()
Inp_cp_years    = tk.Entry(root, width = 10, textvariable = cp_years)
Inp_cp_years.grid(column = col_Var3, row = dyn_row)


#Button for caling gpm_tb_dia.cherry_picking()
def exc_cp():
      
    #create list of years from the input string  and a list of initials (subtring for filename)
    years_list      = cp_years.get().replace(' ','').split(',')
    years_initials  = '-'.join(['' + i[2:] for i in years_list])
      
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_cherry_picking_{rb3.get()}_{years_initials}.png')
    else:
        png_path = False
        
    
    dia.cherry_picking(years_list, which=cp_which.get(), market=rb3.get(), lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.cherry_picking('{ps_years.get()}', market='{rb3.get()}', lang='{rb1.get()}, "
            f"show='{cb2.get()}', save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'cherry picking', command = exc_cp, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')




#---------------------------
# price heat maps (phm)
dyn_row+=1


#Hints for Data Input for dps
root_lbl = tk.Label(root, text='year YYYY', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input for dps
phm_year        = tk.StringVar()
Inp_phm_year    = tk.Entry(root, width = 10, textvariable = phm_year)
Inp_phm_year.grid(column = col_Var3, row = dyn_row)


#Button for caling gpm_tb_dia.price_heatmap()
def exc_phm():

    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_price_heatmap_{rb3.get()}_{phm_year.get()}.png')
    else:
        png_path = False
        
    
    dia.price_heatmap(int(phm_year.get()), market=rb3.get(), lang=rb1.get(), 
                        show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.price_heatmap('{phm_year.get()}', market='{rb3.get()}', lang='{rb1.get()}', "
            f"show='{cb2.get()}', save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'price heatmap', command = exc_phm, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------------------
# price duration curves (pdc)
dyn_row+=1


#Hints for Data Input for pdc
root_lbl = tk.Label(root, text='year(s) YYYY', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input for dps
pdc_years       = tk.StringVar()
Inp_pdc_years   = tk.Entry(root, width = 10, textvariable = pdc_years)
Inp_pdc_years.grid(column = col_Var3, row = dyn_row)


#Button for caling gpm_tb_dia.price_dur_curves()
def exc_pdc():

    #create list of years from the input string  and a list of initials (subtring for filename)
    years_list      = pdc_years.get().replace(' ','').split(',')
    years_initials  = '-'.join(['' + i[2:] for i in years_list])

    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_price_dur_curve_{rb3.get()}_{years_initials}.png')
    else:
        png_path = False
        
            
    dia.price_dur_curve(years_list, market=rb3.get(), lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.price_dur_curve('{pdc_years.get()}', market='{rb3.get()}', lang='{rb1.get()}, "
            f"', show='{cb2.get()}, save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'price dur. curve', command = exc_pdc, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')




#---------------------------
# ets prices (ets)
dyn_row+=1


#Hints for Data Input for ets
root_lbl = tk.Label(root, text='year YYYY (or "all")', font=font_stl_1)
root_lbl.grid(column = col_Hnt1, row = dyn_row, sticky=E)


#Hints for checkbutton of epex-data ad-on
root_lbl = tk.Label(root, text='add day-ahead prices:', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)


#Input for ets
ets_year        = tk.StringVar()
Inp_ets_year    = tk.Entry(root, width = 10, textvariable = ets_year)
Inp_ets_year.grid(column = col_Var1, row = dyn_row)


#Option Menu for all time period for averages
ets_avg      = tk.StringVar()
Inp_ets_avg  = tk.OptionMenu(root, ets_avg, *['week', 'month', 'year'])
Inp_ets_avg.config(font=font_stl_1)
Inp_ets_avg.grid(column = col_Var2-1, columnspan=2, row = dyn_row, sticky="ew")
ets_avg.set('select average period')


#Checkbutton for optinal adding EPEX-prices
cb5         = BooleanVar()      
cb5_ca_c    = tk.Checkbutton(root,variable=cb5).grid(column = col_Var3, row = dyn_row)
cb5.set(False)


#Button for caling gpm_tb_dia.ets_prices()
def exc_ets():
    
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_ets_prices_' +  ets_year.get() + '.png')
    else:
        png_path = False
    
    
    dia.ets_prices(ets_year.get(), avg=ets_avg.get(), epex=cb5.get(), lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print("execute - dia.('" + str(ets_year.get()) + "', lang='" + rb1.get() + \
            "', show=" + str(cb2.get()) +  ", save_png=" + str(cb1.get()) + ")")


Btn_ga = tk.Button(root, text = 'ETS prices', command = exc_ets, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')


## mixed data

#inserting vertical line
dyn_row+=1

#-----------------------------------------------------------------------------------
vline(1000, 10, row=dyn_row)


dyn_row+=1

#calculate the latest mix data availible
dto_lp     = dt.datetime.strptime(latest_epex, '%Y-%m-%d %H:%M:%S')
dto_ll     = dt.datetime.strptime(latest_loads, '%Y-%m-%d %H:%M:%S')

if dto_lp > dto_ll:
    latest_mix = latest_loads
else:
    latest_mix = latest_epex

#calculate the oldest mix data availible
dto_ep     = dt.datetime.strptime(oldest_epex, '%Y-%m-%d %H:%M:%S')
dto_el     = dt.datetime.strptime(oldest_loads, '%Y-%m-%d %H:%M:%S')

if dto_ep < dto_el:
    oldest_mix = oldest_loads
else:
    oldest_mix = oldest_epex

# label for mixed data section
w1_lbl          = tk.Label(root, text='mixed data', bg='silver', font=font_stl_1b)
w1_lbl.grid(column = 0, row = dyn_row, sticky='nesw')

w2_lbl          = tk.Label(root, text=' data range from: ' + oldest_mix + \
                            ' until ' + latest_mix, bg='silver', font=font_stl_1, anchor='w')
w2_lbl.grid(column = 1, row = dyn_row, columnspan=col_num-1, sticky='nesw')



#---------------------------
# price versus emissions (me)
dyn_row+=1


#Hints for Data Input for pem
root_lbl = tk.Label(root, text='year (YYYY)', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)

#Input for pem
year_pem         = tk.StringVar()
Inp_year_pem    = tk.Entry(root, width = 10, textvariable = year_pem)
Inp_year_pem.grid(column = col_Var3, row = dyn_row)


#Button for caling gpm_tb_dia.price_emissions()
def exc_pem():
    
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, 'GUIout_price_emissions_' +  year_pem.get() + '.png')
    else:
        png_path = False


    dia.price_vs_emissions(int(year_pem.get()), market=rb3.get(), lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.price_emissions('{year_pem.get()}', market='{rb3.get()}',  lang='{rb1.get()}, "
            f"show='{cb2.get()}, save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'price vs. emisions', command = exc_pem, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#---------------------------
# price response function (prf)
dyn_row+=1


#Hints for Data Input for pel
root_lbl = tk.Label(root, text='year (YYYY)', font=font_stl_1)
root_lbl.grid(column = col_Hnt1, row = dyn_row, sticky=E)

root_lbl = tk.Label(root, text='curve fitting', font=font_stl_1)
root_lbl.grid(column = col_Hnt3, row = dyn_row, sticky=E)

#Input for pel
year_pel        = tk.StringVar()
Inp_year_pel    = tk.Entry(root, width = 10, textvariable = year_pel)
Inp_year_pel.grid(column = col_Var1, row = dyn_row)


#Option Menu for all generation types
gt_pel       = tk.StringVar()
Inp_gt_pel   = tk.OptionMenu(root, gt_pel, *gen_types)
Inp_gt_pel.config(font=font_stl_1)
Inp_gt_pel.grid(column = col_Var2-1, columnspan=2, row = dyn_row, sticky="ew")
gt_pel.set('select generation type')


#Checkbutton for optinal calculating platting of a fitted curve
cb4         = BooleanVar()      
cb4_ca_c    = tk.Checkbutton(root,variable=cb4).grid(column = col_Var3, row = dyn_row)
cb4.set(False)


#Button for caling gpm_tb_dia.price_response()
def exc_prf():
    
    if cb4.get()==True:
        substr_curve='_fit'
    else:
        substr_curve=''
    
    #create png target path if checkbox 1 is activated
    if cb1.get()==True:
        png_path = os.path.join(png_dir, f'GUIout_price_response_{rb3.get()}_{year_pel.get()}_{gt_pel.get()}_{substr_curve}.png')
    else:
        png_path = False
    
        
    dia.price_response(int(year_pel.get()), gt_pel.get(), curve=cb4.get(), market=rb3.get(), 
                        lang=rb1.get(), show=cb2.get(), save_png=png_path)

    print(  f"execute - dia.price_response('{year_pel.get()}', '{gt_pel.get()}', curve='{cb4.get()}', "
            f"market='{rb3.get()}', lang='{rb1.get()}', show='{cb2.get()}', save_png='{cb1.get()}')" )


Btn_ga = tk.Button(root, text = 'price response', command = exc_prf, font=font_stl_1b)
Btn_ga.grid(column = col_Btns, row = dyn_row, sticky='nesw')



#inserting vertical line
dyn_row+=1

#-----------------------------------------------------------------------------------
vline(1000, 10, row=dyn_row)

## footer
dyn_row+=1

#insert gpm_tb icon
tb_image    = PhotoImage(file = 'gui/GPMdbtb_icon_h48.png')
label       = Label(root, image = tb_image) 
label.grid(row = dyn_row, column = 0, sticky='W') 


#footer text within the root window
root_lbl    = tk.Label(root, text="GermanPowerMarket.database.toolbox, Version 1.0.4, "  + u"\u00A9" + "Martin Dotzauer 2024 \n Source Code availible under: https://gitlab.com/M.Dotzauer/gpm_dbtb \nThis work is licensed under a Creative Commons Attribution 4.0 International License.", font=font_stl_2)
root_lbl.grid(column = 0, row = dyn_row, columnspan=col_num)


#insert CC-BY icon
cc_image    = PhotoImage(file = 'licence/CC_BY_h48.png')
label       = Label(root, image = cc_image) 
label.grid(row = dyn_row, column = col_num-1, sticky='E') 

root.mainloop()