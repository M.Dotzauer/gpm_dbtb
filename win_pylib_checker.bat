:: bat-file for automatically start the gui of the gpm_toolbox

title 'PFT2 bat-script: Check if needed modules (pandas, matplotlib, numpy) are installed.'

cd %cd%

set local_dir=%cd%\py
::echo %local_dir%

::python -c "print('%local_dir%')"

python -c "import sys; sys.path.append('%local_dir%'); import gpm_pl_cai; gpm_pl_cai.pylibs_check_and_install()"

pause