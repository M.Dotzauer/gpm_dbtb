BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "T_epex_da" (
	"PK_ts_UTC"	INTEGER,
	"price"	REAL,
	"mrgnl_em"	REAL,
	"dt_update"	TEXT,
	UNIQUE("PK_ts_UTC"),
	FOREIGN KEY("PK_ts_UTC") REFERENCES "T_ts"("PK_ts_UTC"),
	PRIMARY KEY("PK_ts_UTC")
);
CREATE TABLE IF NOT EXISTS "T_gen" (
	"PK_ts_UTC"	INTEGER,
	"nuclear"	REAL,
	"lignite"	REAL,
	"hard_coal"	REAL,
	"nat_gas"	REAL,
	"oil"	REAL,
	"other_fossil"	REAL,
	"waste"	REAL,
	"biomass"	REAL,
	"geothermal"	REAL,
	"hydro_river"	REAL,
	"hydro_reservoir"	REAL,
	"pv"	REAL,
	"wind_offshore"	REAL,
	"wind_onshore"	REAL,
	"other_renewable"	REAL,
	"hydro_storage_gen"	REAL,
	"hydro_storage_con"	REAL,
	"carbon_mix"	REAL,
	"dt_update"	TEXT,
	PRIMARY KEY("PK_ts_UTC"),
	FOREIGN KEY("PK_ts_UTC") REFERENCES "T_ts"("PK_ts_UTC"),
	UNIQUE("PK_ts_UTC")
);
CREATE TABLE IF NOT EXISTS "T_gen_tps" (
	"ID"	INTEGER,
	"name_db"	TEXT,
	"name_de"	TEXT,
	"name_en"	TEXT,
	"emissions_fuel"	REAL,
	"emissions_full"	REAL
);
CREATE TABLE IF NOT EXISTS "T_caps" (
	"year"	INTEGER,
	"nuclear"	REAL,
	"lignite"	REAL,
	"hard_coal"	REAL,
	"nat_gas"	REAL,
	"oil"	REAL,
	"other_fossil"	REAL,
	"waste"	REAL,
	"biomass"	REAL,
	"geothermal"	REAL,
	"hydro_river"	REAL,
	"hydro_reservoir"	REAL,
	"pv"	REAL,
	"wind_offshore"	REAL,
	"wind_onshore"	REAL,
	"other_renewable"	REAL,
	"hydro_storage_gen"	REAL,
	"dt_update"	TEXT,
	PRIMARY KEY("year")
);
CREATE TABLE IF NOT EXISTS "T_loads" (
	"PK_ts_UTC"	INTEGER,
	"load"	REAL,
	"res_load"	REAL,
	"dt_update"	TEXT,
	FOREIGN KEY("PK_ts_UTC") REFERENCES "T_ts"("PK_ts_UTC"),
	PRIMARY KEY("PK_ts_UTC"),
	UNIQUE("PK_ts_UTC")
);
CREATE TABLE IF NOT EXISTS "T_stats_da" (
	"PK_ts_UTC"	INTEGER,
	"day_min"	REAL,
	"day_mean"	REAL,
	"day_max"	REAL,
	"spread_bp"	REAL,
	"spread_b18"	REAL,
	"spread_b12"	REAL,
	"spread_b6"	REAL,
	"neg_count"	INTEGER,
	"neg_cum"	REAL,
	PRIMARY KEY("PK_ts_UTC"),
	UNIQUE("PK_ts_UTC"),
	FOREIGN KEY("PK_ts_UTC") REFERENCES "T_ts"("PK_ts_UTC")
);
CREATE TABLE IF NOT EXISTS "T_wdays" (
	"PK_wday"	INTEGER,
	"weekday_de"	TEXT,
	"weekday_en"	TEXT,
	PRIMARY KEY("PK_wday")
);
CREATE TABLE IF NOT EXISTS "T_seasons" (
	"PK_season"	INTEGER,
	"season_de"	TEXT,
	"seasons_en"	TEXT,
	PRIMARY KEY("PK_season")
);
CREATE TABLE IF NOT EXISTS "T_months" (
	"PK_month"	INTEGER,
	"FK_season"	INTEGER,
	"month_de"	TEXT,
	"month_en"	TEXT,
	FOREIGN KEY("FK_season") REFERENCES "T_seasons"("PK_season"),
	PRIMARY KEY("PK_month")
);
CREATE TABLE IF NOT EXISTS "T_ts" (
	"PK_ts_UTC"	INTEGER,
	"dt_UTC"	TEXT,
	"dt_CET"	TEXT,
	"year"	INTEGER,
	"FK_month"	INTEGER,
	"FK_season"	INTEGER,
	"day"	INTEGER,
	"FK_wday"	INTEGER,
	"hour"	INTEGER,
	"minute"	INTEGER,
	PRIMARY KEY("PK_ts_UTC"),
	FOREIGN KEY("FK_wday") REFERENCES "T_wdays"("PK_wday"),
	FOREIGN KEY("FK_month") REFERENCES "T_months"("PK_month"),
	FOREIGN KEY("FK_season") REFERENCES "T_seasons"("PK_season"),
	UNIQUE("PK_ts_UTC")
);
CREATE UNIQUE INDEX IF NOT EXISTS "idx_T_ts_PK_ts_UTC" ON "T_ts" (
	"PK_ts_UTC"	ASC
);
CREATE TRIGGER TRG_epex_ts
    BEFORE INSERT ON T_epex_da
    BEGIN
        INSERT OR IGNORE INTO T_ts (PK_ts_UTC, dt_UTC, dt_CET, year, FK_season, FK_month, day, FK_wday, hour, minute)
        VALUES(	NEW.PK_ts_UTC,
                datetime(NEW.PK_ts_UTC,'unixepoch'),
                datetime(NEW.PK_ts_UTC,'unixepoch','localtime'),
                strftime('%Y', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                (SELECT FK_season FROM T_months WHERE PK_month=(SELECT strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')))),
                strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%d', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%w', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime'))+1,
                strftime('%H', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%M', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')));
    END;
CREATE TRIGGER TRG_load_ts
    BEFORE INSERT ON T_loads
    BEGIN
        INSERT OR IGNORE INTO T_ts (PK_ts_UTC, dt_UTC, dt_CET, year, FK_season, FK_month, day, FK_wday, hour, minute)
        VALUES(	NEW.PK_ts_UTC,
                datetime(NEW.PK_ts_UTC,'unixepoch'),
                datetime(NEW.PK_ts_UTC,'unixepoch','localtime'),
                strftime('%Y', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                (SELECT FK_season FROM T_months WHERE PK_month=(SELECT strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')))),
                strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%d', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%w', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime'))+1,
                strftime('%H', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%M', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')));
    END;
CREATE TRIGGER TRG_gen_ts
    BEFORE INSERT ON T_gen
    BEGIN
        INSERT OR IGNORE INTO T_ts (PK_ts_UTC, dt_UTC, dt_CET, year, FK_season, FK_month, day, FK_wday, hour, minute)
        VALUES(	NEW.PK_ts_UTC,
                datetime(NEW.PK_ts_UTC,'unixepoch'),
                datetime(NEW.PK_ts_UTC,'unixepoch','localtime'),
                strftime('%Y', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                (SELECT FK_season FROM T_months WHERE PK_month=(SELECT strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')))),
                strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%d', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%w', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime'))+1,
                strftime('%H', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%M', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')));
    END;
CREATE VIEW v_price
        AS
        SELECT price, T_epex_da.PK_ts_UTC, dt_UTC, dt_CET
        FROM T_epex_da
        JOIN T_ts ON T_epex_da.PK_ts_UTC=T_ts.PK_ts_UTC;
CREATE VIEW v_loads
        AS
        SELECT load, T_loads.PK_ts_UTC, dt_UTC, dt_CET
        FROM T_loads
        JOIN T_ts ON T_loads.PK_ts_UTC=T_ts.PK_ts_UTC;
CREATE VIEW v_gen
        AS
        SELECT T_gen.PK_ts_UTC, dt_UTC, dt_CET, 
				nuclear, lignite, hard_coal, 
				nat_gas, oil, other_fossil, 
				waste, biomass, geothermal, 
				other_renewable, hydro_river, 
				hydro_reservoir, wind_offshore, 
				wind_onshore, pv, hydro_storage_gen, 
				hydro_storage_con
        FROM T_gen
        JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC;
COMMIT;
