#!/usr/bin/env python
"""
This module is a collection of auxilliary functions shared with other entsoE_xxx.py
modules desinged for the entsoE.db (SQLite db).
"""

import pandas 		as 	pd
import numpy 		as 	np
import 					os
import 					sys



## fixed_variables
#dir to that script
var_script_dir = os.path.dirname(__file__)



#fixed database name
var_db_name = 'gpm.db'



#Dictionary for asign season to month
var_season = {1:4,2:4,3:1,4:1,5:1,6:2,7:2,8:2,9:3,10:3,11:3,12:4}



#define target columns for the tables T_epex_da, T_loads, T_gens
var_tc 	= {	'T_epex_da':[['PK_ts_UTC',0], ['price',1]],
			'T_exaa_da':[['PK_ts_UTC',0], ['price',1]],
			'T_loads':[	['PK_ts_UTC',0], ['load',2]],
			'T_gen':[	['PK_ts_UTC',			1], #ts is in csv [1] but ts in df [0]
						['nuclear',				16],
						['lignite', 				3],
						['hard_coal', 			6],
						['nat_gas', 				5],
						['oil', 					7],
						['other_fossil', 		17],
						['waste', 				20],
						['biomass', 				2],
						['geothermal',			10],
						['hydro_river',			13],
						['hydro_reservoir',		14],
						['pv', 					19],
						['wind_offshore', 		21],
						['wind_onshore', 		22],
						['other_renewable', 		18],
						['hydro_storage_gen', 	11],
						['hydro_storage_con', 	12]]}



#translations for generattion types 'en' : 'de'
var_types_en2de = { 'nuclear' 			: 'Kernkraft',
					'lignite' 			: 'Braunkohle',
					'hard_coal' 			: 'Steinkohle',
					'nat_gas' 			: 'Erdgas',
					'oil' 				: 'Öl',
					'other_fossil' 		: 'Andere Fossile',
					'waste' 				: 'Abfall',
					'biomass' 			: 'Biomasse',
					'geothermal' 		: 'Geothermie',
					'other_renewable' 	: 'Andere Erneuerb.',
					'hydro_river' 		: 'Laufwassser',
					'hydro_reservoir' 	: 'Speicherkraftwerk',
					'wind_offshore'		: 'Wind off-shore',
					'wind_onshore' 		: 'Wind on-shore',
					'pv' 				: 'PV',
					'hydro_storage_gen' : 'Pumpspeicherkraftwer (verbr.)',
					'hydro_storage_con' : 'Pumpspeichekraftwerk (erz.)',
					'load' 				: 'Last',
					'residual load' 		: 'Residuallast',
					'non-renewable dispatchable generation' 	: 'nicht-erneuerbare steuerbare Erzeugung'}



#list of non-renewable and dispatchable power plant types
var_nrdg 		= [	'lignite',
					'hard_coal',
					'nat_gas',
					'nuclear',
					'other_fossil',
					'waste',
					'oil']



#design settings for matplot-lib-figures
var_types_col 	= { 'nuclear' 			: 'red',
					'lignite' 			: 'sienna',
					'hard_coal' 		: 'darkgray',
					'nat_gas' 			: 'yellow',
					'oil' 				: 'orange',
					'other_fossil' 		: 'gray',
					'waste' 			: 'brown',
					'biomass' 			: 'green',
					'geothermal'		: 'coral',
					'other_renewable' 	: 'lime',
					'hydro_river' 		: 'blue',
					'hydro_reservoir' 	: 'indigo',
					'wind_offshore' 	: 'aqua',
					'wind_onshore' 		: 'skyblue',
					'pv' 				: 'gold'}



#design setting for aggregated groups in figures
var_types_groups 	= { 'nuclear' 			: 'nuclear',
						'lignite' 			: 'fossil',
						'hard_coal' 		: 'fossil',
						'nat_gas' 			: 'fossil',
						'oil' 				: 'fossil',
						'other_fossil' 		: 'fossil',
						'waste' 			: 'waste',
						'biomass'	 		: 'cRE',
						'geothermal' 		: 'cRE',
						'other_renewable' 	: 'cRE',
						'hydro_river' 		: 'cRE',
						'hydro_reservoir' 	: 'cRE',
						'wind_offshore'		: 'vRE',
						'wind_onshore'		: 'vRE',
						'pv'				: 'vRE'}



#translations for generattion types 'en' : 'de'
var_groups_en2de = {   	'nuclear' 	: 'Kernkraft',
						'fossil' 	: 'Fossil',
						'waste' 	: 'Abfall',
						'cRE' 		: 'sEE',
						'vRE' 		: 'fEE'}



#precalculations for types
types_clmns 	= ['type','group','color','order']
types_data 		= [ ['nuclear','nuclear','red',0],
					['lignite','fossil','sienna',1],
					['hard_coal','fossil','darkgrey',3],
					['nat_gas','fossil','yellow',4],
					['oil','fossil','orange',5],
					['other_fossil','fossil','grey',6],
					['waste','waste','brown',7],
					['biomass','cRE','green',8],
					['geothermal','cRE','coral',9],
					['other_renewable', 'cRE','lime',10],
					['hydro_river','cRE','blue',11],
					['hydro_reservoir','cRE','indigo',12],
					['wind_offshore','vRE','aqua',13],
					['wind_onshore','vRE','skyblue',14],
					['pv','vRE','gold',15]]



#design settings for groups
var_df_types = pd.DataFrame(types_data, columns=types_clmns)
var_df_types.set_index('type')



#design definitions for generation groups
var_df_groups = pd.DataFrame({	'group':var_df_types['group'].unique().tolist(),
								'color':['red','gray','brown','green','chartreuse'],
								'order':[0]*5,
								'value':[0]*5})



#define the assumed kind of function (logistic functio is used here)
def logistic_S(x, Lmin, Lmax, x0, k):
	y = Lmin + ((Lmax - Lmin )/ (1 + np.exp(-k*(x-x0))))

	return(y)