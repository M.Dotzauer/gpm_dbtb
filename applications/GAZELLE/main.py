"""
Main module to calculate the results for systemic life cycle assessment.
Calculation is done step wise but seamlessly.
"""

##preparations

#import global packages
import os, sys
import pandas as pd
import matplotlib.pyplot as plt


#define the local drirectory
loc_dir     = os.path.dirname(__file__)


#add parent directory (tool-box) to the system path for import
tb_dir  = os.path.dirname(os.path.dirname(loc_dir))
sys.path.append(tb_dir)


#import local packages
import processing as prc
import gpm_db_dat as dat
import gpm_tb_dia as dia
import gpm_tb_aux as aux


#define the first and last year of the period for computing empirical ndf and nhf
years_empr  = [2016, 2019]


#define the to predict prices
year_pred   = 2020

#define language seting for plots
lang = 'en'


#define path to GPM.db
db_path =   os.path.join(tb_dir, aux.var_db_name)

#define name and target directroy for png file
png_dir     = os.path.join(loc_dir, 'results')




##calculations



#compute ndf and nhf for the given period of years_empr
dat.ndf_nhf(years_empr[0], years_empr[1])


#plot ndf and save to results folder
png_path    = os.path.join(png_dir, 'ndf_4x7_' + '-'.join([str(i) for i in years_empr]) + '.png')
dia.ndf_4x7_plot(show=False, save_png=png_path)


#plot ndf and save to results folder
png_path    = os.path.join(png_dir, 'nhf_4x168_' + '-'.join([str(i) for i in years_empr]) + '.png')
dia.nhf_4x168_plot(show=False, save_png=png_path)


#computing price prediction as backcasting for a historical year and save result to DataFrame
DF_bc = prc.price_backcast(year_pred)
print('computing price prdiction as backcast')



#plot correlation of prices vs. predictions within backcasting
png_path    = os.path.join(png_dir, 'Backcasting_eval_'  + str(year_pred) + '.png')
prc.backcast_corrl(DF_bc, year_pred, lang='en', show=False, save_png=png_path)


#plot comparisson for potential extra earnings (actual vs. prdicted)
png_path    = os.path.join(png_dir, 'Backcasting_extra_' + str(year_pred) + '.png')
prc.backcast_extra(DF_bc, year_pred, lang='en', show=False, save_png=png_path)


#save prdeictions to csv_file, use reordered columns
DF_export   = DF_bc.iloc[:,[1,2,3,4,0,5]]
csv_path    = os.path.join(png_dir, 'Backcasting_' + str(year_pred) + '.csv')
DF_export.to_csv(csv_path)
print('Export backcasting to ../results')

