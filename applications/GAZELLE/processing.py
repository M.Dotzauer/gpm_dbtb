"""
Module for converting the input files into pandas Dataframe and creae a figure.

"""


import matplotlib.pyplot    as plt
import pandas               as pd
import datetime             as dt
import numpy                as np
import os, sys
import sqlite3              as sq3


#define the local drirectory
loc_dir     = os.path.dirname(__file__)

#add parent directory (tool-box) to the system path for import
tb_dir  = os.path.dirname(os.path.dirname(loc_dir))
sys.path.append(tb_dir)


#import local packages
import gpm_tb_aux as aux
import gpm_tb_dia as dia


    
    
def price_backcast(year):
    """
    Function to compute a backcasting (forecasting for historical periods).
    """
    
    
    #create database connection
    con     = sq3.connect(os.path.join(tb_dir, aux.var_db_name)) #Connect to database
    con_c   = con.cursor()


    #query backcasting price series
    _SQL    = """   SELECT price, dt_CET, FK_season, FK_wday, hour 
                    FROM T_epex_da 
                    JOIN T_ts ON T_ts.PK_ts_UTC=T_epex_da.PK_ts_UTC
                    WHERE substr(dt_CET,1,4) = '{yr}' """.format(yr=year)
    
    DF_bc           = pd.read_sql(_SQL, con)
    DF_bc['price']  = DF_bc['price'].replace('nan',0)
    DF_bc['price']  = pd.to_numeric(DF_bc['price'])
    date_list       = list(DF_bc.loc[::24]['dt_CET'])
    date_list       = [i[0:10] for i in date_list]
    DF_bc['pred']   = 0
    
    
    #query for ndf
    _SQL        = """ SELECT * FROM T_epex_ndf"""
    DF_ndf      = pd.read_sql(_SQL, con)

    #query for ndf
    _SQL        = """ SELECT * FROM T_epex_nhf"""
    DF_nhf      = pd.read_sql(_SQL, con)
    
    
    #query for nhf
    
    
    
    for d, dat in enumerate(date_list):
        
        #define yesterday from current day, with excaption for the first day
        if dat[5:] == '01-01':
            ydy      = dat[0:10]
        else:
            ydy     = date_list[d-1][0:10]
        
        
        #define date of the current day
        cdy          = dat[0:10]
           
            
        #define seasonal and weekday factor for yesterday, with exception for the first day
        if dat[5:] == '01-01':
            ydy_ssn  = DF_bc['FK_season'][d]
            ydy_wdy  = DF_bc['FK_wday'][d]
            
        else:
            ydy_ssn  = DF_bc['FK_season'][d-1]
            ydy_wdy  = DF_bc['FK_wday'][d-1]
        
        #define seasonal and weekday factor for current day
        cdy_ssn  = DF_bc['FK_season'][d]
        cdy_wdy  = DF_bc['FK_wday'][d]
        
            
        #define normalies day-factor for current day 
        ydy_ndf  = DF_ndf[(DF_ndf.PK_season==ydy_ssn) & ((DF_ndf.PK_wday==ydy_wdy))]['value']
        cdy_ndf  = DF_ndf[(DF_ndf.PK_season==cdy_ssn) & ((DF_ndf.PK_wday==cdy_wdy))]['value']
        
        
        
        #calculate average for yesterday corrected by ndf
        ydy_avg_n    = DF_bc[DF_bc['dt_CET'].str.contains(ydy)]['price'].mean() / ydy_ndf
        
        #caluclate prediction for average of current day
        cdy_avg      = float(ydy_avg_n) * float(cdy_ndf)
            
        #compute hourly prediction based on cdy_avg
        for h in range(0,24):
            
            #define hourly-factor for current hour 
            hr_nhf      = DF_nhf[(DF_nhf.PK_season==cdy_ssn)   & \
                            (  DF_nhf.PK_wday==cdy_wdy)     & \
                            (  DF_nhf.hour==h)]['value'] 
            
            
            #compute prediction for single hour
            hr_pred     = cdy_avg * float(hr_nhf)
            
                            
            CET_key = dt.datetime.strftime( dt.datetime.fromisoformat(dat) + \
                                            dt.timedelta(hours=h),'%Y-%m-%d %H:%M:%S')
        
            
            DF_bc.loc[(DF_bc.dt_CET == CET_key), 'pred'] = hr_pred
        
        
        
    return (DF_bc)
        
        
        
def backcast_corrl(DF_bc, year, lang='en', show=True, save_png=False):
    """
    Function for backcasting evaluation.
    """
    
    #computing overall min max values for price and prediction
    min_max    = (  min([DF_bc['price'].min(), DF_bc['pred'].min()]), \
                    max([DF_bc['price'].max(), DF_bc['pred'].max()]))
    
    ccef        = DF_bc['price'].corr(DF_bc['pred'])
    
    #bilungual dictionary for axes labeling
    lbls    = { 'ax_tl' : { 'en':'Backcasting - correlation for ' + str(year), 
                            'de':'Backcasting - Korrelation für ' + str(year)},
                'x_lbl':   { 'en' : ' price [€/MWh]',       'de' : 'Preis [€/MWh]'},
                'y_lbl':   { 'en' : ' prediction [€/MWh]',  'de' : ' Prognose [€/MWh]'}}
    
    #create figure
    fig, ax = plt.subplots(figsize=(5,4))
    
    
    ax.scatter(DF_bc['price'], DF_bc['pred'], s=0.5)
    ax.plot([min_max[0],min_max[1]],[min_max[0],min_max[1]], lw=0.5, color='c')
    
    #subpolpot decoration
    ax.set(title=lbls['ax_tl'][lang], xlabel=lbls['x_lbl'][lang], ylabel=lbls['y_lbl'][lang])
    ax.set_xlim(min_max[0],min_max[1])
    ax.set_ylim(min_max[0],min_max[1])
    
    #add annotation for coefficient of correleation R
    ant_str     = '$R='  + str(round(ccef, 4)) + '$'
    ax.annotate(ant_str, (0,0), (0.75, 0.15), xycoords='axes fraction')
    
    #add grid to each subplot
    ax.grid(lw=0.5, alpha=0.5)

    
    dia.show_save(show, save_png)
    
    
    
def backcast_extra(DF_bc, year, lang='en', show=True, save_png=False): 
        
        
    DF_bc['date']           = DF_bc.dt_CET.str[:10]
    DF_bc['price_rank']     = DF_bc.groupby('date')['price'].rank(ascending=True, method='first')
    DF_bc['pred_rank']      = DF_bc.groupby('date')['pred'].rank(ascending=True, method='first')
        
    annual_mean  =   DF_bc.price.mean()
    
    
    DF_plot = pd.DataFrame(columns=['runtime', 'pot_extra', 'pred_extra'])
    
    hours = [6, 9, 12, 15, 18]
    
    for i, h in enumerate(hours):
        pot_extra   = annual_mean - DF_bc[(DF_bc.price_rank < h +1 )].price.mean()
        pred_extra  = annual_mean -DF_bc[(DF_bc.pred_rank < h + 1)].price.mean()
        DF_plot.loc[i]  = [h, pot_extra, pred_extra]
        
        
    
    #bilungual dictionary for axes labeling
    lbls    = { 'ax_tl' : { 'en':'Potential for extra earnings in ' + str(year), 
                            'de':'Potential für Mehrerlöse in ' + str(year)},
                'x_lbl' :   { 'en' : ' best hours [n]',       'de' : 'Beste Stunden [n]'},
                'y_lbl' :   { 'en' : ' specific extra [€/MWh]',  'de' : ' Spezifischer Mehrerl. [€/MWh]'},
                'bar1l' :   {'en' : ' actual',          'de' : 'Real'},
                'bar2l' :   {'en' : ' prediction',      'de' : 'Prognose'}}
    
    #create figure
    fig, ax = plt.subplots(figsize=(5,4))
    
    x       = np.arange(len(hours))
    width   = 0.4
    
    ax.bar(x - width/2, DF_plot['pot_extra'], width, label=lbls['bar1l'][lang])
    ax.bar(x + width/2 , DF_plot['pred_extra'], width, label=lbls['bar2l'][lang])
    ax.set_xticks(x)
    ax.set_xticklabels(hours)
    
    #subpolpot decoration
    ax.set(title=lbls['ax_tl'][lang], xlabel=lbls['x_lbl'][lang], ylabel=lbls['y_lbl'][lang])
    ax.legend()
    
    #add grid to each subplot
    ax.grid(lw=0.5, alpha=0.5)

    
    dia.show_save(show, save_png)
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
