"""
Main module to calculate the results for systemic life cycle assessment.
Calculation is done step wise but seamlessly.
"""

##preparations

#import global packages
import os, sys
import pandas as pd
import matplotlib.pyplot as plt


#define the local drirectory
main_dir = os.path.dirname(__file__)


#add parent directory (tool-box) to the system path for import
tb_dir  = os.path.dirname(os.path.dirname(main_dir))
sys.path.append(tb_dir)


#import local packages
import xlsx_converter as xlc
import gpm_tb_dia as dia
import gpm_db_dat as dat
import gpm_tb_aux as aux


#define the year for the analysis
year = 2019


#define language seting for plots
lang = 'en'


#define the path for the xlsx file containig the schedules
xl_path = os.path.join(main_dir, 'Sensitivity_2019_PQ.xlsx')


#define name and target directroy for png file
png_dir     = os.path.join(os.path.dirname(__file__), 'results')


##calculations

#computing a DataFrame containing the scheudles, calling the function cl2DF from xlc-module
DF_schedules = xlc.xl2DF(xl_path)


#computing a plot for some sample schedules of the given input file
#the plot will be saved directly to the subfolder FLEXSIGNAL/results
png_name    = 'schedules_' + str(year) + '_' +  lang + '.png'
png_path    =  os.path.join(png_dir, png_name)


#call the function plot7d from xlc-module
xlc.plot_7d(year, DF_schedules, save_png=png_path, lang=lang, title=False)


#refer to external variable var_nrdg in aux-module
nrdg = aux.var_nrdg

#computing all price-elasticity plots for nrdg-types and save png to results subfolder
for gt in nrdg:
    png_name    = 'presponse_' + str(year) + '_' + gt + '_' + lang + '.png'
    png_path    =  os.path.join(png_dir, png_name)
    
    #call function price_elasticity from dia-module
    dia.price_response(year, gt, curve=True, show=False, save_png=png_path, lang=lang, title=False)


#computing priceelasticity functions parameters for nrdg-types
png_name    = 'mod_gen_' + str(year) + '_' + lang + '.png'
png_path    =  os.path.join(png_dir, png_name)


#call nrdg_model-function from dat-module
DF_mod_gen = dat.nrdg_model(year, plot=True, save_png=png_path, lang=lang, title=False)


#plotting marginal emissions for real and modelled generation
png_name    = 'pdei_' + str(year) + '_' + lang + '.png'
png_path    =  os.path.join(png_dir, png_name)

#calling marginal_emissions-function from dia-Module
dia.pdei(year, show=False, save_png=png_path, lang=lang, title=False)


#define local function to calculate net emission effects
def sys_LCA(year:int, lang='en', show=True, save_png=False, title=True):
    """
    Calculating the systemic effects of certain schedules.
    
    Parameters:
    ----------
    year:int
        Year for the investiation
    
    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit
    
    save_png=False
        string with path for saving figure to png file, default=False
    
    lang='en'
        language for the used labels for the plot:
            'en' | englisch
            'de' | german
    """
    
    #create ampty DataFrame with indices for all generation types
    DF_sys_LCA = pd.DataFrame(None, DF_schedules.columns.tolist()[1:-1], ['Emission delta'])
    
    
    #drop row for PQ=2.4 and PQ=3.4286
    DF_sys_LCA = DF_sys_LCA.drop(index='PQ_2.4')
    DF_sys_LCA = DF_sys_LCA.drop(index='PQ_3.4286')
    
        
    #caluclate rated power
    P_rated = DF_schedules['PQ_1.5'].mean()     
    
    
    #compute net emission effects for all generation types
    for gt in DF_sys_LCA.index:
        DF_sys_LCA.loc[gt,'Emission delta'] = ((P_rated - DF_schedules[gt] ) * DF_mod_gen['mem']).sum() / DF_schedules[gt].sum()

    
    #replace '_' by '=' in indexes
    for i in DF_sys_LCA.index:
        i_old = i
        i_new = i.replace('_', '=')
        DF_sys_LCA.rename(index={i_old:i_new}, inplace=True)

    
    #define bilingual dicionary for labels
    lbls={  'title' : { 'en' : 'net emission effect for flex operation in ' + str(year),
                        'de' : 'Nettoemissionseffekt für flexible Fahrweise in '  + str(year)},
            'x_lbl' : { 'en' : 'power quotients (PQ)',
                        'de' : 'Leistungsquotienten (PQ)'},
            'y_lbl' : { 'en' : 'net specific emissions $[g\ CO_{2eq} * kWh^{⁻1}]$',
                        'de' : 'Nettoeffekt spez. Emissionen $[g\ CO_{2eq} * kWh^{⁻1}]$'}}
    
    
    #initial plot of the systemic LCA results as barplot
    ax = DF_sys_LCA.plot.bar(title=lbls['title'][lang], zorder=3)
    
    #add axis labels and grid
    if title==True:
        ax.set_xlabel(lbls['x_lbl'][lang])
    
    ax.set_ylabel(lbls['y_lbl'][lang])
    ax.grid(alpha=0.5, linestyle=':', zorder=0)
    
    #tight layout for reshaping the figure for leave space for the axis labels
    plt.tight_layout()
    
    
    #conditional showing the figure
    if show==True:
        plt.show(block=False)
    
    
    #conditional saving figure as png
    if save_png!=False:
        
        #conditional removing older version of the png-file before save
        if os.path.isfile(png_path):
            os.remove(png_path)
            
        #save figure to png
        plt.savefig(png_path)
                
        #close matplotlib figure if its not shown
        if show!=True:
            plt.close()
                
        #print status message for save png file
        print('save figure to: ' + png_path)
        

#define path for sys_LCA figure to save as png
png_name    = 'em_diff' + str(year) + '_' + lang + '.png'
png_path    =  os.path.join(png_dir, png_name)


#apply syy_LCA calculation
sys_LCA(year, lang=lang, show=False, save_png=png_path, title=False)