"""
Module for converting the input files into pandas Dataframe and creae a figure.

"""

import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import os


def grid_sync_y(ax1, ax2):
    """
    Function to synchronise the scales of two axes (in the same dimension) to get a harmonised grid.
    The function is juste made with basic python operations so no extra dependencies are needed.

    Parameters:
    -----------
    ax_1
        matplotlib-axes-object for the first axis (order doesen't matter)

    ax_2
        matplotlib-axes-object for the first axis (order doesen't matter)

    Return:
    -------
    (ax1_ylim, ax2_ylim)
        A set of two tuples for the y-limits for both input axes

    """


    #create list of scales with a given resolution and a range of 10^x0 to 10^xn
    scales          = []
    resolution      = [0.2, 0.5, 1]
    for i in range(-9, 10):
        scales = scales+[e*pow(10,i) for e in resolution]
    scale_int       = len(scales)


    #extract the initial ylimits for the input axes
    axes            = (ax1, ax2)
    extrema         = [ax.get_ylim() for ax in axes]
    ax1_range       = extrema[0][1] - extrema[0][0]
    ax2_range       = extrema[1][1] - extrema[1][0]


    #create two lists of 10th of the total range
    ax1_rl          = [ax1_range/10] * scale_int
    ax2_rl          = [ax2_range/10] * scale_int


    #calculate nearest scale index based on minimum absolute difference to scales
    ax1_diff        = []
    for i in range(0, scale_int):
        ax1_diff.append(abs(ax1_rl[i] - scales[i]))
    ax1_scale_i     = ax1_diff.index(min(ax1_diff))

    ax2_diff        = []
    for i in range(0, scale_int):
        ax2_diff.append(abs(ax2_rl[i] - scales[i]))
    ax2_scale_i     = ax2_diff.index(min(ax2_diff))


    #compute the initial scale steps (float)
    ax1_steps_0     = ax1_range/scales[ax1_scale_i]
    ax2_steps_0     = ax2_range/scales[ax2_scale_i]


    #select the scale with the smallest amount of intervals and adapt the other one
    if ax1_steps_0 < ax2_steps_0:
        ax1_steps   = ax1_steps_0 // 1
        ax1_scale   = scales[ax1_scale_i - int(ax1_steps - round(ax1_steps_0))]
        ax1_ylim    = [extrema[0][0]//ax1_scale * ax1_scale, -(-extrema[0][1]//ax1_scale) * ax1_scale]

        ax2_scale   = scales[ax2_scale_i + 1]
        ax2_ylim    = [extrema[1][0]//ax2_scale * ax2_scale, -(-extrema[1][1]//ax2_scale) * ax2_scale]
        ax2_steps   = (ax2_ylim[1] - ax2_ylim[0]) / ax2_scale
        steps_diff  = ax1_steps - (ax2_range//ax2_scale) 
        ax2_ylim[1] = ax2_ylim[1] + steps_diff * ax2_scale


    else:
        ax2_steps   = ax2_steps_0 // 1
        ax2_scale   = scales[ax2_scale_i - int(ax2_steps - round(ax2_steps_0))]
        ax2_ylim    = [extrema[0][0]//ax2_scale * ax2_scale, -(-extrema[0][1]//ax2_scale) * ax2_scale]

        ax1_scale   = scales[ax11_scale_i + 1]
        ax1_ylim    = [extrema[1][0]//ax1_scale * ax1_scale, -(-extrema[1][1]//ax1_scale) * ax1_scale]
        ax1_steps   = (ax1_ylim[1] - ax1_ylim[0]) / ax1_scale
        steps_diff  = ax2_steps - (ax1_range//ax1_scale) 
        ax1_ylim[1] = ax1_ylim[1] + steps_diff * ax1_scale


    return(ax1_ylim, ax2_ylim)



def xl2DF(xl_path):
    #create DataFrame for time series
    DF_ts = pd.read_excel(  xl_path, 
                            engine='openpyxl', 
                            sheet_name='BGA_500_PQ_1.5', 
                            usecols = 'I')
    
    
    #
    PQ_list = [ 'PQ_1.5', 
                'PQ_2', 
                'PQ_2.4', 
                'PQ_3', 
                'PQ_3.4286', 
                'PQ_4', 
                'PQ_4.8', 
                'PQ_6', 
                'PQ_8']
    
    
    for i in PQ_list:
        sheet_n='BGA_500_' + i
        DF_ts[i] = pd.read_excel(   xl_path, 
                                    engine='openpyxl', 
                                    sheet_name=sheet_n, 
                                    usecols = 'B')


    return(DF_ts)
    
    
    
def plot_7d(year, DF_ts, lang='en', save_png=False, title=True):
    """
    Function for create a plot of different Schedules (discrete PQ-steps) 
    for the day with the most price variance.
    
    Parameters:
    -----------
    
    lang='en'
        language selector for labeling the plot, possible options: 'en' | 'de'
        

    title=True
        argument for supress the plotting of the title if title!=True
    """
    
    #precalculate the rated capacity
    DF_ts['P_rated'] = DF_ts['PQ_1.5'].mean()
    
    
    #find the day wiht most price variance
    price_variance  = []
    for i in range(0,365):
        day_start  = i*24
        day_end    = day_start + 124
        var = DF_ts.iloc[day_start:day_end,0].var()
        price_variance.append(var)
    
    
    #set day of the most price variance and calculate the string of %d.%m.%y
    var_max_id      = price_variance.index(max(price_variance))
    var_max_date    = dt.date(year,1,1) + dt.timedelta(days =var_max_id)
    var_max_date_str= var_max_date.strftime('%d.%m.%y')
    
    
    #create DataFrame with selected values for the day of the most price variance
    sel_start       = var_max_id*24
    sel_end         = sel_start + 24
    DF_sel          = DF_ts.iloc[sel_start:sel_end,:]
    DF_sel.index    = range(0,24)
    
    #extend DataFrame by one row, to achieve a lot ranging from 0 to 24
    DF_sel          = DF_sel.append(pd.Series(0, index=DF_sel.columns), ignore_index=True)
    
    #elongate the dataseries of dayahead and the rated capacity
    DF_sel.loc[24,'dayahead'] = DF_sel.loc[23,'dayahead']
    DF_sel.loc[24,'P_rated'] = DF_sel.loc[23,'P_rated']
        
    
    #bilingual dictionary for figure labeleling
    lbls    = { 'title' : { 'en' : 'schedules for some PQ at the day of the most price variance - ' + var_max_date_str,
                            'de' : 'Fahrpläne für einige PQ, am Tag der höchsten Preisvarianz - '  + var_max_date_str},
                'x_lbl' : { 'en' : 'hours of one day',
                            'de' : 'Stunden eines Tages'},
                'y_prim': { 'en' : 'load [kW]',
                            'de' : 'Leistung [kW]'},
                'y_sec' : { 'en' : 'epex spot price [€/MWh]',
                            'de' : 'Epex Spot Preis [€/MWh]'}}


    #start plotting with the for rated capacity
    ax0 = DF_sel.plot.line(x=None, 
                    y=10, 
                    rot=90, 
                    figsize=(16,9),
                    fontsize=16,
                    linestyle=':',
                    color='black',
                    alpha=0.50,
                    legend=False)

    
    #Dictionary for columns and colors which should be plotted as barplots
    col_col={1:'orange', 2 :'green', 4:'red', 6:'blue'}
    
    
    #generate bar plots for PQ-steps in the list 'col_col'
    for cc in col_col:
        DF_sel.plot.bar(x=None, 
                        y=cc, 
                        ax=ax0, 
                        color=col_col[cc], 
                        width=1, 
                        alpha=0.33, 
                        align='edge',
                        legend=False)
    
    
    #styling the primary plot                    
    ax0.set_xlim(0,24)
    ax0.set_xlabel(lbls['x_lbl'][lang], fontsize=16)
    ax0.set_ylabel(lbls['y_prim'][lang], fontsize=16)


    #optional plotting the title
    if title==True:
        ax0.set_title(lbls['title'][lang], fontsize=24)
        
        
    #add price series on a secondary y-axis
    ax1 = DF_sel.plot.line( x=None, 
                            y=0, 
                            secondary_y=True, 
                            fontsize=16,
                            drawstyle='steps-post', 
                            color='red',
                            mark_right=False,
                            ax=ax0,
                            legend=False)


    #styling the secondary plot (EPEX prices)
    ax1.set_ylim(-100,DF_sel['dayahead'].max()+10)
    ax1.set_ylabel(lbls['y_sec'][lang], fontsize=16)
    

    # ask matplotlib for the plotted objects and their labels and add legend
    lines0, labels0 = ax0.get_legend_handles_labels()
    lines1, labels1 = ax1.get_legend_handles_labels()
    ax1.legend(lines0 + lines1, labels0 + labels1, loc=9, fontsize=16)
    
    
    #add grid
    ax1.grid(color='grey', alpha=0.33, linestyle='--')
    

    #adjust y-limits for  both axes to algin the grid-lines
    ylims   = grid_sync_y(ax0, ax1)
    ax0.set_ylim(ylims[0])
    ax1.set_ylim(ylims[1])
    

    #show the figure or save to png-file
    if save_png==False:
        plt.show()
    
    else:
        png_path = save_png
        #conditional removing older version of the png-file before save
        if os.path.isfile(png_path):
            os.remove(png_path)
         
        
        plt.savefig(png_path)
        
        
        #print status message for save png file
        print('save figure to: ' + png_path)
    
        
    #close pyplot to suppress showing figures calling form another function
    plt.close()