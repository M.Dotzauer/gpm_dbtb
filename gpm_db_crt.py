#!/usr/bin/env python
"""
This module creates the gmp.db (SQLite db), while 'gmp' stands for german power market.
"""


def create_db(db_path:str):
    """
    Create sqlite3 database.

    parameters
    ----------

    db_dir:str
        target directory of the database.
    """

    import sqlite3 as sq3
    import gpm_tb_aux as aux


    #create the SQL-statement for the database scheme
    _SQL="""

    DROP TABLE IF EXISTS T_wdays;
    CREATE TABLE IF NOT EXISTS T_wdays    (
        PK_wday     INTEGER,
        num_de      INTEGER,
        weekday_de	TEXT,
        weekday_en	TEXT,
        PRIMARY KEY(PK_wday)              );


    DROP TABLE IF EXISTS T_seasons;
    CREATE TABLE IF NOT EXISTS T_seasons (
        PK_season	    INTEGER,
        season_de	    TEXT,
        season_en       TEXT,
        PRIMARY KEY(PK_season)            );


    DROP TABLE IF EXISTS T_months;
    CREATE TABLE IF NOT EXISTS T_months   (
        PK_month	       INTEGER,
        FK_season      INTEGER,
        month_de       TEXT,
        month_en	       TEXT,
        FOREIGN KEY(FK_season) REFERENCES T_seasons(PK_season),
        PRIMARY KEY(PK_month)             );


    DROP TABLE IF EXISTS T_ts;
    CREATE TABLE IF NOT EXISTS T_ts   (
        PK_ts_UTC   INTEGER,
        dt_UTC      TEXT,
        dt_CET      TEXT,
        year        INTEGER,
        FK_month	    INTEGER,
        FK_season	INTEGER,
        day         INTEGER,
        FK_wday	    INTEGER,
        hour        INTEGER,
        minute      INTEGER,
        FOREIGN KEY(FK_season) REFERENCES T_seasons(PK_season),
        FOREIGN KEY(FK_wday) REFERENCES T_wdays(PK_wday),
        FOREIGN KEY(FK_month) REFERENCES T_months(PK_month),
        PRIMARY KEY(PK_ts_UTC),
        UNIQUE(PK_ts_UTC)               );


    DROP TABLE IF EXISTS log;
    CREATE TABLE IF NOT EXISTS log    (
        Name	       TEXT,
        value      TEXT                );


    DROP INDEX IF EXISTS idx_T_ts_PK_ts_UTC;
    CREATE UNIQUE INDEX IF NOT EXISTS idx_T_ts_PK_ts_UTC ON T_ts (
        PK_ts_UTC	ASC);


    DROP TABLE IF EXISTS T_epex_da;
    CREATE TABLE IF NOT EXISTS T_epex_da  (
        PK_ts_UTC	INTEGER,
        price	    REAL,
        pdei        REAL,
        FOREIGN KEY(PK_ts_UTC) REFERENCES T_ts(PK_ts_UTC),
        PRIMARY KEY(PK_ts_UTC),
        UNIQUE(PK_ts_UTC)                   );


    DROP TABLE IF EXISTS T_exaa_da;
    CREATE TABLE IF NOT EXISTS T_exaa_da  (
        PK_ts_UTC	INTEGER,
        price	    REAL,
        pdei        REAL,
        FOREIGN KEY(PK_ts_UTC) REFERENCES T_ts(PK_ts_UTC),
        PRIMARY KEY(PK_ts_UTC),
        UNIQUE(PK_ts_UTC)                   );


    DROP TABLE IF EXISTS T_epex_ndf;
    CREATE TABLE IF NOT EXISTS T_epex_ndf  (
        FK_season	INTEGER,
        FK_wday	    INTEGER,
        value       REAL,
        FOREIGN KEY(FK_season)    REFERENCES T_seasons  (PK_season),
        FOREIGN KEY(FK_wday)      REFERENCES T_wdays    (PK_weekday),
        PRIMARY KEY(FK_season, FK_wday)   );


    DROP TABLE IF EXISTS T_epex_nhf;
    CREATE TABLE IF NOT EXISTS T_epex_nhf             (
        FK_season	INTEGER,
        FK_wday	    INTEGER,
        PK_hour	    INTEGER,
        value       REAL,
        FOREIGN KEY(FK_season)    REFERENCES T_seasons  (PK_season),
        FOREIGN KEY(FK_wday)      REFERENCES T_wdays    (PK_weekday),
        PRIMARY KEY(FK_season, FK_wday, PK_hour)  );


    DROP TABLE IF EXISTS T_gen;
    CREATE TABLE IF NOT EXISTS T_gen  (
        PK_ts_UTC           INTEGER,
        nuclear             REAL,
        lignite             REAL,
        hard_coal           REAL,
        nat_gas             REAL,
        oil                 REAL,
        other_fossil        REAL,
        waste               REAL,
        biomass             REAL,
        geothermal          REAL,
        hydro_river         REAL,
        hydro_reservoir	    REAL,
        pv                  REAL,
        wind_offshore       REAL,
        wind_onshore        REAL,
        other_renewable	    REAL,
        hydro_storage_gen	REAL,
        hydro_storage_con	REAL,
        carbon_mix          REAL,
        FOREIGN KEY(PK_ts_UTC) REFERENCES T_ts(PK_ts_UTC),
        PRIMARY KEY(PK_ts_UTC),
        UNIQUE(PK_ts_UTC)                   );


    DROP TABLE IF EXISTS T_gen_tps;
    CREATE TABLE IF NOT EXISTS T_gen_tps  (
        ID                  INTEGER,
        name_db             TEXT,
        name_de             TEXT,
        name_en             TEXT,
        emissions_fuel      REAL,
        reference	        TEXT,
        PRIMARY KEY(ID)                   );


    DROP TABLE IF EXISTS T_caps;
    CREATE TABLE IF NOT EXISTS T_caps     (
        year                INTEGER,
        nuclear             REAL,
        lignite             REAL,
        hard_coal           REAL,
        nat_gas             REAL,
        oil                 REAL,
        other_fossil        REAL,
        waste               REAL,
        biomass             REAL,
        geothermal          REAL,
        hydro_river         REAL,
        hydro_reservoir	    REAL,
        pv                  REAL,
        wind_offshore       REAL,
        wind_onshore        REAL,
        other_renewable	    REAL,
        hydro_storage_gen   REAL,
        PRIMARY KEY(year)                 );


    DROP TABLE IF EXISTS T_loads;
    CREATE TABLE IF NOT EXISTS T_loads    (
        PK_ts_UTC       INTEGER,
        load            REAL,
        res_load        REAL,
        FOREIGN KEY(PK_ts_UTC) REFERENCES T_ts(PK_ts_UTC),
        PRIMARY KEY(PK_ts_UTC),
        UNIQUE(PK_ts_UTC)                   );


    DROP TABLE IF EXISTS T_ets;
    CREATE TABLE IF NOT EXISTS T_ets  (
        PK_ts_UTC       INTEGER,
        ets             REAL,
        FOREIGN KEY(PK_ts_UTC) REFERENCES T_ts(PK_ts_UTC),
        PRIMARY KEY(PK_ts_UTC),
        UNIQUE(PK_ts_UTC)               );


    """

    #data base connection and submit of the query
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    con_c.executescript(_SQL)
    con.commit()

    #status message for finish
    print('create ' + db_path)


    #create the SQL-statement for the database triggers
    _SQL="""
    DROP TRIGGER IF EXISTS "TRG_epex_ts";
    CREATE TRIGGER TRG_epex_ts
    BEFORE INSERT ON T_epex_da
    BEGIN
        INSERT OR IGNORE INTO T_ts (PK_ts_UTC, dt_UTC, dt_CET, year, FK_season, FK_month, day, FK_wday, hour, minute)
        VALUES(	NEW.PK_ts_UTC,
                datetime(NEW.PK_ts_UTC,'unixepoch'),
                datetime(NEW.PK_ts_UTC,'unixepoch','localtime'),
                strftime('%Y', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                (SELECT FK_season FROM T_months WHERE PK_month=(SELECT strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')))),
                strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%d', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%w', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%H', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%M', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')));
    END;


    DROP TRIGGER IF EXISTS "TRG_exaa_ts";
    CREATE TRIGGER TRG_exaa_ts
    BEFORE INSERT ON T_exaa_da
    BEGIN
        INSERT OR IGNORE INTO T_ts (PK_ts_UTC, dt_UTC, dt_CET, year, FK_season, FK_month, day, FK_wday, hour, minute)
        VALUES(	NEW.PK_ts_UTC,
                datetime(NEW.PK_ts_UTC,'unixepoch'),
                datetime(NEW.PK_ts_UTC,'unixepoch','localtime'),
                strftime('%Y', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                (SELECT FK_season FROM T_months WHERE PK_month=(SELECT strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')))),
                strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%d', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%w', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%H', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%M', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')));
    END;


    DROP TRIGGER IF EXISTS "TRG_load_ts";
    CREATE TRIGGER TRG_load_ts
    BEFORE INSERT ON T_loads
    BEGIN
        INSERT OR IGNORE INTO T_ts (PK_ts_UTC, dt_UTC, dt_CET, year, FK_season, FK_month, day, FK_wday, hour, minute)
        VALUES(	NEW.PK_ts_UTC,
                datetime(NEW.PK_ts_UTC,'unixepoch'),
                datetime(NEW.PK_ts_UTC,'unixepoch','localtime'),
                strftime('%Y', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                (SELECT FK_season FROM T_months WHERE PK_month=(SELECT strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')))),
                strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%d', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%w', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%H', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%M', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')));
    END;


    DROP TRIGGER IF EXISTS "TRG_gen_ts";
    CREATE TRIGGER TRG_gen_ts
    BEFORE INSERT ON T_gen
    BEGIN
        INSERT OR IGNORE INTO T_ts (PK_ts_UTC, dt_UTC, dt_CET, year, FK_season, FK_month, day, FK_wday, hour, minute)
        VALUES(	NEW.PK_ts_UTC,
                datetime(NEW.PK_ts_UTC,'unixepoch'),
                datetime(NEW.PK_ts_UTC,'unixepoch','localtime'),
                strftime('%Y', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                (SELECT FK_season FROM T_months WHERE PK_month=(SELECT strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')))),
                strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%d', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%w', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%H', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%M', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')));
    END;


    DROP TRIGGER IF EXISTS "TRG_ets_ts";
    CREATE TRIGGER TRG_ets_ts
    BEFORE INSERT ON T_ets
    BEGIN
        INSERT OR IGNORE INTO T_ts (PK_ts_UTC, dt_UTC, dt_CET, year, FK_season, FK_month, day, FK_wday, hour, minute)
        VALUES(	NEW.PK_ts_UTC,
                datetime(NEW.PK_ts_UTC,'unixepoch'),
                datetime(NEW.PK_ts_UTC,'unixepoch','localtime'),
                strftime('%Y', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                (SELECT FK_season FROM T_months WHERE PK_month=(SELECT strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')))),
                strftime('%m', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%d', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%w', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%H', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')),
                strftime('%M', datetime(NEW.PK_ts_UTC, 'unixepoch','localtime')));
    END;

    """

    #submit of the query
    con_c.executescript(_SQL)
    con.commit()

    #status message for finish
    print('create trigger for timestamps')


    #create the SQL-statement for the database views
    _SQL="""
        DROP VIEW IF EXISTS V_epex_da;
        CREATE VIEW V_epex_da
        AS
        SELECT T_epex_da.PK_ts_UTC, dt_UTC, dt_CET,  price, pdei
        FROM T_epex_da
        JOIN T_ts ON T_epex_da.PK_ts_UTC=T_ts.PK_ts_UTC;


        DROP VIEW IF EXISTS V_exaa_da;
        CREATE VIEW V_exaa_da
        AS
        SELECT T_exaa_da.PK_ts_UTC, dt_UTC, dt_CET,  price, pdei
        FROM T_exaa_da
        JOIN T_ts ON T_exaa_da.PK_ts_UTC=T_ts.PK_ts_UTC;


        DROP VIEW IF EXISTS V_loads;
        CREATE VIEW V_loads
        AS
        SELECT T_loads.PK_ts_UTC, dt_UTC, dt_CET, load, res_load
        FROM T_loads
        JOIN T_ts ON T_loads.PK_ts_UTC=T_ts.PK_ts_UTC;


        DROP VIEW IF EXISTS V_gen;
        CREATE VIEW V_gen
        AS
        SELECT T_gen.PK_ts_UTC, dt_UTC, dt_CET,
                    nuclear, lignite, hard_coal,
                    nat_gas, oil, other_fossil,
                    waste, biomass, geothermal,
                    other_renewable, hydro_river,
                    hydro_reservoir, wind_offshore,
                    wind_onshore, pv, hydro_storage_gen,
                    hydro_storage_con, carbon_mix
        FROM T_gen
        JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC;


        DROP VIEW IF EXISTS V_ets;
        CREATE VIEW V_ets
        AS
        SELECT T_ts.PK_ts_UTC, dt_UTC, dt_CET, ets
        FROM T_ets
        JOIN T_ts ON T_ets.PK_ts_UTC=T_ts.PK_ts_UTC;
        """


    #submit of the query
    con_c.executescript(_SQL)
    con.commit()

    #status message for finish
    print('create views for prices, loads and generation')



def move_data(db_source, db_target):
    """
    Move data from one _SQLite3 database to antoher.

    parameters
    ----------

    db_source:str
        path (dir + name) of the the source database


    db_target:str
        path (dir + name)
    """

    import sqlite3 as sq
    import os


    con=sq.connect(db_target)
    con_c=con.cursor()

    _SQL=""" ATTACH DATABASE '{dbs}' AS 'source_db';
                INSERT OR REPLACE INTO T_caps SELECT * FROM source_db.T_capacities;
                INSERT OR REPLACE INTO T_gen_tps SELECT * FROM source_db.T_gen_types;
                INSERT OR REPLACE INTO T_wdays SELECT * FROM source_db.T_weekdays;
                INSERT OR REPLACE INTO T_seasons SELECT * FROM source_db.T_seasons;

            DETACH DATABASE source_db""".format(dbs=db_source)
    con_c.executescript(_SQL)

    print('move selected data from ' + db_source + ' to ' + db_target)