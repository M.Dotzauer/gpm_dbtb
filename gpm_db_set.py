#!/usr/bin/env python
"""
Module for set up the database content gpm.db by using the modules:
    gmp_db_crt - creates the database scheme (including triggers and views
    gpm_db_dat - cointains import functions and calculate secondary data.
"""


import gpm_db_crt as crt
import gpm_db_dat as dat
import gpm_tb_aux as aux
import os


#path to data base
db_path = os.path.join(aux.var_script_dir, aux.var_db_name)


#directory for entsoE csv-files
csv_sub_dir = os.path.join(os.path.dirname(db_path), 'csv')


#directory for  ETS xlsx-files
xl_sub_dir      = os.path.join(aux.var_script_dir, 'xls\EEX_ETS')



#function to process many csv files
def batch_entsoe(file_names, db_table_name):
    """
    Function for batch processing of file_name lists.
    """
    for fn in file_names:
        csv_path    = os.path.join(csv_sub_dir, 'entsoE', fn)

        dat.entsoE_csv2db(csv_path, db_table_name)



#function to process many xlsx files
def batch_eex(file_names, db_table_name):
    """    Function for batch processing of file_name lists. """

    for fn in file_names:
        xl_path    = os.path.join(xl_sub_dir, fn)

        dat.eex_ets_2db(xl_path, db_table_name)



def db_rebuild():
    """Function to rebuild the complete data base"""

    #create the data base scheme including triggers and views
    crt.create_db(db_path)
    print('+++++ creation of data base (scheme, trigger, views) complete +++++  \n')


    #importing generation types meta data
    csv_name    = 'T_gen_tps.csv'
    dat.meta_gen_types(os.path.join(csv_sub_dir, 'meta', csv_name))


    #importing seasons, months and weekdays meta data
    csv_names   = ['T_seasons.csv', 'T_months.csv', 'T_wdays.csv']
    dat.smw_metadata(os.path.join(csv_sub_dir, 'meta'), csv_names)


    #price data import for opsd
    csv_name    = 'time_series_60min_singleindex_filtered.csv'
    csv_path    =  os.path.join(csv_sub_dir, 'opsd', csv_name)

    dat.opsd_csv2db(csv_path)


    #../ capacity data
    csv_name    = 'Capacities_2015_2024.csv'
    csv_path    =  os.path.join(csv_sub_dir, 'entsoE', csv_name)

    dat.entsoE_csv2db_cap(csv_path)


    #../ price data for the EPEX Spot market in Paris
    file_names = [  'EPEX_day_ahead_2018_DE_AT_LU.csv',
                    'EPEX_day_ahead_2018_DE_LU.csv',
                    'EPEX_day_ahead_2019.csv',
                    'EPEX_day_ahead_2020.csv',
                    'EPEX_day_ahead_2021.csv',
                    'EPEX_day_ahead_2022.csv',
                    'EPEX_day_ahead_2023.csv',
                    'EPEX_day_ahead_2024.csv',
                ]

    batch_entsoe(file_names, 'T_epex_da')
    print('+++++ import EPEX day-ahead prices +++++  \n')


    #../ price data for the EXAA Spot market in Vienna
    file_names = [  'EXAA_day_ahead_2018.csv',
                    'EXAA_day_ahead_2019.csv',
                    'EXAA_day_ahead_2020.csv',
                    'EXAA_day_ahead_2021.csv',
                    'EXAA_day_ahead_2022.csv',
                    'EXAA_day_ahead_2023.csv',
                    'EXAA_day_ahead_2024.csv',
                    ]

    batch_entsoe(file_names, 'T_exaa_da')
    print('+++++ import EXAA day-ahead prices +++++  \n')



    #../ load data
    file_names = [  'Load_2015.csv',
                    'Load_2016.csv',
                    'Load_2017.csv',
                    'Load_2018.csv',
                    'Load_2019.csv',
                    'Load_2020.csv',
                    'Load_2021.csv',
                    'Load_2022.csv',
                    'Load_2023.csv',
                    'Load_2024.csv',
                    ]

    batch_entsoe(file_names, 'T_loads')
    print('+++++ import ENTSOE load data +++++  \n')


    #../ generation data
    file_names = [  'Generation_2015.csv',
                    'Generation_2016.csv',
                    'Generation_2017.csv',
                    'Generation_2018.csv',
                    'Generation_2019.csv',
                    'Generation_2020.csv',
                    'Generation_2021.csv',
                    'Generation_2022.csv',
                    'Generation_2023.csv',
                    'Generation_2024.csv'
                    ]

    batch_entsoe(file_names, 'T_gen')
    print('+++++ import ENTSOE generation data +++++  \n')


    #data import ETS of EEX
    xl_dirlist  = os.listdir(xl_sub_dir)

    batch_eex(xl_dirlist, 'T_ets')
    print('+++++ import EEX ETS-prices +++++  \n')



    #calculate residual load
    dat.calc_res_load()

    print('+++++ computing residual load +++++ \n')


    #calculate carbon mix
    dat.carbon_mix()


    #calculate price dependet emission intensity
    gen_years   = [2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023]


    #clean EXAA-data table
    dat.T_exaa_clean()


    for gy in gen_years:
        dat.nrdg_model(gy, market = 'EPEX')

        if gy >= 2018:
            dat.nrdg_model(gy, market = 'EXAA')

    print('+++++ computing pdei +++++  \n')




def db_update():
    """ Function to update database referring to entsoe and eex -csv-files
        (dowload of recent files shoul occur beforehand).
        Follwing primary data sets will be updated:
                                            - day-ahead prices (entsoe)
                                            - load (entsoe)
                                            - generation per type (entsoe)
                                            - ETS primary market auction prices (eex)

        Following secondary data sets will be updated:
                                            - residual load
                                            - carbon mix
    """

    #status message for beginning
    print('Start database update...')


    #data import for entsoE
    csv_sub_dir = 'csv\entsoE'


    #../ price data
    files   = ['EPEX_day_ahead_2024.csv']
    batch_entsoe(files, 'T_epex_da')

    files   = ['EXAA_day_ahead_2024.csv']
    batch_entsoe(files, 'T_exaa_da')
    print('...update day ahead prices (EPEX & EXAA) \n')


    #../ load data
    file    = ['Load_2024.csv']
    batch_entsoe(file, 'T_loads')
    print('...update load time series \n')


    #../ generation data
    file    = ['Generation_2024.csv']
    batch_entsoe(file, 'T_gen')
    print('...update generation time series \n')


    #../ ets primary market price data
    file        = [os.path.join(xl_sub_dir, 'emission-spot-primary-market-auction-report-2024-data.xlsx')]
    batch_eex(file, 'T_ets')
    print('...update ets prices \n')


    #calculate residual load
    dat.calc_res_load()
    print('...compute updated residual load time series \n')


    #calculate carbon mix
    dat.carbon_mix()
    print('...compute updated carbon mix time series \n')


    print('Finish data base update')