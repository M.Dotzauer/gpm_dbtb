"""
Module for check and install requiered python libraries.
"""

import sys
import importlib 	as 	il
import subprocess 	as 	sp



#function for checking and if needed install the used python libraries of the GPM-toolbo
def pylibs_check_and_install():
    """Check if all neccessary python libraries were availible and instlall missing libs via pip"""


    lib_list = ['pandas', 'numpy', 'matplotlib', 'scipy', 'openpyxl']


    for l in lib_list:
            try:
                    l_imp = l.replace('-', '_')
                    lib = il.import_module(l_imp)
                    del lib
                    print(f'>>>BAT>>> The "{l}"-package is already installed.')

            except:

                    sp.check_call([sys.executable, '-m', 'pip', 'install', '--user', l])
                    print(f'>>>BAT>>> Install the missing "{l}"-package via pip.')