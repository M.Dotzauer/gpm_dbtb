:: bat-file for automatically start the gui of the gpm_toolbox

title Update GPM.db by latest ETSOE and EEX-Data sets

cd %cd%

set local_dir=%cd%
echo %local_dir%

python -c "print('%local_dir%')"

python -c "import sys; sys.path.append('%local_dir%'); import gpm_db_set; gpm_db_set.db_update()"

pause