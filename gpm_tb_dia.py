#!/usr/bin/env python
"""
Diagramming module of the german-power-market database-toolbox (GPM.db.tb).

"""

#importing global modules
import os
import sqlite3              as sq3
import pandas               as pd
import numpy                as np
import datetime             as dt
from scipy.optimize                         import curve_fit
import matplotlib.pyplot    as plt
import matplotlib.cm        as cmp
import matplotlib.dates     as mdate
import matplotlib.ticker    as ticker
from matplotlib.ticker                      import MaxNLocator, MultipleLocator
from matplotlib                             import gridspec, colors
from mpl_toolkits.axes_grid1.inset_locator  import inset_axes
from matplotlib.dates                       import DateFormatter
from matplotlib.colors                      import ListedColormap, LinearSegmentedColormap
from matplotlib.offsetbox                   import OffsetImage, AnnotationBbox
from matplotlib.widgets                     import Slider, Button, RadioButtons
from matplotlib.patches                     import Rectangle



#importing local modules of gpm_dbtb
import gpm_tb_aux as aux
import gpm_db_dat as dat


#path to data base
db_path = os.path.join(aux.var_script_dir, aux.var_db_name)


market_tabs     = { 'EPEX'  : 'T_epex_da',
                    'EXAA'  : 'T_exaa_da'}


def db_tT_EXAA_hourly_averages(db_con):
    """ Supportfunction to create a temporary table for hourly aggregated prices. """

    _SQL =  """ DROP TABLE IF EXISTS tT_exaa_hourly_avgs;
                
                CREATE TEMPORARY TABLE tT_exaa_hourly_avgs AS
                SELECT T_exaa_da.PK_ts_UTC, avg(price) AS price
                FROM T_exaa_da
                JOIN T_ts ON T_exaa_da.PK_ts_UTC = T_ts.PK_ts_UTC
                GROUP BY substr(dt_UTC, 1, 13) """
                
    db_con.executescript(_SQL)
    db_con.commit()


def figure_creator(figsize = (4,4), window_title='test', grc_subplots=(1,1), 
                    hr_subplots=None, wr_subplots=None):
    """Dynamic figure creator with reactive gridspec- for source declaration subplot.
    
    Parameters:
    ==========
    figsize:tuple
        Figure size in width by height in inches.
        
    window_title:str
        Title of the matplotlib-window of the figure, also default for name if saving from that window.
        
    grc_subplots:tuple
        grid, rows and columns
        
    hr_sublpots:list
        heigth ratios for the given subplots
        
    wr_sublpots:list
        width ratios for the given subplots
        
    sahrey:bool
        Chosen if y-axis should be shared between subplots.


    Return:
    =======
    
    fig:figure object
        final figure object including the footer for source declaration if requested.
        
    gs:GridSpec object
        Grid specifications for the final figure.
    """
    

    fig= plt.figure(figsize=figsize) 
    
    #set window title
    fig.canvas.manager.set_window_title(window_title)
    
    
    
    if hr_subplots == None:
        hr_subplots = [1] * grc_subplots[0]
    
    
    if wr_subplots == None:
        wr_subplots = [1] * grc_subplots[1]
    
    
    gs  = gridspec.GridSpec(grc_subplots[0], 
                            grc_subplots[1],
                            height_ratios=hr_subplots, 
                            width_ratios=wr_subplots)
    

    return(fig, gs)

    
    

    
def add_fig_footer(fig, footer = 'GPM & CC', lang='en, target_dpi=100'):
    """Insert footer content for source citation and or Creative Commons Licence.
    
    Parameters:
    ==========
    fig:figure object
        Already prepared figure with an separate axis for annotations (axa) used as footer.
    
    gs:grid specs
        grid specifications (rows, cols).
    
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC'
    
    lang:str
        Chosen language for the source declaration, can be "de" or "en"
        
    target_dpi:int
        Target_dpi to change positions of annotations.
        
    Return:
    =======
    fig:figure object
        final figure object including the footer for source declaration if requested.
    """

    #add footer axis at the bottom of the figure
    w, h        = fig.get_size_inches() * fig.dpi
    pfw, pfh    = 1/w, 1/h
    

    axf     =  fig.add_axes([1, 0, 0, 0], facecolor='blue')
    axf.set_frame_on(True)
    axf.set_xticks([])
    axf.set_yticks([])
    
    
    #footer content for GPM.db.tb logo and source declaration
    if 'GPM'  in footer:
        #source title
        ps  = {'en': 'primary source', 'de': 'Primärquelle'}
        ss  = {'en': 'secondary source', 'de': 'Sekundärquelle'}    
        
        #GPM logo
        GPM_ir  = plt.imread(os.path.join(aux.var_script_dir, 'gui', 'GPMdbtb_icon_h48.png'))
        GPM_oi  = OffsetImage(GPM_ir, zoom=0.5)
        #GPM_oi.image.axes = axf
        GPM_ab  = AnnotationBbox(GPM_oi, (5, 5), xycoords='figure pixels', box_alignment=(0, 0),frameon=False)
        fig.add_artist(GPM_ab)

        #annotation for source declaration
        plt.annotate(ps[lang] + ': transparency.entsoe.eu/ \n' + ss[lang] + ': ' + 'gpm.db' + ' | ' + str(dt.datetime.now())[:19] + '\nsource code @ gitlab.com/M.Dotzauer/gpm_dbtb',  \
        xy=(pfw * 150, pfh * 3), xycoords='figure fraction', ha='left', va='bottom', size=7)


    #footer content for Creative Commons remark
    if 'CC'  in footer:
        
        #add logo
        CC_ir  = plt.imread(os.path.join(aux.var_script_dir, 'licence', 'CC_BY_h48.png'))
        CC_oi  = OffsetImage(CC_ir, zoom=0.5)
        CC_oi.image.axes = axf
        CC_ab  = AnnotationBbox(CC_oi, (-5, 5), xycoords='axes pixels', 
                                    box_alignment=(1, 0),frameon=False)
        axf.add_artist(CC_ab)

        # #annotation
        axf.annotate( 'Copyright Martin Dotzauer 2024\n Creative Commons Attribute 4.0\n creativecommons.org/licenses/by/4.0',  \
                    xy=(1 - (pfw * 105), pfh * 3), xycoords='figure fraction', ha='right', va='bottom', size=7)
    

        # print(f'width = {w} | hight = {h}')
    
    
    
def show_save(show:bool, save_png:str, dpi_save='figure'):
    """
    Helping function for saving plots to png.
    
    Parameters:
    -----------
    show
        argument for control showing the figure
    
    save_png:str
        argument for control saving the figure, if save_png==False figure wouldnt be saved,
        other wise save_png encode the path to save the png file.
    """
    
    #conditional saving the figure to a given path
    if save_png!=False:
        png_path = save_png
        
        #conditional removing older version of the png-file before save
        if os.path.isfile(png_path):
            os.remove(png_path)
                
        #save the figure    
        plt.savefig(png_path, dpi=dpi_save)        

        print('save figure to: ' + save_png)
        
        #close figure object if its not shown
        if show!=True:
            plt.close()
    
    
    #conditional showing the figure
    if show==True:
        plt.show()



def yticks_align(ax1, ax2):
    """Function to align yticks for two axes and adjust ylim to min max of yticks"""
    
    #create lists for yticks
    ax1_tl 	= list(ax1.get_yticks())
    ax2_tl 	= list(ax2.get_yticks())
    
    
    #if length of both lists is already equal set them to resulting extend lists arguments
    if len(ax1_tl) == len(ax2_tl):
        ax1_etl 	= ax1_tl
        ax2_etl 	= ax2_tl
    
    else:
    
        #get ylims for axes
        ax1_yl 	= ax1.get_ylim()
        ax2_yl 	= ax2.get_ylim()
    
    
        #compute scales for both axes
        ax1_scl = ax1_tl[1] - ax1_tl[0]
        ax2_scl = ax2_tl[1] - ax2_tl[0]
    
    
        def list_expander(ax_tl, ax_yl, ax_scl, t_len):
            """Subfunction to incrementally expand the shorter list of the given ytick-lists"""
    
            #check wich end of ylim is closer to the first / last tick and set start index for inserting
            if abs(ax_tl[0] - ax_yl[0]) > abs(ax_tl[-1] - ax_yl[-1]):
                sii 	= 0
            else:
                sii 	= -1
    
            #initiate extended ticks list
            ax_etl 	= list(ax_tl)
    
    
            #extend list, switich between first and last postition until length is equal to target length
            while len(ax_etl) < t_len:
    
                #compute new tick value, insert and flip ssi
                if sii == 0:
                    new_tick 	= ax_etl[0] - ax_scl
                    ax_etl.insert(sii, new_tick)
                    sii = 1
    
                else:
                    new_tick 	= ax_etl[-1] + ax_scl
                    ax_etl.append(new_tick)
                    sii = 0
    
            return(ax_etl)
    
    
        #apply list extension function to ax1 or ax2 and set the other list
        if len(ax1_tl) < len(ax2_tl):
            ax1_etl 	= list_expander(ax1_tl, ax1_yl, ax1_scl, len(ax2_tl))
            ax2_etl 	= ax2_tl
    
        else:
            ax1_etl 	= ax1_tl
            ax2_etl 	= list_expander(ax2_tl, ax2_yl, ax2_scl, len(ax1_tl))
    
    
    #set new properties (yticks and ylims)
    ax1.set_yticks(ax1_etl)
    ax1.set_ylim(ax1_etl[0], ax1_etl[-1])
    
    ax2.set_yticks(ax2_etl)
    ax2.set_ylim(ax2_etl[0], ax2_etl[-1])
    
    
    return(ax1, ax2)


    

## load and generation diagramms

def cap_timeseries(lang='en', figsize=(12, 6), dpi_save=150, show=True, save_png=False, 
                title=True, footer='GPM & CC'):
    """
    Create a diagramm for annual generation capacities based on a query on entsoE.sqlite-database.

    Parameters
    ----------
    lang='en'
        Language selector {en : english, de : german}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """

    #labels dictionary
    lbls    = { 'title' : { 'en' : 'Power plant capacities  - generation types',
                            'de' : 'Kraftwerkspazitäten - Erzeugungsarten'},
                'sec_tl': { 'en' : 'Capacities',
                            'de' : 'Kapazitäten'},
                'lgd_tl': { 'en' : 'group', 
                            'de' : 'Gruppe' }}

    #SQL-query
    sql_chain='year'
    for i in [*aux.var_types_col.keys()]:
        sql_chain=sql_chain + ',' +  i +  '/1000'

    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    _SQL    = ('SELECT {gen_types} FROM T_caps'.format(gen_types=sql_chain))
    
    #create DataFrame for generation 
    df_gc_types = pd.read_sql(_SQL, con, index_col='year')
    df_gc_types.columns=aux.var_df_types['type'].tolist()


    #precalculations for plotting
    years       = df_gc_types.index.tolist()
    last_year   = df_gc_types.index.max()

    ind         = df_gc_types.index.tolist()
    val         = df_gc_types.T
    types_lbl   = aux.var_df_types['type'].tolist()

    if lang=='de':      #change english labels to german labels
        for i in range(0, len(types_lbl)):
            types_lbl[i] = aux.var_types_en2de[types_lbl[i]]

    #set colours
    col = aux.var_df_types['color'].tolist()


    #set up figure oject
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'capacity_timeseries', 
                                    grc_subplots    = (1,2), 
                                    hr_subplots     = None, 
                                    wr_subplots     = (12,1)
                                )
    
    #optional plotting the title
    if title==True:
        fig.suptitle( lbls['title'][lang] + '[GW]', fontsize=16)


    #subplot 1
    ax0 = fig.add_subplot(gs[0,0])
    ax0.stackplot(ind, val, labels=types_lbl, colors=col,  zorder=3)
    handles, labels = ax0.get_legend_handles_labels()
    ax0.legend(reversed(handles), reversed(labels), title= lbls['sec_tl'][lang] + ' [GW] ' + str(last_year), loc='center left')
    ax0.secondary_yaxis('right')
    ax0.xaxis.set_major_locator(MaxNLocator(integer=True))


    #insert grid
    plt.grid(color='gray', linestyle='dashed', axis='y', zorder=0, alpha=0.66)


    #subplot 2
    groups_lbl = aux.var_df_groups['group'].tolist()


    #change english labels to german labels if lang=='de
    if lang=='de':      
        for i in range(0, len(groups_lbl)):
            groups_lbl[i] = aux.var_groups_en2de[groups_lbl[i]]

    #build generation groups for the last year
    df_group_sum=df_gc_types.T.join(aux.var_df_types.set_index('type'),how='outer').groupby('group').sum()[last_year]
    aux.var_df_groups['value']=aux.var_df_groups.set_index('group').join(df_group_sum)[last_year].tolist()

    #plot right hand bar plot of groups of generation types
    ax1 = fig.add_subplot(gs[0,1], sharey=ax0)
    for i, l in enumerate(groups_lbl):
        ax1.bar(last_year, aux.var_df_groups['value'][i], 
                bottom = sum(aux.var_df_groups['value'][:i]), \
                color = aux.var_df_groups['color'][i], label=l, zorder=i+1)
    
    #labeling for secondary axis
    handles, labels = ax1.get_legend_handles_labels()
    box = ax1.get_position()
    
    #axis styling
    ax1.set_position([box.x0, box.y0, box.width * 0.66, box.height])
    ax1.legend(reversed(handles), reversed(labels), title=lbls['lgd_tl'][lang] , 
                loc='center left', bbox_to_anchor=(0.25, 0.5))
    ax1.xaxis.set_major_locator(ticker.NullLocator())
    ax1.set_xlabel(str(last_year))

    #add grid 
    plt.grid(color='gray', linestyle='dashed', which='both')
    
    plt.subplots_adjust(left=0.05, bottom=0.125, right=0.95, top=0.9)
    
    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)
    

    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)





def cap_annual(year:int, figsize=(9, 7), dpi_save=150, lang='en', show=True, save_png=False, 
                title=True, footer='GPM & CC'):
    """ Create diagramms (double-ring pie chart) for annual electricity generation (just grid feed in) based on a query on entsoE.sqlite-database.

    Parameters
    ----------
    year : int
        Year for the requestet diagramm.

    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """

    #labels dictionary
    lbls={  'plt_ttl' : {   'en' : 'Capacities in ',
                            'de' : 'Kapazitäten in '},
            'lgd_ttl' : {   'en' : 'Share [%] \n of ',
                            'de' : 'Anteil [%] \n von '},
            'src_prm' : {   'en' : 'Primary source: ',      'de' : 'Primärquelle: ' },
            'src_scd' : {   'en' : 'Secondary source: ',    'de' : 'Sekundärquelle: ' },
            }
    
    sql_chain= []
    for i in [*aux.var_types_col.keys()]:
        sql_chain.append( f'{i}/1000')
        
    sql_str = ', '.join(sql_chain)


    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    con_c.execute("""   SELECT {sql_chn} 
                        FROM T_caps 
                        WHERE year={y} """.format(y=year, sql_chn = sql_str))
    
    caps    = con_c.fetchone()
    

    #data preprocessing
    types_lbl   = list(aux.var_types_col.keys())

    if lang=='de':      #change english labels to german labels
        for i in range(0, len(types_lbl)):
            types_lbl[i] = aux.var_types_en2de[types_lbl[i]]

    types_val   = list(caps)
    types_rel   = [0]*len(types_val)
    
    for i in range(0,len(types_val)):
        types_rel[i]=types_val[i]/sum(types_val)
    
    types_lgd   = ['']*len(types_val)
    
    for i in range(0,len(types_lbl)):
        types_lgd[i] = types_lbl[i] + ' ' + '{:.1%}'.format(types_rel[i])
    types_cl=list(aux.var_types_col.values())

    groups      = list(dict.fromkeys(aux.var_types_groups.values()))
    groups_lbl  = list(groups)
    
    if lang=='de':      #change english labels to german labels if lang=='de
        for i in range(0, len(groups_lbl)):
            groups_lbl[i] = aux.var_groups_en2de[groups_lbl[i]]

    groups_val=[0]*len(groups)
    for i in range(0,len(list(aux.var_types_col.keys()))):
        if list(aux.var_types_groups.values())[i]==groups[groups.index(list(aux.var_types_groups.values())[i])]:
            groups_val[groups.index(list(aux.var_types_groups.values())[i])]+=types_val[i]


    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'annual_capacities', 
                                    grc_subplots    = (1,1), 
                                    hr_subplots     = None, 
                                    wr_subplots     = None)

    #first ring (outside)
    ax     = fig.add_subplot(gs[0,0])
    
    #optional plotting the title
    if title==True:
        ax.set_title(lbls['plt_ttl'][lang] + str(year)  + ' [GW] \n', fontsize=16)
   
    pie1, texts =ax.pie(groups_val, radius=1.2-0.2, labels=groups_lbl, labeldistance=0.4, 
                        rotatelabels =False, colors=aux.var_df_groups['color'].tolist(),
                        center=(0,0),textprops={'fontsize': 12})
    plt.setp(pie1, width=0.4, edgecolor='white')

    for t in texts:
        t.set_horizontalalignment('center')

    
    #second ring (inside)
    inner_lbl=['']*len(types_val)
    for i in range(0,len(types_val)):
        inner_lbl[i]=round(types_val[i],1)
    pie2, _ = ax.pie(types_val, radius=1.2, labels=inner_lbl, labeldistance=0.7,
                    rotatelabels =True, colors=types_cl, center=(0,0),textprops={'fontsize': 12})
    plt.setp(pie2, width=0.4, edgecolor='white')
    plt.margins(0,0)


    #pseudo Ring for move first and second ring by center=(x,y)
    psp_val         = [0]
    pseudopie, _    = ax.pie(psp_val, center=(0.5,0),  normalize=False)


    #legend
    plt.legend()
    handles, labels = ax.get_legend_handles_labels()
    l               = ax.legend(reversed(handles[5:]), reversed(types_lgd), loc=(0.85,0.05), title=lbls['lgd_ttl'][lang] + str(int(sum(types_val))) +  ' GW total \n', fontsize=12)
    plt.setp(l.get_title(), multialignment='center', fontsize=14)

    
    plt.subplots_adjust(left=0.05, bottom=0.1, right=0.95, top=0.9) 

    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)

    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)







def gen_annual(year:int, figsize=(9, 7), dpi_save=150, lang='en', show=True, save_png=False, 
                title=True, footer='GPM & CC'):
    """ Create diagramms (double-ring pie chart) for annual electricity generation (just grid feed in) based on a query on entsoE.sqlite-database.

    Parameters
    ----------
    year : int
        Year for the requestet diagramm.

    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """

    #labels dictionary
    lbls={  'plt_ttl' : {   'en' : 'Grid feed in for ',
                            'de' : 'Netzeinspeisung für '},
            'lgd_ttl' : {   'en' : 'Share [%] \n of ',
                            'de' : 'Anteil [%] \n von '},
            'src_prm' : {   'en' : 'Primary source: ',      'de' : 'Primärquelle: ' },
            'src_scd' : {   'en' : 'Secondary source: ',    'de' : 'Sekundärquelle: ' },
            }
            # 'yl' : {'en' : 'specific price spread [€/Mwh]', 'de' : 'Spezifischer Preis Spread [€/Mwh]'}}

    #sql query
    sql_chain=''
    for i in list(aux.var_types_col.keys()):
        if i == list(aux.var_types_col.keys())[0]:
            sep=''
        else:
            sep=','
        sql_chain=sql_chain + sep + ' SUM(' + i + ')/4/1000000'

    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    con_c.execute('SELECT {SUM_types} FROM T_gen INNER JOIN T_ts ON T_ts.PK_ts_UTC=T_gen.PK_ts_UTC WHERE year={y}'.format(y=year,SUM_types=sql_chain))
    
    gen_sum = con_c.fetchone()

    #data preprocessing
    types_lbl   = list(aux.var_types_col.keys())

    if lang=='de':      #change english labels to german labels
        for i in range(0, len(types_lbl)):
            types_lbl[i] = aux.var_types_en2de[types_lbl[i]]

    types_val   = list(gen_sum)
    types_rel   = [0]*len(types_val)
    for i in range(0,len(types_val)):
        types_rel[i]=types_val[i]/sum(types_val)
    types_lgd   = ['']*len(types_val)
    for i in range(0,len(types_lbl)):
        types_lgd[i] = types_lbl[i] + ' ' + '{:.1%}'.format(types_rel[i])
    types_cl=list(aux.var_types_col.values())

    groups      = list(dict.fromkeys(aux.var_types_groups.values()))
    groups_lbl  = list(groups)
    if lang=='de':      #change english labels to german labels if lang=='de
        for i in range(0, len(groups_lbl)):
            groups_lbl[i] = aux.var_groups_en2de[groups_lbl[i]]

    groups_val=[0]*len(groups)
    for i in range(0,len(list(aux.var_types_col.keys()))):
        if list(aux.var_types_groups.values())[i]==groups[groups.index(list(aux.var_types_groups.values())[i])]:
            groups_val[groups.index(list(aux.var_types_groups.values())[i])]+=types_val[i]


    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'annual_generation', 
                                    grc_subplots    = (1,1), 
                                    hr_subplots     = None, 
                                    wr_subplots     = None)

    #first ring (outside)
    ax     = fig.add_subplot(gs[0,0])
    
    #optional plotting the title
    if title==True:
        ax.set_title(lbls['plt_ttl'][lang] + str(year)  + ' [TWh] \n', fontsize=16)
   
    pie1, texts =ax.pie(groups_val, radius=1.2-0.2, labels=groups_lbl, labeldistance=0.45, 
                        rotatelabels =False, colors=aux.var_df_groups['color'].tolist(),
                        center=(0,0),textprops={'fontsize': 12})
    plt.setp(pie1, width=0.4, edgecolor='white')

    for t in texts:
        t.set_horizontalalignment('center')

    
    #second ring (inside)
    inner_lbl=['']*len(types_val)
    for i in range(0,len(types_val)):
        inner_lbl[i]=round(types_val[i],1)
    pie2, _ = ax.pie(types_val, radius=1.2, labels=inner_lbl, labeldistance=0.75,
                    rotatelabels =True, colors=types_cl, center=(0,0),textprops={'fontsize': 12})
    plt.setp(pie2, width=0.4, edgecolor='white')
    plt.margins(0,0)


    #pseudo Ring for move first and second ring by center=(x,y)
    psp_val         = [0]
    pseudopie, _    = ax.pie(psp_val, center=(0.5,0),  normalize=False)


    #legend
    plt.legend()
    handles, labels = ax.get_legend_handles_labels()
    l               = ax.legend(reversed(handles[5:]), reversed(types_lgd), loc=(0.85,0.05), title=lbls['lgd_ttl'][lang] + str(int(sum(types_val))) +  ' TWh total \n', fontsize=12)
    plt.setp(l.get_title(), multialignment='center', fontsize=14)

    
    plt.subplots_adjust(left=0.05, bottom=0.1, right=0.95, top=0.9) 

    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)

    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def resload_heatmap(year:int, figsize=(12,6), dpi_save=150, lang='en', show=True, save_png=False, 
                    title=True, footer='GPM & CC'):
    """ Function for plotting a heat map of residual loads for a single year.

    Parameters
    ----------

    year:int
        Input year which should be plottet
        
    figsize:tuple
        figure sizes in inches

    lang='en':str
        Language selector {'en' : english, 'de' : german}
    
    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """

    #querry residual load data and datetime for CET
    _SQL    = """   SELECT res_load, dt_CET
                    FROM T_loads
                    JOIN T_ts ON T_loads.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE strftime('%Y', dt_CET)='{y}'
                    AND res_load IS NOT '-' """.format(y=year)
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    con_c.execute(_SQL)
    f_resl  = con_c.fetchall()

    #create nested list from fetchall
    nl_resl = []
    sublist = []
    for i in f_resl:                        #slice daily resloads (96 quater hours)
        if i[1][11:19]=='23:45:00':         #interrupt sublist creation for last entry
            sublist.append(i[0])
            if len(sublist)==92:            #handle exceptions for timeshift in spring
                for idx in range(0, 4):
                    sublist.insert(8+idx,sublist[7])

            elif len(sublist)==100:         #handle exceptions for timeshift in autumn
                for idx in range(0, 4):
                    del sublist[12+idx]

            nl_resl.append(sublist)         #append sublist to the long nested list
            sublist=[]

        else:
            sublist.append(i[0])

    #create numpy array for regular values
    a_resl  = np.array(nl_resl).T
    a_resl  = np.array(a_resl, dtype=float) #convert NULL-values to numpy nan
    a_resl  = np.nan_to_num(a_resl)         #convert numpy nan to ZEROS
    a_resl  = a_resl / 1E3                  #concert MW into GW 

    #bilingual labels for the heat map
    lbls    = { 'titel' : { 'en':'Heatmap for residual load [GW] - '  + str(year), 
                            'de':'Heatmap für die Residuallast [GW] - '  + str(year)},
                'x_lbl' : { 'en':'days', 'de':'Tage'},
                'y_lbl' : { 'en':'hours', 'de':'Stunden'}}

    
    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'residual_load_heatmap', 
                                    grc_subplots    = (1,2), 
                                    hr_subplots     = None, 
                                    wr_subplots     = (20, 1)
                                )

    #subplot for regular values
    ax     = fig.add_subplot(gs[0,0])
    
    im_price = ax.imshow(a_resl, cmap='RdBu_r', vmin=-80, vmax=80, extent=[0,365,23,0])
    ax.set_aspect(6.25)
    
    #optional plotting the title
    if title==True:
        ax.set(title=lbls['titel'][lang])
    
    ax.set(xlabel=lbls['x_lbl'][lang], ylabel=lbls['y_lbl'][lang])
    ax.set_xticks(np.arange(0, 365, 30))
    ax.set_yticks(np.arange(0, 23, 3))

    #colorbar for regular values
    cbax    = fig.add_subplot(gs[0,1])
    cb      = fig.colorbar(im_price, cax = cbax)


    plt.subplots_adjust(left=0.05, bottom=0.1, right=0.95, top=0.95, wspace=0.05)
    
    #reshape colorbar, matching the vertical layout of the main plot
    ax_pos  = ax.get_position()
    cbax_pos= cbax.get_position()
    cb.ax.set_position([cbax_pos.x0, ax_pos.y0, cbax_pos.width, ax_pos.height])

    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)


    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)





def resload_dur_curve(years:list, figsize=(12,6), 
                    dpi_save=150, lang='en', show=True, save_png=False, title=True, footer='GPM & CC'):
    """ Plotting a price duration curve, grouped by years ore types.

    Parameters:
    -----------

    years:list
        list of years as integers, if just one type is choosen
        for multiple types the first year in the list is used

    
    figsize:tuple
        figure sizes in inches
        
    lang='en':str
        Language selector {'en' : english, 'de' : german}
        
    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 

    Returns:
    --------
    optional Error-message, if a certain year isn't complete so load duration curve
    could not be computed.
    """


    con         = sq3.connect(db_path)

    #Create empty DataFrame with index of quater hours for generation curves (DF_gc)   
    h_index     = list(range(0,(8784*4)))
    DF          = pd.DataFrame(data=None,index=h_index)


    #data collection with regard to the series-argument
    for y in years:
        
        #values
        _SQL = """  SELECT *
                    FROM V_loads
                    WHERE dt_CET LIKE '{yr}%'
                    AND res_load IS NOT 'nan'
                    ORDER BY res_load DESC""".format(yr=y)
        
        
        DF[y] = pd.read_sql(_SQL, con)['res_load'] / 1E3


    yrs_sl      = [ f'{x}' for x in years]
    yrs_str     = ', '.join(yrs_sl)

    #bilingual labels for load duratio ncurves
    lbls    = { 'title' : { 'en' : f'residual load duration curves for {yrs_str}' , 
                             'de':'Jahresdauerlinien der residuallast {yrs_str}' },
                'x_lbl' : {  'en':'sorted quarter hours of one year', 
                             'de':'Sortierte Viertelstunden eines Jahres'},
                'y_lbl' : {  'en':'residual load [MW]', 
                             'de':'Residuallast [MW]'},}


    

    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'residual_load_duration_curves', 
                                    grc_subplots    = (1,1), 
                                    hr_subplots     = None, 
                                    wr_subplots     = None, 
                                )

    #first ring (outside)
    ax     = fig.add_subplot(gs[0,0])

    #plotting the load duration curves
    DF.plot( ax=ax, xlabel=lbls['x_lbl'][lang], ylabel=lbls['y_lbl'][lang], xlim=[0,8784*4])
    

    #optinal plotting the title
    if title==True:
        ax.set_title(lbls['title'][lang])
    
    ax.grid(ls=':', alpha=0.75)
    
    ax.legend(loc='upper right')

    plt.subplots_adjust(left=0.1, bottom = 0.1 * ((1 / figsize[1]) * 10), right=0.925, wspace=0.05)

    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)


    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)



def gen_heatmap(year:int, gen_type:str, figsize=(12,6), dpi_save=150, lang='en', show=True, 
                save_png=False, title=True, footer='GPM & CC'):
    """ Plotting a heat map for generation pattern of a single year and a certain generation type.

    Parameters
    ----------
    year:int
        Input year which should be plottet

    gen_type:str
        Generation type for which the heat map shoul be plotted
    
    figsize:tuple
        figure sizes in inches


    lang='en':str
        Language selector {'en' : english, 'de' : german}
    
    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """


    if gen_type == 'wind':
        gen_type = 'wind_onshore + wind_offshore'

    _SQL    = """   SELECT avg({gt}), dt_CET
                    FROM T_gen
                    JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE year={y}
                    GROUP BY substr(dt_CET,1,13)""".format(y=year, gt=gen_type)
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    con_c.execute(_SQL)
    f_gen   = con_c.fetchall()

    #create nested list from fetchall
    nl_gen  = []
    sublist = []
    for i in f_gen: #disaggregate daily prices (24h)
        if int(i[1][11:13])==23 and len(sublist)==22:
            sublist.insert(2,sublist[2]) #2nd value for spring time shift
            sublist.append(i[0])
            nl_gen.append(sublist)
            sublist=[]
        elif int(i[1][11:13])==23:
            sublist.append(i[0])
            nl_gen.append(sublist)
            sublist=[]
        else:
            if i[0]=='':
                sublist.append(sublist[-1]) #last value for autumn time shift
            else:
                sublist.append(i[0])
    
    
    #numpy array made from the nested list
    a_gen   = np.array(nl_gen).T
    a_gen   = a_gen / 1E3


    #conditional translation of the generation type
    if lang=='de':
        gen_type    = aux.var_types_en2de[gen_type]

    #bilingual labels for the heat map
    lbls    = { 'title' : { 'en':'Heat Map for generation from ' + gen_type +  ' [GW]: ' + str(year), 
                            'de':'Heat Map  für die Erzeugung aus ' + gen_type +  ' [GW]: ' + str(year)},
                'x_lbl' : { 'en':'days', 'de':'Tage'},
                'y_lbl' : { 'en':'hours', 'de':'Stunden'}}


    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'generation_heatmap', 
                                    grc_subplots    = (1,2), 
                                    hr_subplots     = None, 
                                    wr_subplots     = (20, 1)
                                )

    #subplot for regular values
    ax     = fig.add_subplot(gs[0,0])

    im_gen  = ax.imshow(a_gen, cmap='Greys', aspect=6)

    #optional plotting the title
    if title==True:
        ax.set(title=lbls['title'][lang])
        
    ax.set(xlabel=lbls['x_lbl'][lang], ylabel=lbls['y_lbl'][lang])
    ax.set_yticks(np.arange(0, 23, 3))
    ax.set_xticks(np.arange(0, 365, 30));


    #colorbar for regular values
    cbax    = fig.add_subplot(gs[0,1])
    cb      = fig.colorbar(im_gen, cax = cbax)

    
    #rearrange figure elemtents for tight layout
    plt.subplots_adjust(left=0.05, bottom=0.1, right=0.95, top=0.95, wspace=0.05)

    
    #reshape colorbar, matching the vertical layout of the main plot
    ax_pos  = ax.get_position()
    cbax_pos= cbax.get_position()
    cb.ax.set_position([cbax_pos.x0, ax_pos.y0, cbax_pos.width, ax_pos.height])


    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)


    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)





def vre_heatmap(year:int, dark_calm_threshold=0.05, figsize=(12,6), dpi_save=150, lang='en', 
                        show=True, save_png=False, title=True, footer='GPM & CC'):
    """ Function for plotting a heat map of variable renewable energies
    (wind onshore, wind offshore, pv) for a single year.

    Parameters
    ----------

    year:int
        Input year which should be plottet

    dark_calm_threshold=0.0
        Threshold for dark calm values, which were colored in black. Default == 0.01.
    
    figsize:tuple
        figure sizes in inches

    lang='en':str
        Language selector {'en' : english, 'de' : german}
        
    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """

    #querry residual load data and datetime for CET
    _SQL    = """   SELECT wind_offshore + wind_onshore + pv AS vRE, dt_CET
                    FROM T_gen
                    JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE strftime('%Y', dt_CET)='{y}' """.format(y=year)
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    
    con_c.execute(_SQL)
    f_vre   = con_c.fetchall()

    #create nested list from fetchall
    nl_vre  = []
    sublist = []
    for i in f_vre:                         #slice daily resloads (96 quater hours)
        if i[1][11:19]=='23:45:00':         #interrupt sublist creation for last entry
            sublist.append(i[0])
            if len(sublist)==92:            #handle exceptions for timeshift in spring
                for idx in range(0, 4):
                    sublist.insert(8+idx,sublist[7])

            elif len(sublist)==100:         #handle exceptions for timeshift in autumn
                for idx in range(0, 4):
                    del sublist[12+idx]


            nl_vre.append(sublist)          #append sublist to the long nested list
            sublist=[]

        else:
            sublist.append(i[0])

    #create numpy array for regular values
    a_vre   = np.array(nl_vre).T
    a_vre   = np.array(a_vre, dtype=float)          #convert NULL-values to numpy nan
    a_vre   = np.nan_to_num(a_vre)                  #convert numpy nan to ZEROS
    a_vre   = a_vre / 1E3
    vre_peak= a_vre.max()
    dct     = vre_peak*dark_calm_threshold           #dark calm threshold for masked values
    dct_lbl = str(int(dark_calm_threshold*100))


    #bilingual labels for the heat map
    lbls    = {'ttl': { 'en' : f'Variable renewbale feed in (dark calm threshold: {dct_lbl}%) [GW] - {year}',
                        'de' : f'Fluktuierende erneuerbare Erzeugung (Schwellwert Dunkelflaute: {dct_lbl}%) [GW] - {year}'},
            'x_lbl'     : {'en':'days', 'de':'Tage'},
            'y_lbl'     : {'en':'hours', 'de':'Stunden'}}

   
    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'variable_renewables_heatmap', 
                                    grc_subplots    = (1,2), 
                                    hr_subplots     = None, 
                                    wr_subplots     = (20, 1), 
                                )

    #subplot for regular values
    ax     = fig.add_subplot(gs[0,0])

    #modify colormap to dark caml-value-threshhold (black)
    cm_base = cmp.get_cmap('Reds', 1000)
    cm_mod  = cm_base(np.linspace(0,1,1000))
    col     = np.array([0/256, 0/256, 0/256, 1])
    dct     = int(dark_calm_threshold*1000)
    cm_mod[:dct, :] = col
    ccmap   = ListedColormap(cm_mod)

    #subplot for regular values
    im_vre  = ax.imshow(a_vre, cmap=ccmap, extent=[0,365,23,0])

    #axis modifications
    ax.set_aspect(6.25)
    
    #optional plotting the title
    if title==True:
        ax.set(title=lbls['ttl'][lang])
    
    ax.set(xlabel=lbls['x_lbl'][lang], ylabel=lbls['y_lbl'][lang])
    ax.set_xticks(np.arange(0, 365, 30));
    ax.set_yticks(np.arange(0, 23, 3))


    #colorbar for regular values
    cbax    = fig.add_subplot(gs[0,1])
    cb      = fig.colorbar(im_vre, cax = cbax)

    
    #rearrange figure elemtents for tight layout
    plt.subplots_adjust(left=0.05, bottom=0.075, right=0.95, top=0.975, wspace=0.05)

    
    #reshape colorbar, matching the vertical layout of the main plot
    ax_pos  = ax.get_position()
    cbax_pos= cbax.get_position()
    cb.ax.set_position([cbax_pos.x0, ax_pos.y0, cbax_pos.width, ax_pos.height])


    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)


    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def gen_timeseries_static(last_day:str, periods:int, figsize=(12,6), dpi_save=150, lang='en', 
                                    show=True, save_png=False, title=True, footer='GPM & CC'):
    """ Create a diagramm for annual generation capacities based on a query on entsoE.sqlite-database.
    
    Parameters:
    -----------
    last_day:str
        last day for the timeseries as datetime string
    
    periods:int
        periods (days) for the timeseries plot
        
    figsize:tuple
        figure sizes in inches
    
    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    save_png=False
        string with path for saving figure to png file, default=False

    lang='en'
        Language selector {en : english, de : german}

    title=True
        argument for supress the plotting of the title if title!=True
        
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """
    
    #calculate date-objects
    d_end_ob    = dt.datetime.strptime(last_day,'%d.%m.%Y')
    d_end_str   = '"' + dt.datetime.strftime(d_end_ob,'%Y-%m-%d') + '%"'
    d_start_ob  = d_end_ob-dt.timedelta(days=periods-1)
    d_start_str = '"' + dt.datetime.strftime(d_start_ob,'%Y-%m-%d') + '%"'

    #database connection and cursor
    con         = sq3.connect(db_path)
    con_c       = con.cursor()
    con_c.execute("SELECT MAX(PK_ts_UTC) FROM T_ts WHERE dt_CET LIKE {dt}".format(dt=d_end_str))
    ts_last     = con_c.fetchone()

    con_c.execute("SELECT MIN(PK_ts_UTC) FROM T_ts WHERE dt_CET LIKE {dt}".format(dt=d_start_str))
    ts_first=con_c.fetchone()

    #constructing SQL-query string
    sql_chain   = 'PK_ts_UTC'
    for col_i in aux.var_df_types['type']:
        sql_chain=sql_chain + ', ' +  col_i +  '/1000'
    _SQL        = ("""  SELECT {gen_types} 
                        FROM T_gen
                        WHERE PK_ts_UTC>={start} AND PK_ts_UTC<={end} """.format(gen_types=sql_chain, start=ts_first[0], end=ts_last[0]))
    
    #SQL query directly to DataFrame
    DF_ts       = pd.read_sql(_SQL, con)
    
    #converting UTC timestamps to CET datetime values
    DF_ts['PK_ts_UTC']  = pd.to_datetime(DF_ts['PK_ts_UTC'],unit='s').dt.tz_localize('UTC')
    DF_ts['PK_ts_UTC']  = DF_ts['PK_ts_UTC'].dt.tz_convert('Europe/Berlin')#.dt.tz_localize(None)

    
    DF_ts2              = DF_ts.set_index('PK_ts_UTC')
    DF_ts2.columns      = aux.var_df_types['type']

    _SQL                = ('SELECT PK_ts_UTC, load/1000, res_load/1000 FROM T_loads WHERE PK_ts_UTC>={start} AND PK_ts_UTC<={end} '.format(start=ts_first[0], end=ts_last[0]))
    
    DF_ts_loads         = pd.read_sql(_SQL, con)
    DF_ts_loads['PK_ts_UTC'] = pd.to_datetime(DF_ts_loads['PK_ts_UTC'],unit='s').dt.tz_localize('UTC')
    DF_ts_loads['PK_ts_UTC'] = DF_ts_loads['PK_ts_UTC'].dt.tz_convert('Europe/Berlin')
    DF_ts_loads         = DF_ts_loads.set_index('PK_ts_UTC')
    DF_ts_loads.columns = ['load','residual load']

    
    #create date strings for labels
    start_str       = d_start_ob.strftime('%d.%m.%y')  
    end_str         = d_end_ob.strftime('%d.%m.%y')
    
    #labels dictionary
    lbls={  'title'     : { 'en' : 'Generation [GW] period: ' + start_str + '-' + end_str,
                            'de' : 'Erzeugung [GW] Periode: ' + start_str + '-' + end_str},
            'x_lbl'     : { 'en' : 'Date',
                            'de' : 'Datum'},
            'y_lbl'     : { 'en' : 'Generation  [GW]',      
                            'de' : 'Erzeugung [GW] ' },
            'src_scd' : {   'en' : 'Secondary source: ',    'de' : 'Sekundärquelle: ' }}

    
    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'generation_timeseries', 
                                    grc_subplots    = (1,1), 
                                    hr_subplots     = None, 
                                    wr_subplots     = None, 
                                )

    #first ring (outside)
    ax     = fig.add_subplot(gs[0,0])

    #plotting the timeseries
    DF_ts2.plot.area(ax=ax, color=aux.var_df_types['color'], label=aux.var_df_types['type'], linewidth=0)
    DF_ts_loads.plot(kind='line',ax=ax, color='black', style=['-',':'])
    plt.grid(color='gray', linestyle='dashed', which='both')
    
    
    #optional plotting the title
    if title==True:
        ax.set_title(lbls['title'][lang])
    
    ax.set(xlabel=lbls['x_lbl'][lang], ylabel=lbls['y_lbl'][lang])
    
    handles, labels = ax.get_legend_handles_labels()
    
    #change english labels to german labels if lang=='de
    if lang=='de':      
        for i, l in enumerate(labels):
            labels[i] = aux.var_types_en2de[l]

    #show legend labels in revesed order
    ax.legend(reversed(handles), reversed(labels), loc='upper left')
    
    
    plt.subplots_adjust(left=0.05, bottom=0.15, right=0.99, top=0.95, wspace=0.05)

    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)


    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def generation_timeseries(year:int, lang='en', show=True, save_png=False, title=True, 
                        figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """ Plotting a generation time series and residual load for a single year.

    Parameters:
    -----------
    year:int
        selected year for generation time series
    
    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.
        
    footer:str
        definition of footer content: 'GPM' - GermanPowerMarket sources | 'CC' - Creative Commons
    """

    #create data base connection and cursor
    con         = sq3.connect(db_path)
    con_c       = con.cursor()
    
    _SQL_gen    = """   SELECT V_gen.PK_ts_UTC, V_gen.dt_UTC, V_gen.dt_CET, {gtl}, V_loads.load, V_loads.res_load 
                        FROM V_gen
                        JOIN V_loads ON V_loads.PK_ts_UTC == V_gen.PK_ts_UTC
                        WHERE strftime('%Y', date(V_gen.dt_CET)) == '{yr}' """
        
        
    #query the base price for one year
    gen_tps_DS  = aux.var_df_types['type']
    gen_tps_cnc = ['V_gen.' + x for x in gen_tps_DS]
    gen_tps_str = ', '.join(gen_tps_cnc)
    _SQL_frmt   = _SQL_gen.format(gtl = gen_tps_str, yr=year)
    
    
    DF          = pd.read_sql(_SQL_frmt, con)
    DF['dt_CET']= pd.to_datetime(DF['dt_CET'])
    DF.fillna(value=0, inplace=True)
    DF          = DF.replace('nan', 0)
    DF.index    = DF['dt_CET']
        
    val_cols    = aux.var_df_types['type'].tolist() + ['load', 'res_load']
    
    DF[val_cols]= DF[val_cols]/1E3
    DF.index.name = 'GW'
    
    
    wddict  =  {    'Mon'   : 'Mo',
                    'Tue'   : 'Di',
                    'Wed'   : 'Mi',
                    'Thu'   : 'Do',
                    'Fri'   : 'Fr',
                    'Sat'   : 'Sa',
                    'Sun'   : 'So' }

    
    lbls    = { 'title' : { 'en' : f'time series for loads and generation in {year}', 
                            'de' : f'Zeitreihen für Lasten und Erzugung in {year}'},
                'ax2tl' : { 'en' : f'resiudal load course for the whole year (use slider to move focus 14-day range)', 
                            'de' : f'Residuallastgang für das Gesamtjahr (Schieberegler für Verschieben des Fokus der 14-Tagesauswahl)'},
                'lgnd'  : { 'en' : 'legend', 'de':'Legende'},
                'xlbl'  : { 'en' : 'date', 'de':'Datum'},
                'ylbl'  : { 'en' : 'generation / load  [GW]', 'de':'Erzeugung / Last [GW]'},
                'sldr'  : { 'en' : 'day \nslider', 
                            'de' : 'Tag \nSchieber'}
                }
    
    fig, gs = figure_creator(   figsize=figsize, 
                                window_title=f'generation_timeseries_{year}', 
                                grc_subplots=(3,1), 
                                hr_subplots=[7, 1, 2]
                            )
                                
    ax0     = fig.add_subplot(gs[0,0])
    ax1     = fig.add_subplot(gs[1,0])
    ax2     = fig.add_subplot(gs[2,0])
    
    #axis for the Export-Button
    axb     = fig.add_axes([0.35, 0.0125, 0.3, 0.05], facecolor='silver')
    
    range_slider    = Slider(ax1, lbls['sldr'][lang], 0, 365 - 14, valinit=0, valstep=list(range(365 - 14)))
    export_button   = Button(axb, f'Export --> gpm_dbtb/xls/export_gen_loads_{year}.xlsx', color='silver', hovercolor='0.9')
    
    #axis modifications
    if title==True:
        fig.suptitle(lbls['title'][lang])
    
    
    DF[gen_tps_DS].plot.area(ax=ax0, color=aux.var_df_types['color'], label=aux.var_df_types['type'], linewidth=0)
    DF['res_load'].plot(ax=ax0, color='black', lw=1.5, ls=':')
    DF['load'].plot(ax=ax0, color='black', lw=1.5)
    ax0.set_xlabel('')

    ax0_sec     = ax0.secondary_xaxis('top')
    dates       = [dt.date.fromtimestamp(d*3600*24).strftime('%a') for d in ax0.get_xticks()]
    if lang == 'de':
        dates = [wddict[d] for d in dates]
    ax0_sec.set_ticks(ax0.get_xticks(), dates)
    

    handles, labels = ax0.get_legend_handles_labels()
    
    #change english labels to german labels if lang=='de
    if lang=='de':      
        for i, l in enumerate(labels):
            labels[i] = aux.var_types_en2de[l]

    #show legend labels in revesed order
    ax0.legend(reversed(handles), reversed(labels), loc='upper left')
    
    loc = MultipleLocator(base=1)
    ax0.xaxis.set_major_locator(loc)
    
    ax0.set(ylabel=lbls['ylbl'][lang]) #xlabel=lbls['xlbl'][lang], 
    ax0.grid(ls=':', alpha=0.5)
    
    ax1.axis('off')
    
    ax2.plot(DF['dt_CET'], DF['res_load'], lw=0.75, color='black')
    ax2.grid(ls=':', alpha=0.8)
    ax2.set_title(lbls['ax2tl'][lang])
    
    ts0     = DF.iloc[0, 0]/3600/24
    tsn     = DF.iloc[-1, 0]/3600/24

    
    ax0.set_xlim(ts0, ts0 + 14)
    ax2.set_xlim(ts0, tsn)
    
    
    ymin, ymax    = ax2.get_ylim()

    rect = Rectangle((ts0,ymin), 14, ymax - ymin, color='blue', alpha=0.33)
    ax2.add_patch(rect)
    
    
    
    def slider_interaction(val, ref0 = ts0, ref1 = ts0 + 14):
    
        shift       = range_slider.val
        x_l         = ref0 + shift
        x_r         = ref1 +shift
        ax0.set_xlim([x_l, x_r])
        rect.set(x=x_l)
        dates       = [dt.date.fromtimestamp(d*3600*24).strftime('%a') for d in ax0.get_xticks()]
        if lang == 'de':
            dates = [wddict[d] for d in dates]
        ax0_sec.set_ticks(ax0.get_xticks(), dates)
        
        fig.canvas.draw_idle()
    

    def export_interaction(mouse_event):
        xl_path = aux.os.path.join(aux.var_script_dir, 'xls', f'export_gen_loads_{year}.xlsx')
        DF.to_excel(xl_path, 'export')
    
    
    range_slider.on_changed(slider_interaction)
    export_button.on_clicked(export_interaction)  


    # plt.tight_layout()
    plt.subplots_adjust(left=0.075, right=0.95, bottom=0.125, top = 0.9125, hspace=0.45)
    
    
    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)
        
    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)






def gen_dur_curve(years:list, types:list, norm=False, series='types', figsize=(12,6), 
                    dpi_save=150, lang='en', show=True, save_png=False, title=True, footer='GPM & CC'):
    """ Plotting a price duration curve, grouped by years ore types.

    Parameters:
    -----------

    years:list
        list of years as integers, if just one type is choosen
        for multiple types the first year in the list is used

    types:list
        list of generation type (if just one year is choosen)
        for multiple years the first tpye in the list is used


    norm:bool (default: False)
        value for specifying if an absolute scaling or a normalised scaling
        is reuiered. normalisation is done by the maximum value of the timeline.

    series:str (default: 'types')
        specify if different 'types' or different 'years' should be considered
    
    figsize:tuple
        figure sizes in inches
        
    lang='en':str
        Language selector {'en' : english, 'de' : german}
        
    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 

    Returns:
    --------
    optional Error-message, if a certain year isn't complete so load duration curve
    could not be computed.
    """

    raw_list    = []
    con         = sq3.connect(db_path)

    #Create empty DataFrame with index of quater hours for generation curves (DF_gc)   
    h_index     = list(range(0,(8784*4)))
    DF_gc       = pd.DataFrame(data=None,index=h_index)


    
    
    def data_collector(DF_gc:pd.DataFrame, years:int, types:list, norm:bool, series:str, db_path:str):
        """
        Subfunction for data collection depending on arg "series" which decide if 
        one type and several years or several types for one year should be selected.
        
        Parameters:
        -----------
        DF_gc:pd.DataFrame 
            containing generation data
        
        years:int
            year or years for the duration curves
            
        types:list
            list of one type or rather several types for generation duration curves
        
        norm:bool
            argument for switch between absolute or normalised values
            
        series
            argument for define if years or types should be the 
            
        db_path
            path leading to the gpm.db
        
        Returns:
        --------
        [DF_gc, title_substr]
            list of the DataFrame for the generation curves and the substring for the title.
        """
        
        
        #differ between the two options for series
        if series=='types':         #series were made for one year and several types
            yrs     = years[0]
            multi   = types
        elif series=='years':       #series were made for one type and several years
            tps     = types[0]
            multi   = years
        else:                       #error message for wrong argument settings
            print('wrong argument for -series-')

        
        #data collection with regard to the series-argument
        for m in multi:
            if series=='types':     #set types to multi-variable (m)
                tps     = m
            else:                   #set years to multi-variable (m)
                yrs     = m

            #data base connection and cursor
            con     = sq3.connect(db_path)
            con_c   = con.cursor()
            
            #values
            _SQL="""SELECT year, {tp}
                    FROM T_gen
                    JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE year={yr}
                    AND {tp} IS NOT 'nan'
                    ORDER BY {tp} DESC""".format(yr=yrs, tp=tps)
            con_c.execute(_SQL)
            f_multi = con_c.fetchall()


            #annual maximum
            _SQL="""SELECT year, max({tp})
                    FROM T_gen
                    JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE year={yr} 
                    AND {tp} IS NOT 'nan' """.format(yr=yrs, tp=tps)
            con_c.execute(_SQL)
            f_max   = con_c.fetchone()

            #optional normalising factor (fn) based on annual maximum
            if norm==True: 
                fn  = f_max[1]
            else:
                fn  = 1

            #calculate normalised values
            multi_list  = []
            for f in f_multi:
                if type(f[1])==str:
                    gen = 0
                else:
                    gen = f[1]/fn
                multi_list.append(gen)
            
            
            #append list with None-values if missing vales or non-leap-year lead to a shorter list
            len_dif     = 35136 - len(multi_list) 
            multi_list  = multi_list+[None]*len_dif
            DF_gc[m]    = multi_list


        return([DF_gc, multi])


    #call subfunction data_collector
    data    = data_collector(DF_gc, years, types, norm, series, db_path)
    DF_gc   = data[0]
    DF_gc   = DF_gc / 1E3


    #set title depending on selected series
    if series=='years':
        title_substr = types[0]
    elif series=='types':
        title_substr = years[0]
    else:
        print('no valid series - argument')
    
    #change title if lang=='de' and series=='years'
    if lang=='de' and series=='years':
        title_substr = aux.var_types_en2de[title_substr]


    #y_label depending on argument for normalised feed in
    if norm==True:
        y_lbl_dict  = { 'en':'normalised feed-in [-]', 
                        'de':'Normalisierte Einspeisung [-]'}
    else:
        y_lbl_dict  = { 'en':'feed-in [GW]', 
                        'de':'Einspeisung [GW]'}


    #bilingual labels for load duratio ncurves
    lbls    = { 'title' : { 'en':'generation duration curves for ' + str(title_substr), 
                             'de':'Jahresdauerlinien der Erzeugung ' + str(title_substr)},
                'x_lbl' : {  'en':'sorted quarter hours of one year', 
                             'de':'Sortierte Viertelstunden eines Jahres'},
                'y_lbl' : y_lbl_dict}


    
    #change title substrings for german generation types of lang=='de'
    if lang=='de' and series=='types':
        series_lbls = data[1]
        for t in enumerate(series_lbls):
            series_lbls[t[0]] = aux.var_types_en2de[t[1]]
    else:
        series_lbls = data[1]
    
    #define colors align with genration types colors if series=='types'
    if series=="types":
        color_list  = []
        for c in DF_gc.columns:
            color   = aux.var_types_col[c]
            color_list.append(color)
        if lang=='de': #change DataFrame column names, which were used for ploting legend
            DF_gc = DF_gc.rename(columns = aux.var_types_en2de)
    else:
        color_list=None

    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'generation_timeseries', 
                                    grc_subplots    = (1,1), 
                                    hr_subplots     = None, 
                                    wr_subplots     = None, 
                                )

    #first ring (outside)
    ax     = fig.add_subplot(gs[0,0])

    #plotting the load duration curves
    DF_gc.plot( ax=ax, xlabel=lbls['x_lbl'][lang], ylabel=lbls['y_lbl'][lang], xlim=[0,8784*4], \
                        grid=True, color=color_list)
    

    #optinal plotting the title
    if title==True:
        ax.set_title(lbls['title'][lang])
    
    #placing legend depending whatever its an absolute or normalised scaling
    if norm==True:
        leg_loc='lower left'
    else:
        leg_loc='upper right'
    
    ax.legend(loc=leg_loc)

    plt.subplots_adjust(left=0.1, bottom=0.15, right=0.925, top=0.95, wspace=0.05)

    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)


    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




##price diagramms
def annual_pricestats(market = 'EPEX', figsize=(12,6), dpi_save=150, lang='en', show=True, save_png=False, 
                        title=True, footer='GPM & CC'):
    """ Create 3 stacked diagramms for annual price statistics based on a query on gpm.db.

    Parameters
    ----------
    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    figsize:tuple
        figure sizes in inches
    
    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC'
    """

    #data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()

    
    #create empty Data Frame for annual price statistics with years as index
    _SQL                = """   SELECT distinct(year) 
                                FROM {market_tab} 
                                JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                                """
    DF_aps              = pd.read_sql(_SQL.format(market_tab = market_tabs[market]), con) 

    
    #loop for max, avg, min price values
    mav = ['avg'] # ['max', 'avg', 'min']
     
    for i in mav:
        #create SQL query statemanet for max, avg, min price values 
        _SQL = """  SELECT {mav}(price)
                    FROM {market_tab} 
                    JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                    AND price IS NOT 'nan'
                    GROUP BY year """.format(mav=i, market_tab = market_tabs[market])
        
        #qerry each value directly to a new column
        DF_aps[i] = pd.read_sql(_SQL, con)
    
    
    #loop for dynamic price spread (dyps) of best 18, 12 and 6 hours
    dyps = [18, 12, 6]
    
    for i in dyps:
        #create SQL query statemanet for max, avg, min price values 
        _SQL = """  SELECT avg(price) as {lb}
                        FROM
                            (SELECT year, price, date(dt_CET) AS 'dt_CET', rank () OVER (PARTITION BY date(dt_CET) ORDER BY price) pricerank
                            FROM {market_tab}
                            JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC)
                        WHERE pricerank>24-{hr}
    GROUP BY year""".format(lb='b_' + str(i), hr=i, market_tab = market_tabs[market])
        
        #qerry each value directly to a new column
        DF_aps['b_' + str(i)] = pd.read_sql(_SQL, con)
        DF_aps['b_' + str(i)] = DF_aps['b_' + str(i)] - DF_aps['avg']
    
    
    #create SQL query statemanet for count von negative prices 
    _SQL = """  SELECT sum(CASE WHEN price < 0 THEN 1 ELSE 0 END) AS 'count_neg'
                FROM {market_tab}
                JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                GROUP BY year """

    
    #qerry each value directly to a new column
    DF_aps['neg_cnt'] = pd.read_sql(_SQL.format(market_tab = market_tabs[market]), con)

    
    #replace NaN by zeros
    DF_aps['neg_cnt'] = DF_aps['neg_cnt'].fillna(0)
    
    

    #bilungual dictionary for axes labeling
    lbls    = { 's_ttl' : { 'en': f'annual price statistics @{market}', 
                            'de': f'Jährliche Preistatistiken @{market}'},
                'ax0_tl': { 'en': 'annual average',
                            'de': 'Jahressmittel'},
                'ax1_tl': { 'en': 'dynamic price spreads for n-best hours', 
                            'de': 'Dynamische Preisspreads für die n-besten Stunden'},
                'ax2_tl': { 'en': 'frequency for negative prices', 
                            'de': 'Häufigkeit negateiver Preise'},
                'x_lbl' : { 'en': 'years', 'de':'Jahre'}}

    
    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = f'annual_price_statistics_{market}', 
                                    grc_subplots    = (3,1), 
                                    hr_subplots     = [1, 1, 1], 
                                    wr_subplots     = None, 
                                )


    #optional super titel for the whole diagram
    if title==True:
        fig.suptitle(lbls['s_ttl'][lang])
    
    
    #todo: try to compact the spacing between subplots
    #subplot for avg
    ax0     = fig.add_subplot(gs[0,0])
    DF_aps.plot(y=['avg'], lw=1, ax=ax0, color=['grey'], drawstyle='steps-mid', legend=None)
    ax0.set(title=lbls['ax0_tl'][lang], ylabel='[€/MWh]')
 

    #subplot for dynamic price spreads
    ax1     = fig.add_subplot(gs[1,0])
    DF_aps.plot.area(y=['b_18', 'b_12', 'b_6'], lw=0, ax=ax1)
    ax1.set(title=lbls['ax1_tl'][lang], ylabel='[€/MWh]')


    #subplot for negative prices
    ax2     = fig.add_subplot(gs[2,0])
    DF_aps.plot.bar(x='year', y=['neg_cnt'], width=1, ax=ax2, color=['darkcyan'], legend=None)
    ax2.set(title=lbls['ax2_tl'][lang], ylabel='[n]', xlabel=lbls['x_lbl'][lang])

    ax0.set_xlim(ax2.get_xlim()[0], ax2.get_xlim()[1])
    ax0.set_xticks(ax2.get_xticks(), labels=[])
    ax1.set_xlim(ax2.get_xlim()[0], ax2.get_xlim()[1])
    ax1.set_xticks(ax2.get_xticks(), labels=[])


    #add grids to all subplots
    for ax in fig.get_axes():
        ax.grid(ls=':', alpha=0.75)
    
    plt.subplots_adjust(left=0.055, bottom=0.175, right=0.995, top=0.9, hspace=0.33)

    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def daily_pricestats(year:int, market = 'EPEX', figsize=(12,6), dpi_save=150, lang='en', show=True, 
                                        save_png=False, title=True, footer='GPM & CC'):
    """ Create 3 stacked diagramms for daily price statistics based on a query on gpm.db.

    Parameters
    ----------
    year : int
        Year for the requestet diagramm.

    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    figsize:tuple
        figure sizes in inches

    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC'
    """
    
    #data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()

    #create an empty Data Frame for daily price statistics with the days for the requested year as index
    _SQL                = """SELECT distinct(date(dt_CET)) AS 'dt_CET' FROM T_ts WHERE year = {yr} """.format(yr=year)
    DF_dps              = pd.read_sql(_SQL, con) 
    #loop for max, avg, min price values
    mav = ['max', 'avg', 'min']
    
    market_tab = 'T_epex_da'
    
    #use support function to create an temporary table for hourly aggregated values from EXAA
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab = 'tT_exaa_hourly_avgs'
    
    
    
    for i in mav:
        #create SQL query statemanet for max, avg, min price values 
        _SQL = """  SELECT  {mav}(price)
                    FROM    {market_tab}
                    JOIN    T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE year = {yr}
                    AND price IS NOT 'nan'
                    GROUP BY date(dt_CET) """.format(mav=i, yr=year, market_tab = market_tab)
        
        #qerry each value directly to a new column
        DF_dps[i] = pd.read_sql(_SQL, con)
        
    #replace non-number data points for min / max values
    DF_dps.replace('-', 0, inplace = True)


    #loop for dynamic price spread (dyps) of best 18, 12 and 6 hours
    dyps = [18, 12, 6]
    
    for i in dyps:
        
        
        #create SQL query statemanet for max, avg, min price values 
        _SQL = """  SELECT avg(price) as {lb}
                        FROM
                            (SELECT year, price, date(dt_CET) AS 'dt_CET', rank () OVER (PARTITION BY date(dt_CET) ORDER BY price DESC) pricerank
                            FROM {market_tab}
                            JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                            WHERE year={yr} )
                        WHERE pricerank < {hr} + 1
    GROUP BY date(dt_CET)""".format(lb='b_' + str(i), yr=year, hr=i, market_tab = market_tab)
        
        #qerry each value directly to a new column
        DF_dps[f'b_{i}']    = pd.read_sql(_SQL, con)
        DF_dps[f'b_{i}']    = DF_dps[f'b_{i}'] - DF_dps['avg']


    #create SQL query statemanet for count von negative prices 
    _SQL = """  SELECT sum(CASE WHEN price < 0 THEN 1 ELSE 0 END) AS 'count_neg'
                FROM {market_tab}
                JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                WHERE year = {yr}
                GROUP BY date(dt_CET) """.format(yr=year, market_tab = market_tab)
    
    #qerry each value directly to a new column
    DF_dps['neg_cnt'] = pd.read_sql(_SQL, con)
    
    #replace NaN by zeros
    DF_dps['neg_cnt'] = DF_dps['neg_cnt'].fillna(0)


    #create SQL query statemanet for count von negative prices 
    _SQL = """  SELECT sum(price)
                FROM {market_tab}
                JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                WHERE year = {yr}
                AND price < 0
                GROUP BY date(dt_CET) """.format(mav=i, yr=year, market_tab = market_tab)
    
    #qerry each value directly to a new column
    DF_dps['neg_sum'] = pd.read_sql(_SQL, con)
    
    #replace NaN by zeros
    DF_dps['neg_sum'] = DF_dps['neg_sum'].fillna(0)

    #convert the dt_CET column to datetime format
    DF_dps['dt_CET']    = pd.to_datetime(DF_dps['dt_CET'])
    #DF_dps.index        = DF_dps['dt_CET'] 


    lbls    = { 's_ttl' : { 'en': f'daily price statistics for {year} @{market_tab}', 
                            'de': f'Tägliche Preistatistiken für  {year} @{market_tab}' },
                'ax0_tl': { 'en': 'maximum / average / minimum',
                            'de': 'Maximum / Tagesmittel / Minimum'},
                'ax1_tl': { 'en': 'dynamic price spreads for n-best hours', 
                            'de': 'Dynamische Preisspreads für die n-besten Stunden'},
                'ax2_tl': { 'en': 'frequency for negative prices', 
                            'de': 'Häufigkeit negateiver Preise'},
                'x_lbl' : { 'en': 'months', 'de':'Monate'}}

    
    #set up figure properties
    fig, gs     = figure_creator(   figsize         = figsize, 
                                    window_title    = 'daily_price_statistics', 
                                    grc_subplots    = (3,1), 
                                    hr_subplots     = [1, 1, 1], 
                                    wr_subplots     = None, 
                                )
    
    
    #optional plotting super title
    if title==True:
        fig.suptitle(lbls['s_ttl'][lang])

    #subplot for max, avg, min
    ax0     = fig.add_subplot(gs[0,0])
    DF_dps.plot(y=['max', 'avg', 'min'], lw=1, ax=ax0, color=['red', 'grey', 'blue'])
    ax0.set(title=lbls['ax0_tl'][lang], ylabel='[€/MWh]') 
    

    #subplot for dynamic price spreads
    ax1     = fig.add_subplot(gs[1,0], sharex = ax0)
    DF_dps.plot.area(y=['b_18', 'b_12', 'b_6'], lw=0, ax=ax1)
    # DF_dps.plot.bar(y=['b_6'], width=1, ax=ax1, color='tab:green')
    # DF_dps.plot.bar(y=['b_12'], width=1, ax=ax1, color='tab:blue')
    # DF_dps.plot.bar(y=['b_18'], width=1, ax=ax1, color='tab:orange')
    ax1.set(title=lbls['ax1_tl'][lang], ylabel='[€/MWh]')


    #subplot for negative prices
    ax2     = fig.add_subplot(gs[2,0], sharex=ax0)
    DF_dps.plot.bar(y=['neg_cnt'], width=1, ax=ax2, color=['darkcyan'])
    ax2.set(title=lbls['ax2_tl'][lang], ylabel='[n]')

    
    #add grids to all subplots
    for ax in fig.get_axes():
        ax.grid(ls=':', alpha=0.75)
    
    # reduced ticks (days) and labels (months) for the wohle plot 
    ticks = []
    labels = []
    for i, ts in enumerate(DF_dps['dt_CET']):
        if i == 0 or ts.year != DF_dps['dt_CET'][i-1].year:
            ticks.append(i)
            labels.append(ts.strftime('%b\n%Y'))
        elif ts.month != DF_dps['dt_CET'][i-1].month:
            ticks.append(i)
            labels.append(ts.strftime('%b'))
    
    # Set major ticks and labels
    ax2.set_xticks(ticks)
    ax2.set_xticklabels(labels)
    
    
    plt.subplots_adjust(left=0.055, bottom=0.175, right=0.995, top=0.9, hspace=0.33)

    #add footer content
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)





def price_timeseries(year:int, market = 'EPEX', lang='en', show=True, save_png=False, title=True, 
                        figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """ Plotting a price average('s) for a single year.

    Parameters:
    -----------
    year:int
        selected year for computing averages

    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.
        
    footer:str
        definition of footer content: 'GPM' - GermanPowerMarket sources | 'CC' - Creative Commons
    """

    #create data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    
    market_tab = 'T_epex_da'
    
    #create an temporary database table for hourly aggregated prices if market == EXAA
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab = 'tT_exaa_hourly_avgs'
        
        
    #query the base price for one year
    _SQL        = """   SELECT 	{market_tab}.PK_ts_UTC      AS ts_UTC,
                                T_ts.dt_UTC                 AS dt_UTC,
                                T_ts.dt_CET                 AS dt_CET,
                                T_ts.FK_month               AS month,
                                T_ts.FK_season              AS season,
                                T_ts.day                    AS day,
                                T_ts.FK_wday                AS wday,
                                T_ts.hour                   AS hour,
                                T_ts.minute                 AS min,
                                {market_tab}.price          AS price
                
                        FROM {market_tab}
                        JOIN T_ts 				ON T_ts.PK_ts_UTC = {market_tab}.PK_ts_UTC
                        
                        WHERE year == '{yr}'
                    """
    
    
    DF              = pd.read_sql(_SQL.format(yr=year, market_tab = market_tab), con)
    DF['dt_CET']    = pd.to_datetime(DF['dt_CET'])
    
    
    wddict  =  {    'Mon'   : 'Mo',
                    'Tue'   : 'Di',
                    'Wed'   : 'Mi',
                    'Thu'   : 'Do',
                    'Fri'   : 'Fr',
                    'Sat'   : 'Sa',
                    'Sun'   : 'So' }
    
    avg_ssd = { 'EPEX' : { 'en' : '', 'de' : '' }, 
                'EXAA' : {'en' : '(hourly averages)', 'de' : '(Stundenmittel)'}}
    
    lbls    = { 'title' : { 'en' : f'power prices for {year} {avg_ssd[market][lang]} @{market}', 
                            'de' : f'Strom Preise für {year} {avg_ssd[market][lang]} @{market}'},
                'ax2tl' : { 'en' : f'overview for the whole year (use slider to move selected range)', 
                            'de' : f'Überblick für das Gesamtjahr (Schieberegler für Verschieben der Auswahl'},
                'lgnd'  : { 'en' : 'legend', 'de':'Legende'},
                'xlbl'  : { 'en' : 'date', 'de':'Datum'},
                'ylbl'  : { 'en' : 'EPEX-Spot [€/MWh]', 'de':'EPEX-Spot [€/MWh]'},
                'sldr'  : { 'en' : 'day \nslider', 
                            'de' : 'Tag \nSchieber'}
                }
    
    fig, gs = figure_creator(   figsize=figsize, 
                                window_title=f'price_averages_{year}', 
                                grc_subplots=(3,1), 
                                hr_subplots=[4,0.5,1]
                            )
                                
    ax0     = fig.add_subplot(gs[0,0])
    ax1     = fig.add_subplot(gs[1,0])
    ax2     = fig.add_subplot(gs[2,0])
    
    #axis for the Export-Button
    axb     = fig.add_axes([0.35, 0.0125, 0.3, 0.05], facecolor='silver')
    
    range_slider    = Slider(ax1, lbls['sldr'][lang], 0, 365 - 7, valinit=0, valstep=list(range(365 - 7)))
    export_button   = Button(axb, f'Export --> gpm_dbtb/xls/export_EPEX-SPOT_{year}.xlsx', color='silver', hovercolor='0.9')
    
    #axis modifications
    if title==True:
        fig.suptitle(lbls['title'][lang])
    
    
    ax0.step(DF['dt_CET'], DF['price'], lw=1, color='black')
    ax0_sec     = ax0.secondary_xaxis('top')
    dates       = [dt.date.fromtimestamp(d*3600*24).strftime('%a') for d in ax0.get_xticks()]
    if lang == 'de':
        dates = [wddict[d] for d in dates]
    ax0_sec.set_ticks(ax0.get_xticks(), dates)
    
    
    ax1.axis('off')
    
    ax2.plot(DF['dt_CET'], DF['price'], lw=1, color='black')
    ax2.set_title(lbls['ax2tl'][lang])
    
    ax0.set(xlabel=lbls['xlbl'][lang], ylabel=lbls['ylbl'][lang])
    ax0.grid(ls=':', alpha=0.5)
    
    ts0     = DF.loc[0, 'ts_UTC']/3600/24
    tsn     = DF.loc[DF.index[-1], 'ts_UTC']/3600/24

    
    ax0.set_xlim(ts0, ts0+7)
    ax2.set_xlim(ts0, tsn)
    
    
    ymin, ymax    = ax2.get_ylim()

    rect = Rectangle((ts0,ymin), 7, ymax - ymin, color='blue', alpha=0.33)
    ax2.add_patch(rect)
    
    
    
    def slider_interaction(val, ref0=ts0, ref1=ts0+7):
    
        shift       = range_slider.val
        x_l         = ref0 + shift
        x_r         = ref1 +shift
        ax0.set_xlim([x_l, x_r])
        rect.set(x=x_l)
        dates       = [dt.date.fromtimestamp(d*3600*24).strftime('%a') for d in ax0.get_xticks()]
        if lang == 'de':
            dates = [wddict[d] for d in dates]
        ax0_sec.set_ticks(ax0.get_xticks(), dates)
        
        fig.canvas.draw_idle()
    

    def export_interaction(mouse_event):
        xl_path = aux.os.path.join(aux.var_script_dir, 'xls', f'export_{market}-SPOT_{year}.xlsx')
        DF.to_excel(xl_path, 'export')
    
    range_slider.on_changed(slider_interaction)
    export_button.on_clicked(export_interaction)  


    plt.subplots_adjust(left=0.055, bottom=0.15, right=0.995, top=0.9, hspace=0.25)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)
    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def price_averages(year:int, market = 'EPEX', specifiers:list = ['m', 'y'], benchmark=None, lang='en', 
                    show=True, save_png=False, title=True, rtrn=False, figsize=(13,6), dpi_save=150,
                     ylim=None, footer='GPM & CC'):
    """ Plotting a price average('s) for a single year.

    Parameters:
    -----------
    year:int
        selected year for computing averages

    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    specifiers:list
        Specifiers which averages should be plotted, allowed specifiers ['d', 'w', 'm', 'y']
        Special specifiers for also annotate averages ['w+', 'm+', 'y+']
        
    benchmark:float
        insert a dotted line for a given benchmark in €/MWh

    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    rtrn:bool
        Argument if the datatable should be returned
        
    figsize:tuple
        Dimensions of the figure in inches.
    
    ylim:float
        Limits for the y-axis if required.
        
    footer:str
        definition of footer content: 'GPM' - GermanPowerMarket sources | 'CC' - Creative Commons
    """

    #create data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    
    views_d = { 'EPEX' : 'V_epex_da', 'EXAA' : 'V_exaa_da'}
    
    #query the base price for one year
    _SQL        = """   SELECT 	DISTINCT(strftime('%Y-%m-%d %H:%M',dt_CET))                             AS 'dt'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('%Y-%m-%d %H:00', dt_CET) )   AS 'avg_hours'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('%j', dt_CET) )   AS 'avg_days'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('%W', dt_CET) )   AS 'avg_weeks'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('%m', dt_CET) )   AS'avg_months'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('%Y', dt_CET) )   AS 'avg_year'
                                
                        FROM {view}
                        WHERE strftime('%Y', dt_CET) == '{yr}'
                    """
    
    
    DF          = pd.read_sql(_SQL.format(yr=year, view = views_d[market]), con)
    DF['dt']    = pd.to_datetime(DF['dt'])
    
    
    #dictionary for specifiers { 'key' : (avg_col, color, line width) }
    spec_dict   = { 'h' : ('avg_hours', 'cornflowerblue',   0.5),
                    'd' : ('avg_days',  'turquoise',        1  ),
                    'w' : ('avg_weeks', 'lime',             1.5),
                    'm' : ('avg_months','darkorange',       2  ),
                    'y' : ('avg_year',  'dimgrey',          2.5)}
    
    
    #bilingual dicitonary for labeling
    if benchmark != None:
         bm_lbl = { 'en' : 'benchmark', 'de' : 'Vergleichswert'}
         bm_str = f'| {bm_lbl[lang]} {benchmark:.0f} €/MWh'
         
    else:
        bm_str = ''
    
    lbls    = { 'title' : { 'en' : f'Price averages for {year} {bm_str} @{market}', 
                            'de' : f'Durchschnittspreise für {year} {bm_str} @{market}'},
                'y'     : { 'en' : 'annual mean', 'de':'Jahresmittel'},
                'm'     : { 'en' : 'monthly mean', 'de':'Monatsmittel'},
                'w'     : { 'en' : 'weekly mean', 'de':'Wochenmittel'},
                'd'     : { 'en' : 'daily mean', 'de':'Tagesmittel'},
                'h'     : { 'en' : 'hourly mean', 'de':'Stundenmittel'},
                'lgnd'  : { 'en' : 'legend', 'de':'Legende'},
                'xlbl'  : { 'en' : 'date', 'de':'Datum'},
                'ylbl'  : { 'en' : 'EPEX-Spot [€/MWh]', 'de':'EPEX-Spot [€/MWh]'}}
    
    fig, gs = figure_creator(   figsize=figsize, 
                                window_title=f'price_averages_{year}'
                            )
    ax  = fig.add_subplot(gs[0,0]) 
    
    if title==True:
        ax.set_title(lbls['title'][lang])
    
    for sp in specifiers:
        ax.step( DF['dt'], DF[spec_dict[sp[0]][0]], where='mid', label=lbls[sp[0]][lang],
                    color=spec_dict[sp[0]][1], lw=spec_dict[sp[0]][2])
    
    if benchmark != None:
        ax.plot( DF['dt'], [benchmark] * len(DF['dt']), label=bm_lbl[lang], ls=':', lw=2, color='red')
    
    #axis modifications
    if title==True:
        ax.set_title(lbls['title'][lang])
    ax.set(xlabel=lbls['xlbl'][lang], ylabel=lbls['ylbl'][lang])
    ax.grid(ls=':', alpha=0.5, axis='y')
    ax.legend(title=lbls['lgnd'][lang])
    ax.set(xlabel=lbls['xlbl'][lang], ylabel=lbls['ylbl'][lang])
    ax.grid(axis='y', ls=':', alpha=0.5)
    
    if ylim != None:
        ax.set_ylim(ylim)


    #insert annotations for special specifiers
    if 'y+' in specifiers:
        mean_y = DF.loc[0, 'avg_year']
        ax.annotate(f'{mean_y:.0f} ', (DF.loc[0, 'dt'], mean_y), ha='right', va='center')
                                        #,bbox=dict(boxstyle='round,pad=0.5', fc='white', alpha=0.66))
                                        
    if 'm+' in specifiers:
        for nm in range(1,13,1):
            date_str    = f'{year}-{nm:02d}-15'
            date        = DF.query(f"dt == '{date_str}' ")['dt'].iloc[0]
            mean_m      = DF.query(f"dt == '{date}' ")['avg_months'].iloc[0]
            ax.annotate(f'{mean_m:.0f}', (date, mean_m), ha='center', va='bottom')
        
        
    plt.subplots_adjust(left=0.055, bottom=0.15, right=0.995, top=0.95, hspace=0.33)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)
    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)
    
    
    if rtrn == True:
        return(DF_avgs)

    


def price_volatility(years:list, market = 'EPEX', outliners=False, lang='en', show=True, save_png=False, 
                            title=True, figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """ Plotting a price duration curve for one or several years.

    Parameters:
    -----------
    years:list
        list of years as integers

    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    outliners:bool
        parameter to chose if outliners (above 95%-quantile and below 5%-quantile) should be plotted 

    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
        
    figsize:tuple
        Dimensions of the figure in inches.
    
    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 

    Outputs:
    --------
    BP_results
        Box-Plots results as dictionary for ['whiskers', 'caps', 'boxes', 'medians', 'fliers', 'means']
    """
    
    
    #convert single year argument into a list
    if type(years)!=list:                    
        years=[int(years)]

    
    #create data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    
    
    #query the base price for one year
    _SQL        = """   SELECT price
                        FROM {market_tab}
                        JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                        WHERE year={y} """
    
    BP_dict = {}
    
    for yr in years:

        con_c.execute(_SQL.format(y=yr, market_tab = market_tabs[market]))
        fetch       = con_c.fetchall()
        fetchl      = [x[0] for x in fetch]
        fetchl_cl   = [x for x in fetchl if type(x) == float]
 
        BP_dict[yr]  =  fetchl_cl
        
    if outliners == True:
        BP_qntls        = ' Outliners··· 95|--|75 Median 25|--|5 ···Outliners' 
    else:
        BP_qntls        = ' 95|--|75 Median 25|--|5'
    
    #bilingual dicitonary for labeling
    lbls    = { 'title' : { 'en' : f'Price volatility for EPEX-Spot [€/MWh] / Box-Plot quantiles:{BP_qntls} @{market}', 
                            'de' : f'Preisvolatilität für EPEX-Spot [€/MWh] / Box-Plot Quantile:{BP_qntls} @{market}'},
                'xlbl'  : { 'en' : 'year(s)', 'de':'Jahr(e)'},
                'ylbl'  : { 'en' : 'EPEX-Spot [€/MWh]', 'de':'EPEX-Spot [€/MWh]'}}
    
    fig, gs = figure_creator(   figsize=figsize, 
                                window_title=f'price_volatility', 
                            )
                            
    ax  = fig.add_subplot(gs[0,0]) 

    ax.boxplot(BP_dict.values(), showfliers=outliners)
    ax.set_xticklabels(BP_dict.keys())
    
    #axis modifications
    if title==True:
        ax.set_title(lbls['title'][lang])
    
    ax.set(xlabel=lbls['xlbl'][lang], ylabel=lbls['ylbl'][lang])
    ax.grid(axis='y', ls=':', alpha=0.5)


    plt.subplots_adjust(left=0.055, bottom=0.15, right=0.995, top=0.95, hspace=0.33)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)
    




def price_spreads(  years:list, market = 'EPEX', reference:str = 'm', best_hrs:list=None, PQ=True, 
                    exp2xlsx=False, lang='en', show=True, save_png=False, title=True, figsize=(13,7), 
                    dpi_save=150, footer='GPM & CC'):
    """ Plotting curves for dynamic price spreads of one or several years.

    Parameters:
    -----------
    years:list
        list of years as integers
        
    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    reference:str
        Reference of aggregated average, element of ['d', 'w', 'm', 'y'] 
        (daily, weekly, monthly, yearly), base price calculation for building spreads

    best_hrs:list
        list of the hours for which the average dynamic spread shoul be calulated, if None
        best_hrs=[0, ... ,23]

    exp2xlsx:bool | str =False
        encoding if the generated DataFrame should be exported to a xlsx-file or not.
    
    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 

    Outputs:
    --------
        Create and save an MS-Excel-file to the folder ...//xls
    """


    #convert single year argument into a list
    if type(years)!=list:                    
        years=[int(years)]


    #if no argument for best hours is given, use all
    if best_hrs==None:                      
        best_hrs=list(range(1,25))
        
    
    #initiate DataFrame
    DF_ps       = pd.DataFrame(data=None,index=best_hrs, columns=years)
    DF_ps['PQ'] = 24 / DF_ps.index 
    DF_ps.index.name = 'h/d'

    
    #create data base connection and cursor
    con     = sq3.connect(db_path)


    market_tab = 'T_epex_da'
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab = 'tT_exaa_hourly_avgs'
    
    
    ref_dict    = { 'd' : '%j',
                    'w' : '%W',
                    'm' : '%m',
                    'y' : '%Y'}
    
    
    #query the price averages for one year with hourly resolution
    _SQL        = """   SELECT 	DISTINCT strftime('%j', dt_CET)                                             AS 'dist_day'
                                ,strftime('%Y-%m-%d', dt_CET)                                               AS 'date'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('{ref}', dt_CET) )    AS 'rank_avg'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('{ref}', dt_CET) )
                                - avg(ref_avg) OVER ( PARTITION BY strftime('{ref}', dt_CET) )              AS 'spread'
                                
                                
                        FROM	(SELECT strftime('%Y-%m-%d %H:%M:%S',dt_CET)                                    AS 'dt_CET'
                                        ,price
                                        ,rank () OVER (PARTITION BY strftime('%Y-%m-%d',dt_CET) ORDER BY price) AS 'rank'
                                        ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('{ref}', dt_CET) )AS 'ref_avg'
                                FROM {market_tab}
                                JOIN T_ts ON {market_tab}.PK_ts_UTC = T_ts.PK_ts_UTC
                                WHERE strftime('%Y', dt_CET) == '{yr}'
                        
                                ORDER BY dt_CET)
                                
                        WHERE rank > {rnk}
                    """ 
    
    for year in years:
        for bh in best_hrs:
    
            DF_qry      = pd.read_sql(_SQL.format(yr=year, ref= ref_dict[reference], rnk = 24 - bh, 
                                        market_tab = market_tab), con)
            DF_ps.loc[bh,year]    = DF_qry['spread'].mean()

    DF_ps[years]    = DF_ps[years].astype(float)
    DF_ps           = DF_ps.round(2)
    
    #conditional exporting the spreads to an xlsx-file in the certain subfolder
    if exp2xlsx==True:
        DF_ps.to_excel('xls/Spread_Analysis.xlsx') #export the spreads to an output-excel file
        
    elif type(exp2xlsx) == str:
         DF_ps.to_excel(exp2xlsx) #export the spreads to an output-excel file


    ref_str = { 'd' : { 'en' : 'daily averages',   
                        'de' : 'Tagesmittel'},
                'w' : { 'en' : 'weekly averages',   
                        'de' : 'Wochenmittel'},
                'm' : { 'en' : 'monthly means',   
                        'de' : 'Monatsmittel'},
                'y' : { 'en' : 'annual average',
                        'de' :'Jahresmittel'}
            }

    lbls    = { 'title' : { 'en' : f'price spreads for n most expenisve hours (base: {ref_str[reference][lang]}) @{market}', 
                            'de' : f'Preisspannen für die n teuersten Stunden (Bezug: {ref_str[reference][lang]}) @{market}'},
                'xlbl'  : { 'en' : 'n expenisve hours per day',
                            'de' : 'n teuerste Stunden pro Tag'},
                'ylbl0' : { 'en' : 'average price spread [€/Mwh]', 
                            'de' : 'mittlere Preisspanne [€/Mwh]'},
                'ylbl1' : { 'en' : 'power quotient (PQ) [-]', 
                            'de' : 'Leistungsuqotient (PQ) [-]'}}


    #define title variable
    if title==True:
        title=lbls['title'][lang]
    else:
        title=None
    
    #plot results
    fig, gs = figure_creator(   figsize=figsize, 
                                window_title=f'price_spreads'
                            )
                            
    ax1     = fig.add_subplot(gs[0,0]) 
    # ax2     = fig.add_subplot(gs[0,0]) 


    ax1.plot( DF_ps.index, DF_ps[years], label=years)
    ax1.set_title(title)
    ax1.set_xlabel(lbls['xlbl'][lang])
    ax1.set_ylabel(ylabel=lbls['ylbl0'][lang])
    ax1.set_xlim(0.75, 24.25)
    ax1.set_xticks([2,4,6,8,10,12,14,16,18,20,22,24])
    ax1.grid(ls=':', alpha=0.66, which='both')
    ax1.legend()
                  
    
    lns1, lbls1 = ax1.get_legend_handles_labels()
    
    if PQ == True:
        ax2 = ax1.twinx()
        
    
        ax2.step(DF_ps.index, DF_ps['PQ'], ls=':', label='PQ', color='magenta', where='mid')
        ax2.set_ylabel(ylabel=lbls['ylbl1'][lang])
        ax2.set_ylim(0, 12)
    
        yticks_align(ax1, ax2)
        
        lns2, lbls2 = ax2.get_legend_handles_labels()
        ax1.legend(lns1 + lns2, lbls1 + lbls2)

    else:
        ax1.legend(lns1, lbls1)


    plt.subplots_adjust(left=0.05, bottom=0.15, right=0.95, top=0.9, hspace=0.33)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)
    
    


def price_spreads_disaggregate(year:int, market = 'EPEX', resolution:str = 'm', best_hrs:list=[4,8], 
                                sort=False, exp2xlsx=False, lang='en', show=True, save_png=False, 
                                title=True,  figsize=(15,7), dpi_save=150, footer='GPM & CC'):
    """ Plotting curves for dynamic price spreads of one or several years.

    Parameters:
    -----------
    year:int
        year
        
    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    resolution:str
        Period for aggregation, element of ['d', 'w', 'm'] (daily, weekly, monthly)
        
    best_hrs:list = [2,4,6,8,12]
        list of the hours for which the average dynamic spread shoul be calulated, if None
        best_hrs=[0, ... ,23]
        
    sort:bool = False
        Argument for sorting the values: False | 'ASC' | 'DSC'

    exp2xlsx:bool | str =False
        encoding if the generated DataFrame should be exported to a xlsx-file or not.
    
    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 

    """
        
    
    #initiate DataFrame
    DF_ps       = pd.DataFrame(data=None,index=range(366), columns=best_hrs)
    DF_ps.index.name = 'p'

    
    #aggregation dictionary
    
    agd         = {     'd' : { 'en' : 'days',      'de' : 'Tage'   },
                        'w' : { 'en' : 'weeks',     'de' : 'Wochen' }, 
                        'm' : { 'en' : 'months',    'de' : 'Monate' }
                    }
    
    
    #create data base connection and cursor
    con     = sq3.connect(db_path)


    market_tab = 'T_epex_da'
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab = 'tT_exaa_hourly_avgs'

    
    #query the price averages for one year with hourly resolution
    _SQL        = """   SELECT 	DISTINCT strftime('%j', dt_CET)                                             AS 'dist_day'
                                ,strftime('%Y-%m-%d', dt_CET)                                               AS 'date'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('%j', dt_CET) )    AS 'rank_avg'
                                ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('%j', dt_CET) )
                                - avg(ref_avg) OVER ( PARTITION BY strftime('%j', dt_CET) )              AS 'spread'
                                
                                
                        FROM	(SELECT strftime('%Y-%m-%d %H:%M:%S',dt_CET)                                    AS 'dt_CET'
                                        ,price
                                        ,rank () OVER (PARTITION BY strftime('%Y-%m-%d',dt_CET) ORDER BY price) AS 'rank'
                                        ,avg(nullif(price, '-')) OVER ( PARTITION BY strftime('%j', dt_CET) )AS 'ref_avg'
                                FROM {market_tab}
                                JOIN T_ts ON {market_tab}.PK_ts_UTC = T_ts.PK_ts_UTC
                                WHERE strftime('%Y', dt_CET) == '{yr}'
                        
                                ORDER BY dt_CET)
                                
                        WHERE rank > {rnk}
                    """ 

    
    
    for bh in best_hrs:
        DF_qry      = pd.read_sql(_SQL.format(yr=year, rnk = 24 - bh, market_tab = market_tab), con)
        
        if bh == best_hrs[0]:
            DF_ps['days']   = DF_qry['dist_day']
            DF_ps['date']   = pd.to_datetime(DF_qry['date'])
            DF_ps['isoy']   = DF_ps.date.dt.isocalendar().year
            DF_ps['weeks']  = DF_ps.date.dt.isocalendar().week
            DF_ps['weeks']  = DF_ps.weeks.where((DF_ps.isoy == year), 0)
            DF_ps['months'] = DF_ps.date.dt.month
            
        DF_ps.loc[:,bh]     = DF_qry['spread']


    if DF_ps.loc[365, 'weeks'] == 0:
        DF_ps = DF_ps.drop(365, axis=0)
        
    DF_ps['months']         = DF_ps['months'].astype(int)
    DF_ps[best_hrs]         = DF_ps[best_hrs].astype(float)
    DF_ps[best_hrs]         = DF_ps[best_hrs] .round(2)
    
    #conditional exporting the spreads to an xlsx-file in the certain subfolder
    if exp2xlsx==True:
        DF_ps.to_excel('xls/Disaggregated_Sprea_Analysis.xlsx') #export the spreads to an output-excel file
        
    elif type(exp2xlsx) == str:
         DF_ps.to_excel(exp2xlsx) #export the spreads to an output-excel file
         
    
    #create DataFrame for plotting
    DF_plt          = DF_ps.groupby(agd[resolution]['en'])[best_hrs].mean()
    DF_plt.index    = DF_plt.index.astype(int)
        
    
    if sort != False:
        
        xlbl_suffix_en = ' | sorted'
        xlbl_suffix_de = ' | sortiert'
        
        if sort == 'ASC':
            sort_asc = True
        else:
            sort_asc = False
    
    else:
        xlbl_suffix_en = ''
        xlbl_suffix_de = ''


    lbls    = { 'title' : { 'en' : f'disaggregated price spreads for {year} (on a daly base) @{market}', 
                            'de' : f'Disaggregierte Preisspannen für {year} (auf Tagesbasis) @{market}'},
                'xlbl'  : { 'en' : agd[resolution]['en'] + xlbl_suffix_en,
                            'de' : agd[resolution]['de'] + xlbl_suffix_de},
                'ylbl0' : { 'en' : 'price spread [€/Mwh]', 
                            'de' : 'Preisspanne [€/Mwh]'},
                'aavg'  : { 'en' : 'annual mean', 
                            'de' : 'Jahresmitel'} 
                }


    #define title variable
    if title==True:
        title=lbls['title'][lang]
    else:
        title=None
    
    #plot results
    fig, gs = figure_creator(   figsize=figsize, window_title=f'price_spreads')
    ax1     = fig.add_subplot(gs[0,0]) 


    ax1.hlines(0, DF_plt.index.min(), DF_plt.index.max(), ls=':', color='black', label=lbls['aavg'][lang])
    
    
    for i, bh in enumerate(best_hrs):
        
        if sort != False:
            DF_srt          = DF_plt.sort_values(bh, ascending = sort_asc, ignore_index=True)
            DF_srt.index    = DF_srt.index + 1
            
        else:
            DF_srt = DF_plt

        
        lbl = f'PQ: 24/{bh} = {24/bh:.2f}'
        col = cmp.tab10(i)
        
        ax1.step(DF_srt.index, DF_srt[bh], label=lbl, color=col, where='mid')
        
        ax1.hlines(DF_srt[bh].mean(),DF_srt.index.min(), DF_srt.index.max(), ls=':', color=col)
    
    
    ax1.set_title(title)
    ax1.set_xlabel(lbls['xlbl'][lang])
    ax1.set_ylabel(ylabel=lbls['ylbl0'][lang])
    ax1.set_xlim(DF_srt.index.min(), DF_srt.index.max())
    ax1.set_ylim(0, ((DF_plt[best_hrs].max().max() // 10) + 1) *10)
    ax1.grid(ls=':', alpha=0.66, which='both')
    ax1.legend()
                  
    
    ax1.legend()

    plt.subplots_adjust(left=0.05, bottom=0.15, right=0.99, top=0.95, hspace=0.33)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def cherry_picking(years:list, market = 'EPEX', which='both', exp2xlsx=False, lang='en', show=True, 
                    save_png=False, title=True, figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """ Plotting steps-curves for average prices n-best hours perday of one or several years.

    Parameters:
    -----------
    years:list
        list of years as integers

    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    which:str
        select which kind of "cherries" should be picked
        'both' | 'expensive' | 'cheap'

    exp2xlsx=False
        encoding if the generated DataFrame should be exported to a xlsx-file or not.
    
    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
    
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 

    Outputs:
    --------
        Create and save an MS-Excel-file to the folder ...//xls
    """


    #convert single year argument into a list
    if type(years)!=list:                    
        years=[int(years)]


    #if no argument for best hours is given, use all
    exp_hrs     = list(range(1,25))
    chp_hrs     = list(reversed(exp_hrs))
    
    exp_lbl     = [''] + [f'{x}' for x in exp_hrs]
    chp_lbl     = [''] + [f'{x}' for x in chp_hrs]
    
    #initiate DataFrames
    DF_exp  = pd.DataFrame(data=None,index=exp_lbl, columns=years) 
    DF_exp.columns.name = '€/MWh'
    DF_chp  = pd.DataFrame(data=None,index=chp_lbl, columns=years)
    DF_chp.columns.name = '€/MWh'
    
    #create data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    
    market_tab = 'T_epex_da'
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab = 'tT_exaa_hourly_avgs'
        

    _SQL    =  """ SELECT avg(price)
                    FROM
                        (   SELECT  year, 
                                    price, 
                                    substr(dt_CET,1,10), rank () OVER (PARTITION BY substr(dt_CET,1,10) ORDER BY price) pricerank
                            FROM {market_tab}
                                JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                            WHERE year={y}
                            AND price != '-' )
                    WHERE pricerank{cond}"""
    
    for yr in years:
        
        #query the n best hours and add get their average level
        for i in range(0,24):
            
            #query for the n most expensive hours average
            con_c.execute(_SQL.format(cond=f'> 24 -{exp_hrs[i]}', y=yr, market_tab=market_tab))
            exp_avg  = con_c.fetchone()
            DF_exp.loc[exp_lbl[i + 1],yr]    = exp_avg[0]

            #query for the n most cheapest hours average
            con_c.execute(_SQL.format(cond=f'<= {chp_hrs[i]}', y=yr, market_tab=market_tab))
            chp_avg  = con_c.fetchone()
            DF_chp.loc[chp_lbl[i + 1],yr]    = chp_avg[0]

    
    #conditional exporting the spreads to an xlsx-file in the certain subfolder
    if exp2xlsx==True:
        writer      = pd.ExcelWriter('xls/cherry_picking.xlsx')
        DF_exp.iloc[1::].to_excel(writer, sheet_name='most_expenisve', index_label=DF_exp.columns.name)
        DF_chp.iloc[1::].to_excel(writer, sheet_name='cheapest', index_label=DF_chp.columns.name)
        writer.save()        
        

    #bilingual labels dictionary
    lbls    = { 'supt'  : { 'en' : f'daily averages for most expenisve and cheapest hours @{market}', 
                            'de' : f'Tagesdurchnschnittspreise für die teuersten und günstigsten Stunden @{market}'},
                't_exp' : { 'en' : 'daily averages for most expenisve hours', 
                            'de' : 'Tagesdurchnschnittspreise der teuersten Stunden'},
                't_chp' : { 'en' : 'daily averages for cheapest hours', 
                            'de' : 'Tagesdurchnschnittspreise  der günstigsten Stunden'},
                'xl_exp': { 'en' : 'n expenisve hours per day',
                            'de' : 'n teuerste Stunden pro Tag'},
                'xl_chp': { 'en' : 'n cheapest hours per day',
                            'de' : 'n günstigste Stunden pro Tag'},
                'ylbl'  : { 'en' : 'average price [€/Mwh]', 
                            'de' : 'Durchschnittspreis [€/Mwh]'}}


    nc_dict     = {'both' : 2, 'expensive' : 1, 'cheap' : 1}

    
    #create figure
    fig, gs = figure_creator(   figsize=figsize, 
                                window_title=f'price_cherry_picking',
                                grc_subplots=(1, nc_dict[which])
                            )
    
    
    #optional plotting super title
    if title==True and which == 'both':
        fig.suptitle(lbls['supt'][lang])

    
    #plot expensive pickings
    if which == 'both' or which == 'expensive':
        ax_e  = fig.add_subplot(gs[0,0])
        DF_exp.plot(title=lbls['t_exp'][lang], ax=ax_e, drawstyle='steps-mid', \
                    xlabel=lbls['xl_exp'][lang], ylabel=lbls['ylbl'][lang], grid=True, \
                    xlim=(0,25), xticks=list(range(0,26,3)))
                    
        ax_e.set_xlim(1,24)


    #plot cheap pickings    
    if which == 'both' or which == 'cheap':
        if which == 'both':
            ax_c  = fig.add_subplot(gs[0,1], sharey=ax_e)
        else:
            ax_c  = fig.add_subplot(gs[0,0])
            
        DF_chp.plot(title=lbls['t_chp'][lang], ax=ax_c, drawstyle='steps-mid', \
                    xlabel=lbls['xl_chp'][lang], ylabel=lbls['ylbl'][lang], grid=True, \
                    xlim=(0,25), xticks=list(range(1,25,3)))
        if which == 'both':
            ax_c.tick_params(bottom=True, top=False, left=False, right=True, \
            labelbottom=True, labeltop=False, labelleft=False, labelright=True, )


        ax_c.set_xlim(1,24)


    plt.subplots_adjust(left=0.05, bottom=0.15, right=0.95, top=0.9, wspace=0.05)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)
    
    
    #conditional showing and or saving to png
    show_save(show, save_png)





def price_heatmap(year:int, market = 'EPEX', half_range=0, lang='en', show=True, save_png=False, title=True,
                    figsize=(13,6), dpi_save=150, footer='GPM & CC', cmap='RdBu_r'):
    """ Function for plotting a heat map of prices for a single year.

    Parameters
    ----------
    year:int
        Input year which should be plottet

    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    half_range=0:int
        Define the half range +/- €/MWh for the price scale. If == 0, then half range is computed.

    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """


    #create data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()


    market_tab = 'T_epex_da'
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab = 'tT_exaa_hourly_avgs'
        

    #querry price data and datetime for CET
    _SQL    = """   SELECT price, dt_CET
                    FROM {market_tab}
                    JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE strftime('%Y', dt_CET)='{y}'
                    AND price IS NOT '-' """.format(y=year, market_tab = market_tab)
    con_c.execute(_SQL)
    f_price=con_c.fetchall()

    
    #create nested list from fetchall
    nl_price=[]                             
    sublist=[]
    
    
    #disaggregate daily prices (24h)
    for i in f_price:                       
        if int(i[1][11:13])==23 and len(sublist)==22:       #2nd value insert for spring time shift
            sublist.insert(2,sublist[2])
            sublist.append(i[0])
            nl_price.append(sublist)
            sublist=[]
        elif int(i[1][11:13])==23 and len(sublist)==24:      #2nd value pop for autumn time shift
            sublist.append(i[0])
            nl_price.append(sublist)
            sublist.pop(1)
            sublist=[]
        elif int(i[1][11:13])==23:
            sublist.append(i[0])
            nl_price.append(sublist)
            sublist=[]
        else:
            if i[0]=='':
                sublist.append(sublist[-1]) #last value for autumn time shift
            else:
                sublist.append(i[0])


    #transfering and processing data within a numpy array
    a_price=np.array(nl_price).T                                    #array for regular values
    a_price=np.array(a_price, dtype=float)                          #NULL-values to numpy nan
    a_price=np.nan_to_num(a_price)                                  #numpy nan to ZEROS
    
    #compute half range based on min / max if argument for half_range == 0
    if half_range == 0 and type(half_range)  == int:
        scaling         = 25
        threshold       = 0.9
        lower_scales    = abs(a_price.min()) // scaling + 1
        upper_scales    = abs(a_price.max()) // scaling + 1
        half_range      = max([lower_scales, upper_scales])  * threshold * scaling
    
    
    bounds          = [-half_range*2,half_range*2]
    
    a_extreme=np.ma.masked_where(abs(a_price)<half_range, a_price)  #array for extreme values

    
    #custom colormap for extreme values
    ccmap = colors.ListedColormap(['cyan', 'magenta'])
    norm = colors.BoundaryNorm(bounds, ccmap.N)

    #bilingual dicitonary for labeling
    lbls    = { 'title' : { 'en': f'Heatmap for EPEX-Spot [€/MWh] - {year} @{market}', 
                            'de': f'Heatmap für EPEX-Spot [€/MWh] - {year} @{market}'},
                'xlbl'  : { 'en': 'days', 'de'  : 'Tage'},
                'ylbl'  : { 'en': 'hours', 'de' : 'Stunden'}}

    #create figure
    fig, gs = figure_creator(   figsize         = figsize, 
                                window_title    = 'price_heatmap', 
                                grc_subplots    = (1,2), 
                                hr_subplots     = None, 
                                wr_subplots     = (20, 1) 
                            )
                            
    ax      = fig.add_subplot(gs[0,0]) 
    

    #subplot for regular values
    im_price=ax.imshow(a_price, cmap=cmap, aspect=6, vmin=-half_range, vmax=half_range)


    #subplot for extreme values with custom colormap
    im_xtrm=ax.imshow(a_extreme, cmap=ccmap, aspect=6, vmin=-half_range*2, vmax=half_range)


    #axis modifications
    if title==True:
        ax.set_title(lbls['title'][lang])
    
    ax.set(xlabel=lbls['xlbl'][lang], ylabel=lbls['ylbl'][lang])
    ax.set_xticks(np.arange(0, 365, 30));
    ax.set_yticks(np.arange(0, 23, 3))


    #colorbar for regular values
    xcbax   = fig.add_subplot(gs[0,1])
    cbax    = fig.add_subplot(gs[0,1])
    
    cb      = fig.colorbar(im_price, cax = cbax)
    xcb     = fig.colorbar(im_xtrm, cax = xcbax, extend='both')
    xcb.set_ticks([])

    #rearrange figure elemtents for tight layout
    plt.subplots_adjust(left=0.05, bottom=0.125, right=0.95, top=0.925, wspace=0.05)
    
    #reshape colorbar, matching the vertical layout of the main plot
    ax_pos  = ax.get_position()
    cbax_pos= cbax.get_position()
    cb.ax.set_position([cbax_pos.x0, ax_pos.y0, cbax_pos.width, ax_pos.height])
    xcb.ax.set_position([cbax_pos.x0, ax_pos.y0-0.05, cbax_pos.width, ax_pos.height+0.1])


    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)
    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def price_24h_average(year:int, market = 'EPEX', best_hrs=12, lang='en', show=True, save_png=False, 
                        title=True, figsize=(10,5), dpi_save=150, footer='GPM & CC'):
    """ Function for plotting a heat map of prices for a single year.

    Parameters
    ----------
    year:int
        Input year which should be plottet

    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    half_range=0:int
        Define the half range +/- €/MWh for the price scale. If == 0, then half range is computed.

    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """


    #create data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()


    market_tab = 'T_epex_da'
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab = 'tT_exaa_hourly_avgs'
        

    #querry price data and datetime for CET
    _SQL    = """   SELECT dt_CET, price
                    FROM {market_tab}
                    JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                    WHERE strftime('%Y', dt_CET)='{y}'
                    AND price IS NOT '-' """.format(y=year, market_tab = market_tab)
    
    DF = pd.read_sql(_SQL, con)
    
    DF['dt_CET']    = pd.to_datetime(DF['dt_CET'])
    DF['date']      = DF['dt_CET'].dt.date
    DF['time']      = DF['dt_CET'].dt.time
    DF['hour']      = DF['dt_CET'].dt.hour
    DF['price']     = DF['price'].astype(float)
    
    DS_mn           = DF.groupby('hour')['price'].mean()
    DF_mn           = pd.DataFrame(DS_mn, index=DS_mn.index, columns=['price'])
   

    bp_index        = DS_mn.sort_values(ascending=False)[:best_hrs].index
    bp_value        = DS_mn.sort_values(ascending=False)[:best_hrs].mean()
    ap_value        = DF_mn['price'].mean()
    
    DF_mn.loc[24, 'price'] = DF_mn.loc[DF_mn.index.max(), 'price']

    
    #bilingual dicitonary for labeling
    
    PQ_str_en  = f'PQ = {24/best_hrs:.2} / {best_hrs} best hours'
    PQ_str_de  = f'LF = {24/best_hrs:.2} / {best_hrs} beste Stunden'
    
    
    lbls    = { 'title' : { 'en': f'Ideal schedule for hourly averages [€/MWh] of {year} @{market} | {PQ_str_en}', 
                            'de': f'Ideler Fahrplan für die Stundenmittel [€/MWh] in {year} @{market} | {PQ_str_de}'},
                'xlbl'  : { 'en': 'hour', 'de'  : 'Stunde'},
                'ylbl'  : { 'en': 'price  [€/MWh]', 'de' : 'Preis [€/MWh]'},
                'pcl'   : { 'en': 'avg. price curve', 'de' : 'mitt. Preiskurve'},
                'bpl'   : { 'en': 'optimised price', 'de' : 'Optimierter Preis'},
                'apl'   : { 'en': 'mean price', 'de' : 'Durchschnittspreis'},
                'sdl'   : { 'en': 'schedule block', 'de' : 'Fahrplanblock'},}

    #create figure
    fig, gs = figure_creator(   figsize         = figsize, 
                                window_title    = 'price_24h_average', 
                            )
                            
    ax      = fig.add_subplot(gs[0,0]) 
    

    DF_mn['price'].plot(drawstyle='steps-post', color='black', ax=ax, label=lbls['pcl'][lang])
    
    for i in bp_index:
        x = i + 0.5
        
        if i == bp_index[0]:
            ax.bar(x, DF_mn['price'].max(), width=1, alpha=0.4, color='green', label=lbls['sdl'][lang])
        else:
            ax.bar(x, DF_mn['price'].max(), width=1, alpha=0.4, color='green')

    
    ax.hlines(ap_value, 0, 24, color='blue', label=lbls['apl'][lang])
    ax.hlines(bp_value, 0, 24, color='green', label=lbls['bpl'][lang])



    ax.set_xlim(0, 24)
    
    ax.set(xlabel=lbls['xlbl'][lang], ylabel=lbls['ylbl'][lang])
    
    ax.xaxis.set_major_locator(ticker.MultipleLocator(2))
    ax.grid(ls=':', alpha = 0.5)

    
    ax.annotate('', xy = (3, bp_value), xytext = (3, ap_value), arrowprops=dict(arrowstyle="-|>"))
    ax.text(3, bp_value * 1.025, f'+ {(bp_value  - ap_value):.1f} €/MWh', ha='center', va='bottom')
    
    
    ax.legend(loc='lower center')
    
    #axis modifications
    if title==True:
        ax.set_title(lbls['title'][lang])


    #rearrange figure elemtents for tight layout
    plt.subplots_adjust(left=0.075, bottom=0.175, right=0.975, top=0.9, wspace=0.05)


    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)
    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def price_dur_curve(years:list, market = 'EPEX', y_limits=None, lang='en', show=True, save_png=False,
                    title=True, figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """ Plotting a price duration curve for one or several years.

    Parameters:
    -----------
    years:list
        list of years as integers

    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'
    
    y_lim_u:int
        value for upper y-limits, default=200
        
    y_lim_l:int
        value for upper y-limits, default=-100
        
    lang = 'en'
        language selector, default setting. 'en' { 'en' : 'english', 'de' : 'deutsch'}

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 

    Returns:
    --------
    optional Error-message, if a certain year isn't complete so load duration curve
    could not be computed.
    """
    
    if type(years) == (int or float):
        years = [years]

    #create index for empty DataFrame
    h_index     = list(range(0,8784))
    
    #create empty DataFrame
    DF_pdc      = pd.DataFrame(data=None,index=h_index)

    
    #create data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()
    
    
    market_tab  = 'T_epex_da'
    view        = 'V_epex_da'
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab  = 'tT_exaa_hourly_avgs'
        view        = 'V_exaa_da'
        

    #loop function for all years
    for yr in years:
        
        #last valid data point for power prices
        DF_tsmx = pd.read_sql(f""" SELECT  max(PK_ts_UTC) 
                                    FROM    {view}
                                    WHERE   price != '-' 
                                    AND     substr(dt_CET,1,4) == '{yr}' """.format(view = view), con)
        tsmx    = DF_tsmx.values.tolist()[0][0]
        
        
        #query sorted values for each year
        _SQL    = """   SELECT  year, price
                        FROM    {market_tab}
                        JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                        WHERE   year={y}
                        AND     {market_tab}.PK_ts_UTC <= {tx}
                        ORDER BY price DESC""".format(y=yr, tx = tsmx, market_tab = market_tab)
        con_c.execute(_SQL)
        annual   = con_c.fetchall()

        

        #clear the 
        annual_list  = []
        
        #data harmonisation
        for i in annual:
            if type(i[1])==str:
                price   = None
            else:
                price   = i[1]
            annual_list.append(price)
        if len(annual_list) in [8759, 8783]:
            annual_list.append(None)
            
        elif len(annual_list)==8760:
            annual_list  = annual_list+([None]*24)
        else:
            diff        = 8784 - len(annual_list)
            annual_list = annual_list+([None] * diff)
             
        DF_pdc[yr]      = annual_list


    #bilingual dicitonary for labeling
    lbls    =   {   'title' : { 'en': f'price duration curve(s) @{market}', 
                                'de': f'Geordnete Jahresdauerlinie(n) der Preise @{market}'},
                    'xlbl'  : { 'en': 'sorted hours of one year (8,760 / 8,784)', 
                                'de': 'Sortierte Jahresstunden (8.760 / 8.784)'},
                    'ylbl'  : { 'en': 'price [€/MWh]', 'de' : 'Preis [€/MWh]'}}


    #create plot
    if title==True:
        title=lbls['title'][lang]
    else:
        title=None
    
    #create figure
    fig, gs = figure_creator(figsize=figsize, window_title=f'price_duration_curve_{market}')
    ax  = fig.add_subplot(gs[0,0])
    
    
    DF_pdc.plot(figsize=figsize, ax=ax, \
                title = title, xlabel=lbls['xlbl'][lang], ylabel=lbls['ylbl'][lang], \
                xlim=[-80,8800], grid=True)
                
    
    ax.set_xticks([0, 1E3, 2E3, 3E3, 4E3, 5E3, 6E3, 7E3, 8E3])
    
    #define limits for the y-axis
    if y_limits != None:
        ax.ylim=(y_limits)
    
                

    #insert legend
    ax.legend(loc='upper right')


    plt.subplots_adjust(left=0.075, bottom=0.15, right=0.995, top=0.95)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)




def ets_prices(years='all', avg='week', epex=False, lang='en', show=True, save_png=False,
                title=True, figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """
    Function to plot the price response of a certain generation type and fit logistig functin parameters.

    Parameters:
    -----------
    year:int
        year for the ets

    avg:str
        averages by:
            'day'   | original time resolution
            'month' | monthly averages
            'year'  | yearly averages

    curve=False
        siwtch for choosing if the curve fitting should made.

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    lang='en'
        language for the used labels for the plot:
            'en' | englisch
            'de' | german

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """
    if type(years) == int:
        years = [years]
    years_str   = [str(y) for y in years]
    years_strl  = ', '.join(years_str)
    years_strq  = [ f'"{str(y)}"' for y in years]
    years_strql = ', '.join(years_strq)

    #dictionary of formatting strings for SQL averages
    avg_args= { 'week'  : '%Y-%W',
                'month' : '%Y-%m',
                'year'  : '%Y'}
    
    avg_arg = avg_args[avg]
    
    
    avg_lbl = { 'week'  : { 'en' : ' | weekly values / averages',
                            'de' : ' | Wochen- (mittel-) werte '},
                'month' : { 'en' : ' | monthly averages',
                            'de' : ' | Monatsmittelwerte'},
                'year'  : { 'en' : ' | yearyl averages',
                            'de' : ' | Jahresmittelwerte'} }
    
    
    #creating additional condition for certain years if 'year' !=None
    if years != 'all':
        year_arg    = "WHERE strftime('%Y', dt_CET) IN (" + years_strql + ")"
        
    else:
        year_arg = '\n'
    
    #create data base connection
    con     = sq3.connect(db_path)
    con_c   = con.cursor()

    #SQL query for pirces and directly put them into a DataFrame
    _SQL    = """   SELECT strftime('%Y-%m-%d', dt_CET) AS'dt', avg(ets) AS 'avg_ETS'
                    FROM V_ets
                    {ya}
                    GROUP BY strftime('{aa}', dt_CET)""".format(aa=avg_arg, ya=year_arg)
    
    DF_dat = pd.read_sql(_SQL, con)
    
    DF_dat['dt'] = DF_dat['dt'].astype('datetime64[ms]')

    
    DF_dat_dt_min   = DF_dat['dt'].min()
    DF_dat_dt_max   = DF_dat['dt'].max()
    
    DF_dat_dt_span  = str(DF_dat['dt'].min())[:-9] + ' - ' + str(DF_dat['dt'].max())[:-9]
    
    DF_dat_ets_max  = DF_dat['avg_ETS'].max()
    DF_epex_ap_max  = 0
    
    
    # create year span for plotting all years at once
    if epex == True:
        
      
        if years != 'all':
            year_arg =  """     WHERE strftime('%Y', dt_CET) IN ({yrs})
                                AND date(dt_CET) > date('{dt_min}')      
                                AND date(dt_CET) < date('{dt_max}') 
                        """.format(yrs=years_strql, dt_min=DF_dat_dt_min , dt_max=DF_dat_dt_max)
            
        else:
             year_arg = """     WHERE date(dt_CET) > date('{dt_min}')      
                                AND date(dt_CET) < date('{dt_max}')   
                        """.format(dt_min=DF_dat_dt_min , dt_max=DF_dat_dt_max)
             
      
        #SQL query for prices and directly put them into a DataFrame
        _SQL    = """   SELECT strftime('%Y-%m-%d', dt_CET) AS'dt', avg(price)
                        FROM T_epex_da
                        JOIN T_ts ON T_epex_da.PK_ts_UTC=T_ts.PK_ts_UTC
                        {ya}
                        GROUP BY strftime('{aa}', dt_CET)""".format(aa=avg_arg, ya=year_arg)
    
        DF_epex = pd.read_sql(_SQL, con)
        
        DF_epex['dt'] = DF_epex['dt'].astype('datetime64[ms]')
    
        DF_epex_ap_max  = DF_epex['avg(price)'].max()
    
    if years == 'all':
        year_label  = DF_dat_dt_span
    else:
        year_label  = years_strl
    
    
    y_max   = max(DF_dat_ets_max, DF_epex_ap_max) * 1.1
    
    
    
    #bilingual labels dictionary
    lbls    = { 'ttl_0' : { 'en' : f'ETS prices for Germany: {year_label} {avg_lbl[avg][lang]}',
                            'de' : f'ETS Preise für Deutschland: {year_label} {avg_lbl[avg][lang]}'},
                'ttl_1' : { 'en' : f'ETS- and EPEX-prices for Germany: {year_label} {avg_lbl[avg][lang]}',
                            'de' : f'ETS- und EPEX-Preise für Deutschland: {year_label} {avg_lbl[avg][lang]}'},
                'xlbl'  : { 'en' : 'date',
                            'de' : 'Datum'},
                'ylbl'  : { 'en' : 'price [€/t CO_2]',
                            'de' : 'Preis [€/t CO_2]'},
                'ylbl_2': { 'en' : 'price [€/MWh]',
                            'de' : 'Preis [€/MWh]'}}


    #codeblock for plotting
    if title==True:
        if epex == False:
            ttl_lbl     = lbls['ttl_0'][lang]
        else:
            ttl_lbl     = lbls['ttl_1'][lang]
    else:
        ttl_lbl     = None
    
    
    #create figure
    fig, gs = figure_creator(figsize=figsize, window_title=f'price_ets_and_averages')
    axp     = fig.add_subplot(gs[0,0])
    
    plt.title(ttl_lbl)
    axp.plot(DF_dat['dt'], DF_dat['avg_ETS'], label='ETS', color='orange')
    axp.set_ylim(0, axp.get_ylim()[1])


    if epex == True:
        
        axs     = axp.twinx()
        axs.plot(DF_epex['dt'], DF_epex['avg(price)'], label='EPEX', color='blue')
        axs.set(ylabel=lbls['ylbl_2'][lang])
        
        yticks_align(axp, axs)
        
    
    axp.set(xlabel=lbls['xlbl'][lang], ylabel=lbls['ylbl'][lang])
    
    
    
    
    
    line_p, label_p = axp.get_legend_handles_labels()
    
    if epex == True:
        line_s, label_s = axs.get_legend_handles_labels()
        lines           = line_p + line_s
        labels          = label_p + label_s
        
    else:
        lines   = line_p
        labels  = label_p
    
    
    plt.legend(lines, labels, loc=0)
    plt.grid()

    
    plt.subplots_adjust(left=0.05, bottom=0.125, right=0.925, top=0.95)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)
    
    



def price_response(year:int, gen_type:str, market = 'EPEX', plot=True, curve=False, lang='en',
                    show=True, save_png=False, title=True, figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """
    Function to plot the price response of a certain generation type and fit logistig functin parameters.

    Parameters:
    -----------
    year:int
        year for the price response
        
    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'

    gen_type:str
        generation types, based on gpm_tb_aux.var_types_en2de dictionary.

        if gen_type == 'non-renewable dispatchable generation' all non-renewable and dispatchable generation
        types will be processed togehter ('nuclear', 'lignite', 'hard_coal',
        'nat_gas', 'oil', 'other_fossil', 'waste') referring to gpm_tb_aux.var_nrdg

    curve=False
        siwtch for choosing if the curve fitting should made.

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    lang='en'
        language for the used labels for the plot:
            'en' | englisch
            'de' | german

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """


    #create data base connection
    con     = sq3.connect(db_path)
    con_c   = con.cursor()

    #create data base connection and cursor
    con     = sq3.connect(db_path)
    con_c   = con.cursor()


    market_tab  = 'T_epex_da'
    view        = 'V_epex_da'
    utc_slice   = '6, 8' 
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        market_tab  = 'T_exaa_da'
        view        = 'V_exaa_da'
        utc_slice   = '6, 11' 
        

    #SQL query for prices and directly put them into a DataFrame
    _SQL =  """ SELECT dt_CET, price, T_ts.PK_ts_UTC
                FROM {market_tab}
                JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                WHERE year={y}""".format(y=year, market_tab = market_tab)
    DF_ts = pd.read_sql(_SQL, con, index_col = 'PK_ts_UTC')
    
    
    #last valid data point for power prices
    DF_tsmx = pd.read_sql(f""" SELECT  max(PK_ts_UTC) 
                                FROM    V_epex_da
                                WHERE   price != '-' 
                                AND     substr(dt_CET,1,4) == '{year}' """,con)
    tsmx    = DF_tsmx.values.tolist()[0][0]


    if gen_type == 'non-renewable dispatchable generation':
    
        #create temporal table if cumulated non-renewable-generation should be processed
        _SQL    = """   CREATE TEMP TABLE IF NOT EXISTS TT_nrdg AS
                        SELECT 	PK_ts_UTC,
                                (lignite + hard_coal + nat_gas + nuclear + other_fossil + waste + oil) AS nrdg
                        FROM T_gen"""
                        
        con.execute(_SQL)
        con.commit()
        
        table       = 'TT_nrdg'
        gen_key     = 'nrdg'
    
    else:
        table   = 'T_gen'
        gen_key = gen_type


    _SQL    ="""SELECT avg({gt}) AS gen, T_ts.PK_ts_UTC
                FROM {tab}
                JOIN T_ts ON {tab}.PK_ts_UTC=T_ts.PK_ts_UTC
                WHERE year={y}
                GROUP BY substr(dt_UTC, {utc_slc})""".format(tab=table, gt=gen_key, y=year, utc_slc = utc_slice)
   
    DF_ts['gen'] = pd.read_sql(_SQL, con, index_col = 'PK_ts_UTC')
    
    #last valid data point for power prices
    DF_gnmx = pd.read_sql(f""" SELECT  max({table}.PK_ts_UTC) 
                                FROM {table}
                                JOIN T_ts ON {table}.PK_ts_UTC=T_ts.PK_ts_UTC
                                WHERE   {gen_key} != '-' 
                                AND     substr(dt_CET,1,4) == '{year}' """ ,con)
    gnmx    = DF_gnmx.values.tolist()[0][0]
    
    
    #find the last common valid data set
    UTC_max = min(tsmx, gnmx)

    #use queried DataFrame to also process incomplete years
    DF_qry          = pd.DataFrame(DF_ts.query(f"PK_ts_UTC < {UTC_max}"))
    
    DF_qry['price'] = DF_qry['price'].astype('float')
    n               = len(DF_qry)
    
    
    #clean up DataFrame and calculate the price range
    DF_qry['price'] = DF_qry['price'].replace(['N/A', '-', '', 'nan', 'NaN'],DF_qry['price'].mean())
    DF_qry['price'] = DF_qry['price'].fillna(DF_qry['price'].mean())

    DF_qry['gen']    = DF_qry['gen'].replace(['N/A', '-', '', 'nan', 'NaN'],DF_qry['gen'].mean())
    DF_qry['gen']    = DF_qry['gen'].fillna(DF_qry['gen'].mean())

    

    #gets "0" in the DataFrame
    if gen_type in ['lignite', 'hard_coal', 'nat_gas', 'oil', 'waste', 'nuclear']:
        DF_qry['gen'] = DF_qry['gen'].replace(0, DF_qry['gen'].mean())


    #curve fitting for logistic S-shaped function
    crv_prm={}      #empty dictinoary for curve variables
    if curve==True:


        #refer to the function y = Lmin + ((Lmax - Lmin ) / (1 + np.exp(-k*(x-x0))))
        #def logistic_S(x, Lmin, Lmax, x0, k):
        log_S = aux.logistic_S


        #assign boundries definition (_lbnd - lower bound, _ubnd - upper bound)
        Lmin_lbnd   = min(DF_qry['gen']) + 0        #minimum generation -x
        Lmin_ubnd   = min(DF_qry['gen']) + 100      #minimum generation +x

        Lmax_lbnd   = max(DF_qry['gen']) * 0.9      #maximum generation -x
        Lmax_ubnd   = max(DF_qry['gen']) * 1.2      #maximum generation +x

        x0_lbnd     = min(DF_qry['price']) * 0.8    #minmum price
        x0_ubnd     = max(DF_qry['price']) * 1.2    #maximum price

        k_lbnd      = -1
        k_ubnd      = 1


        bnds        = [ [Lmin_lbnd, Lmax_lbnd, x0_lbnd, k_lbnd], \
                        [Lmin_ubnd, Lmax_ubnd, x0_ubnd, k_ubnd]]

        #starting point for the curve fitting
        p0          = [Lmin_lbnd, Lmax_ubnd, DF_qry['price'].median(), 1]

        #curve fitting
        popt, pcov  = curve_fit(log_S, DF_qry['price'], DF_qry['gen'], p0, \
                                method='trf', bounds=bnds)


        #calculate coefficient of determination (cod) - R²
        DF_qry['mod_gen']    = log_S(DF_qry['price'], *popt)                  #modelled generation
        correlation_matrix  = np.corrcoef(DF_qry['gen'], DF_qry['mod_gen'])   #corellation matrix
        correlation_xy      = correlation_matrix[0,1]                       #coefficient of corel.
        r_sqrd              = round(correlation_xy**2,3)                    #coefficient ot determ.


        #setting curve fitting parameters
        crv_prm['popt'] = popt
        crv_prm['min']  = min(DF_qry['gen'])
        crv_prm['med']  = np.median(DF_qry['gen'])
        crv_prm['max']  = max(DF_qry['gen'])
        crv_prm['cod']  = r_sqrd


        #pricerange in integer steps for plotting the fitted curve
        price_rng   = list(range(int(min(DF_qry['price'])), int(max(DF_qry['price']))))


        #create legend based on fitted curve parameters
        legend  = r'$g(p) = G_{min} + \frac{G_{max} - G_{min}}{(1+e^{-k*(p-p_0)})} $' \
        + '\n $G_{min}$ = %d \n $G_{max} = $%d \n $p_{0}$ = %5.2f \n $k$ = %5.3f \n'  % tuple(popt)

        #add coefficient of determination to legend
        legend  = legend + r'$R^{2} =$ ' + str(r_sqrd)


    #bilingual labels dictionary
    lbls    = { 'title' : { 'en' : f'price response for {gen_type} in {year} | n={n} @{market}',
                            'de' : f'Preisreaktion für {aux.var_types_en2de[gen_type]} in {year} | n={n} @{market}'},
                'xlbl'  : { 'en' : 'price [€/MWh]',
                            'de' : 'Preis [€/MWh]'},
                'ylbl'  : { 'en' : 'generation hourly mean [MW]',
                            'de' : 'Erzeugung im Stundenmittel [MW]'}}


    #codeblock for plotting
    if title==True:
        title=lbls['title'][lang]
    else:
        title=None
    
    if plot == True:
        
        #create figure
        fig, gs = figure_creator(figsize=figsize, window_title=f'price_response_func-fit')
        ax      = fig.add_subplot(gs[0,0])


        DF_qry.plot.scatter(1, 2, s=2, ax=ax, title = title, grid=True)
    
        #optional plotting of the curve
        if curve==True:
            plt.plot(price_rng, log_S(price_rng, *popt), color='deeppink', label=legend)
    
        plt.grid(linestyle=':',linewidth=0.75)
        plt.xlabel(lbls['xlbl'][lang])
        plt.ylabel(lbls['ylbl'][lang])
    
        if curve==True:
            #plt.legend(loc='upper left')
            plt.legend(loc='lower right')
    
    
        plt.subplots_adjust(left=0.075, bottom=0.125, right=0.995, top=0.95)
    
        #insert footer content if footer != False
        if footer != False:
            add_fig_footer(fig, footer, lang)
    
        
        #conditional showing and or saving to png
        show_save(show, save_png, dpi_save)


    #return fittted curve parameters
    if crv_prm != {}:
        return(crv_prm)




def pdei(year:int, market='EPEX', xlim='auto', lang='en', show=True, save_png=False, title=True, 
            figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """
    Function to plot price dependet emission intensity for all nrdg-types, real as well as modeled values.

    Parameters:
    -----------
    year:int
        year for the marginal emissions

    market:str
        Define the exchange market  place: 'EPEX' | 'EXAA'
    
    xlim:tuple
        If xlim != 'auto' use tuple to define x-axis limits

    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    lang='en'
        language for the used labels for the plot:
            'en' | englisch
            'de' | german

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC' 
    """
    
    #create data base connection
    con     = sq3.connect(db_path)
    con_c   = con.cursor()


    market_tab  = 'T_epex_da'
    view        = 'V_epex_da'
    slice_str   = '6, 8'
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab  = 'T_exaa_da'
        view        = 'V_exaa_da'
        slice_str   = '6, 11'


    #generation types and build SQL-sub-query string
    gen_tps = list(aux.var_types_en2de.keys())[:-3]
    for i in range(0, len(gen_tps)):
        gen_tps[i] = ' avg(' + gen_tps[i] + ') AS ' + gen_tps[i]
    sql_substr = ','. join(gen_tps)

    #SQL-statement for query price and marginal emissions
    _SQL    = """   SELECT substr(dt_CET, 6, 8) AS 'CET', price, pdei,  {gt}
                    FROM T_gen
                    JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC
                    LEFT JOIN {market_tab} ON T_ts.PK_ts_UTC={market_tab}.PK_ts_UTC
                    WHERE year={y}
                    GROUP BY substr(dt_CET, {slcs})
                """.format(gt=sql_substr, y=year, market_tab=market_tab, slcs=slice_str)


    #do SQL query and directly transfer to DataFrame
    DF_ts = pd.read_sql(_SQL, con)
    DF_ts = DF_ts.replace(['N/A','-',''],0)


    #Data_Frame for generation types
    DF_gt           = pd.read_sql('SELECT * FROM T_gen_tps', con)
    DF_gt.index     = DF_gt['name_db']


    #list of non-renewable dispatchable generation
    nrdg                = aux.var_nrdg
    DF_nrdg             = DF_ts[nrdg]
    V_nrdg_em           = DF_gt.loc[nrdg,'emissions_fuel']
    DF_ts['mem_real']   = (DF_nrdg * V_nrdg_em).sum(axis=1) / DF_nrdg.sum(axis=1)

    #drp rows with missing values of mem_real
    DF_ts.dropna(subset=['mem_real'], inplace=True)

    #cut off extreme values caused by gaps in the entsoe raw data
    ylim_co_id          = len(DF_ts) - 1
    ylim_max            = (np.ceil(DF_ts['mem_real'].sort_values().iloc[ylim_co_id]/100))*100
    ylim_min            = np.floor(DF_ts['mem_real'].min()/100)*100
    ylim                = [ylim_min, ylim_max]


    #calculate coefficient of determination (cod) - R²
    correlation_xy      = DF_ts['pdei'].corr(DF_ts['mem_real'])     #coefficient of corel.
    r_sqrd              = round(correlation_xy**2,3)                    #coefficient ot determ.



    #bilingual labels dictionary
    lbls    = { 'title' : { 'en' : f'price dependent emission intensity {year} @{market}',
                            'de' : f'Preisabhängige Emissionsintensitätv {year} @{market}'},
                'xlbl'  : { 'en' : 'price [€/MWh]',
                            'de' : 'Preis [€/MWh]'},
                'ylbl'  : { 'en' : 'emissions intensity[g CO$_\mathrm{ 2eq}$/kWh]',
                            'de' : 'Emissionsintensität [g CO$_\mathrm{ 2eq}$/kWh]'},
                'lbl_0' : { 'en' : 'real emissions vs. price',
                            'de' : 'reale Emissionen vs Preis'},
                'lbl_1' : { 'en' : 'modeled emission intensity',
                            'de' : 'modellierte Emissionsintensität'}}
    
    
    #create figure
    fig, gs = figure_creator(figsize=figsize, window_title=f'depended_emission_intensity')
    ax      = fig.add_subplot(gs[0,0])
    
    
    #plotting real values to axis
    DF_ts.plot.scatter(1, 20, s=1, c='blue', ylim=ylim, label=lbls['lbl_0'][lang], ax=ax)


    #optinoal plotting the title
    if title==True:
        ax.set_title(lbls['title'][lang])
    
    #add coefficient of determination to legend
    lbl_1  = lbls['lbl_1'][lang] + r' $R^{2} =$ ' + str(r_sqrd)
    
    #plotting modeled values to axis
    DF_ts.plot.scatter(1, 2, s=1, c='deeppink', ax=ax, label=lbl_1)
    
    #figure styling
    plt.grid(linestyle=':',linewidth=0.75)
    plt.xlabel(lbls['xlbl'][lang])
    plt.ylabel(lbls['ylbl'][lang])
    
    
    if xlim != 'auto':
        ax.set_xlim(xlim)
   
    plt.subplots_adjust(left=0.075, bottom=0.125, right=0.975, top=0.95)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)


    return(DF_ts)




def price_vs_emissions(year:int, market = 'EPEX', plot_actual=True,  plot_model=False, xlim = 'auto',
                    lang='en', show=True, save_png=False, title=True, figsize=(13,7), 
                    dpi_save=150, footer='GPM & CC'):
    """
    Plotting the weighted carbon footprint for the power mix depending on the spot market price.

    Parameters:
    -----------
    year:int
        year for that the plot shoul be made.

    plot_actual:True
        argument for plotting (just) the actual values
    
    plot_model:False
        argument for plotting (just) the modelled values

    xlim:tuple
        If xlim != 'auto' use tuple to define x-axis limits
    
    lang='en'
        language for the used labels for the plot:
            'en' | englisch
            'de' | german

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC'
    """


    #create data base connection
    con     = sq3.connect(db_path)
    con_c   = con.cursor()

    market_tab = 'T_epex_da'
    
    #use support function to create an temporary database table for hourly aggregated prices
    if market == 'EXAA':
        db_tT_EXAA_hourly_averages(con)
        market_tab = 'tT_exaa_hourly_avgs'
        

    #generation types and build SQL-sub-query string
    gen_tps = list(aux.var_types_en2de.keys())[:-3]     #skip 'load' & 'residual load'
    for i in range(0, len(gen_tps)):
        gen_tps[i] = ' avg(' + gen_tps[i] + ') AS ' + gen_tps[i]
    sql_substr = ','. join(gen_tps)

    #SQL-statement for query price and marginal emissions
    sql=""" SELECT substr(dt_CET, 6, 8) AS 'CET', price, pdei,  {gt}
            FROM T_gen
            JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC
            LEFT JOIN {market_tab} ON T_ts.PK_ts_UTC={market_tab}.PK_ts_UTC
            WHERE year={y}
            GROUP BY substr(dt_CET, 6, 8)""".format(gt=sql_substr, y=year, market_tab=market_tab )


    #do SQL query and directly transfer to DataFrame
    DF_ts = pd.read_sql(sql, con)
    DF_ts = DF_ts.replace(['N/A','-',''],0)


    #Data_Frame for generation types
    DF_gt = pd.read_sql('SELECT * FROM T_gen_tps', con)


    #calculate actual emission mix
    V_emix          = np.array(DF_gt.iloc[:,4])
    DF_gen_act      = DF_ts.iloc[:,3:]
    DF_ts['actual'] = (DF_gen_act * V_emix).sum(axis=1) / DF_gen_act.sum(axis=1)

    
    #optional calculating and plotting modelled values
    if plot_model==True:
        #get non-renewable dispatchable generation from modelling function referring to gpm_db_dat
        DF_nrdg     = dat.nrdg_model(year, market = market).iloc[:,3:-1]
    
        #copy actual generation DataFrame and replace modelled generations for nrdg
        DF_gen_mod      = DF_gen_act
        for gt in DF_nrdg.columns:
            DF_gen_mod[gt]  = DF_nrdg[gt]
    
    
        DF_ts['model']  = (DF_gen_mod * V_emix).sum(axis=1) / DF_gen_mod.sum(axis=1)


    #bilingual labels dictionary
    lbls    = { 'title' : { 'en' : f'weighted emission mix vs. price in {year} @{market}',
                            'de' : f'Gewichteter  Emissionsmix vs. Preis in {year} @{market}'},
                'xlbl'  : { 'en' : 'price [€/MWh]',
                            'de' : 'Preis [€/MWh]'},
                'ylbl'  : { 'en' : 'emission mix [g CO$_\mathrm{ 2eq}$/kWh]',
                            'de' : 'Emissionsmix [g CO$_\mathrm{ 2eq}$/kWh]'},
                'rlbl'  : { 'en' : 'real', 'de' : 'real'},
                'mlbl'  : { 'en' : 'modelled', 'de' : 'modelliert'},}


    #create figure
    fig, gs = figure_creator(figsize=figsize, window_title=f'price_emissions')
    ax      = fig.add_subplot(gs[0,0])
    DF_ts.plot.scatter(1, 20, s=2, ax=ax, grid=True, c='blue', label=lbls['rlbl'][lang])
    

    #optinoal plotting the title
    if title==True:
        ax.set_title(lbls['title'][lang])

    
    #optional platting modelled values
    if plot_model==True:
        DF_ts.plot.scatter(1, 21, s=1, c='deeppink', alpha=0.5 , ax=ax, label=lbls['mlbl'][lang])
        ax.legend(loc='lower right')
    
    
    #styling attributes
    plt.grid(linestyle=':',linewidth=0.75)
    plt.xlabel(lbls['xlbl'][lang])
    plt.ylabel(lbls['ylbl'][lang])


    if xlim != 'auto':
        ax.set_xlim(xlim)

    plt.subplots_adjust(left=0.075, bottom=0.125, right=0.995, top=0.95)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)

    
    
    
def price_vs_resload(year:int, market = 'EPEX',  xlim = 'auto', lang='en', show=True,
                    save_png=False, title=True, figsize=(13,7), dpi_save=150, footer='GPM & CC'):
    """
    Plotting the annual spot market price vaersus the residual load.

    Parameters:
    -----------
    year:int
        year for that the plot shoul be made.
        
    market:str
        Define the exchange market place: 'EPEX' | 'EXAA'


    xlim:tuple
        If xlim != 'auto' use tuple to define x-axis limits
    
    lang='en'
        language for the used labels for the plot:
            'en' | englisch
            'de' | german

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC'
    """


    #create data base connection
    con     = sq3.connect(db_path)
    con_c   = con.cursor()


    #SQL-statement for query price and marginal emissions
    _SQL    = """   SELECT  substr(dt_CET, 6, 8)    AS 'CET'
                            , max(price)            AS 'price'
                            , avg(res_load) / 1E3   AS 'res_load'
                    FROM    T_loads
                    JOIN    T_ts ON T_loads.PK_ts_UTC = T_ts.PK_ts_UTC
                    LEFT    JOIN T_epex_da ON T_ts.PK_ts_UTC=T_epex_da.PK_ts_UTC
                    WHERE   year={y} 
                    GROUP BY substr(dt_UTC, 1, 13)"""

    DF      = pd.read_sql(_SQL.format(y=year), con)



    #bilingual labels dictionary
    lbls    = { 'title' : { 'en' : f'price versus residual load: {year}',
                            'de' : f'Preis versus Residuallast: {year}'},
                'xlbl'  : { 'en' : 'price [€/MWh]',
                            'de' : 'Preis [€/MWh]'},
                'ylbl'  : { 'en' : 'residual load [GW]',
                            'de' : 'Residualast [GW]'}}


    #create figure
    fig, gs = figure_creator(figsize=figsize, window_title=f'price_residual_load')
    ax      = fig.add_subplot(gs[0,0])
    ax.scatter(DF.price, DF.res_load, s=1, c='blue')
    

    #optinoal plotting the title
    if title==True:
        ax.set_title(lbls['title'][lang])

    
    #styling attributes
    plt.grid(linestyle=':',linewidth=0.75)
    plt.xlabel(lbls['xlbl'][lang])
    plt.ylabel(lbls['ylbl'][lang])

    
    if xlim != 'auto':
        ax.set_xlim(xlim)


    plt.subplots_adjust(left=0.075, bottom=0.125, right=0.99, top=0.95)


    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)
    
    
    
    
## secondary data analytics


def ndf_4x7_plot(year0=2016, year1=2020, show=True, save_png=False, lang='en', title=True,
                    figsize=(12,4), dpi_save=150, footer='GPM & CC'):
    """
    Function for plotting normalized daily factors for electricity prices.
    
    Parameters:
    -----------
    year0, year1: int
        years of the timespan for calculate ndf
    
    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    lang='en'
        language for the used labels for the plot:
            'en' | englisch
            'de' | german

    title=True
        argument for supress the plotting of the title if title!=True
        
    figsize:tuple
        Dimensions of the figure in inches.

    footer:str
        Argument for footer content, if != None integrate into the figure via an extra subplot.
        Options: False | 'GPM' | 'CC' | 'GMP & CC'
    """
    
    #recalculate ndf's
    dat.ndf_nhf(year0, year1)
    
    #create database connection
    con = sq3.connect(db_path)
    
    
    #precalculate the rated capacity
    _SQL = """
    SELECT  T_epex_ndf.FK_season, 
            T_epex_ndf.FK_wday, 
            T_epex_ndf.value, 
            season_{lg}, 
            weekday_{lg}, 
            num_de
    FROM T_epex_ndf
    JOIN T_seasons  ON T_epex_ndf.FK_season = T_seasons.PK_season
    JOIN T_wdays    ON T_epex_ndf.FK_wday   = T_wdays.PK_wday
    """.format(lg=lang)
    

    DF_ndf = pd.read_sql(_SQL, con)
    
    
    #query meta data
    _SQL = """ SELECT * FROM log"""
    DF_meta =   pd.read_sql(_SQL, con)
    
    
    #retrieve first and last year of the analysis
    first_y     = str(DF_meta['Value'][0])
    last_y      = str(DF_meta['Value'][1])
    
    
    #define meta data for plotting
    seasons_list    = list(dict.fromkeys(list(DF_ndf['season_' + lang])))
    wdays_list      = DF_ndf.where(DF_ndf['FK_season']==1).dropna().sort_values('num_de')['weekday_' + lang]
    wdays_list      = [i[0:2] for i in wdays_list]
    
    
    #bilungual dictionary for axes labeling
    lbls    = { 's_ttl' : { 'en':'Normalised daily factors (ndf) referencend to weekly averages of the period from ' \
                            + first_y + ' to ' + last_y, 
                            'de':'Normalisierte Tagesfaktoren (ndf) bezogen auf die wöchentlichen Mittelwerte  des Zeitraums von ' \
                            + first_y + ' bis ' + last_y \
                            },
                'ax_tl':  { 'en':'seasonal ndf -  ',
                            'de':'Saisonaler ndf - '},
                'y_lbl' : { 'en':' ndf [-]', 'de':' ndf [-]'}}


    #create figure
    fig, gs = figure_creator(   figsize=figsize, 
                                window_title=f'ndf_factors', 
                                grc_subplots=(1, 4))

    
    #optional super titel for the whole diagram
    if title==True:
        fig.suptitle(lbls['s_ttl'][lang])

    
    for i in range(4):
        DF_plot     = DF_ndf.where(DF_ndf['FK_season']==i+1).dropna().sort_values('num_de')
        ax          = fig.add_subplot(gs[0,i])
        ax.bar(wdays_list, DF_plot['value'])
        ax.set_title(seasons_list[i])
        ax.grid(ls=':', alpha=0.75)
    
        #add y-label for the first subplot
        if i ==0:
            ax.set_ylabel(lbls['y_lbl'][lang])
            ax0 = ax
        else:
            ax.sharey(ax0)
    
    
    plt.subplots_adjust(left=0.075, bottom=0.2, right=0.99, top=0.85, hspace=0.125)

    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)
    
    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)
    
    
    #return DataFrame of ndf
    return(DF_ndf)
    
    
    
    
def nhf_4x168_plot(year0=2016, year1=2020, show=True, save_png=False, lang='en', title=True,
                    figsize=(12,4), dpi_save=150, footer='GPM & CC'):
    """
    Function for plotting normalized hourly factors for electricity prices.
    
    Parameters:
    -----------
    year0, year1: int
        years of the timespan for calculate ndf
        
    show=True
        swith for suppress to show to figure if the function is just used for computing the curve fit

    save_png=False
        string with path for saving figure to png file, default=False

    lang='en'
        language for the used labels for the plot:
            'en' | englisch
            'de' | german

    title=True
        argument for supress the plotting of the title if title!=True
    """
    
    #recalculate ndf's
    dat.ndf_nhf(year0, year1)
    
    #create database connection
    con = sq3.connect(db_path)
    
    
    #query normalised hourly factors (nhf)
    _SQL = """
    SELECT  T_epex_nhf.FK_season, 
            T_epex_nhf.FK_wday, 
            T_epex_nhf.hour, 
            T_epex_nhf.value, 
            season_{lg}, 
            weekday_{lg}, 
            num_de
    FROM T_epex_nhf
    JOIN T_seasons ON T_epex_nhf.FK_season=T_seasons.PK_season
    JOIN T_wdays ON T_epex_nhf.FK_wday=T_wdays.PK_wday
    """.format(lg=lang)
    
    
    DF_nhf = pd.read_sql(_SQL, con)
    
    
    #query meta data
    _SQL = """ SELECT * FROM log"""
    DF_meta =   pd.read_sql(_SQL, con)
    
    #retrieve first and last year from metadata query
    first_y     = str(DF_meta['Value'][0])
    last_y      = str(DF_meta['Value'][1])
    
    
    #create lists for several metadata
    seasons_list    = list(dict.fromkeys(list(DF_nhf['season_' + lang])))
    wdays_list      = list(dict.fromkeys(DF_nhf.sort_values('num_de')['weekday_' + lang]))
    wdays_list      = [i[0:2] for i in wdays_list]
    hours_list      = [0,3,6,9,12,15,18,21,24]
    
    
    #bilungual dictionary for axes labeling
    lbls    = { 's_ttl' : { 'en':'Normalised hourly factors (nhf) referencend to daily averages of the period from ' \
                            + first_y + ' to ' + last_y, 
                            'de':'Normalisierte Stundenfaktoren (nhf) bezogen auf die täglichen Mittelwerte des Zeitraums von ' \
                            + first_y + ' bis ' + last_y \
                            },
                'ax_tl' :  { 'en':'seasonal nhf -  ',
                            'de':'Saisonaler nhf - '},
                'x_lbl':   { 'en':' 7 days x 24 hours', 'de':' 7 Tage x 24 Stunden'},
                'y_lbl':   { 'en':' nhf [-]', 'de':' nhf [-]'}}


    #create figure
    #create figure
    fig, gs = figure_creator(   figsize=figsize, 
                                window_title=f'nhf_factors', 
                                grc_subplots=(1, 4))
    
    #optional super titel for the whole diagram
    if title==True:
        fig.suptitle(lbls['s_ttl'][lang])


    for i in range(4):
        DF_plot     = DF_nhf.where(DF_nhf['FK_season']==i+1).dropna().sort_values(['num_de','hour'], ignore_index=True)
        ax          = fig.add_subplot(gs[0,i])
        ax.plot(DF_plot['value'], lw=0.75)
        ax.set_xlabel(lbls['x_lbl'][lang])
        ax.set_xlim(0,167)
        ax.set_title(seasons_list[i])
        ax.set_xticks(np.arange(len(wdays_list))*24)
        ax.set_xticklabels(wdays_list)
        ax.grid(ls=':', alpha=0.75)
    
        #add y-label for the first subplot
        if i ==0:
            ax.set_ylabel(lbls['y_lbl'][lang])
            ax0 = ax
        else:
            ax.sharey(ax0)
    

    plt.subplots_adjust(left=0.075, bottom=0.25, right=0.99, top=0.85, hspace=0.125)


    #insert footer content if footer != False
    if footer != False:
        add_fig_footer(fig, footer, lang)

    
    #conditional showing and or saving to png
    show_save(show, save_png, dpi_save)
    
    
    #return DataFrame of ndf
    return(DF_nhf)