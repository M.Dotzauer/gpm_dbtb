#!/usr/bin/env python
"""
This module is a collection of functions to crawl and process data for the german power market (gpm)
 from the entsoE-transparency plattform and store that data in the gpm.db (SQLite db).
"""

#import global libraries
import os
import csv
import pandas 				as pd
import numpy 				as np
import sqlite3 				as sq3
import matplotlib.pyplot 	as plt
import matplotlib.gridspec 	as gsp
import openpyxl 			as xl
from openpyxl import load_workbook
import datetime 			as dt
from datetime import timezone

#import local libraries
import gpm_tb_dia 			as dia
import gpm_tb_aux 			as aux


#path to data base
db_path = os.path.join(aux.var_script_dir, aux.var_db_name)



def sql_com(action:str, table:str, keyval:dict, *cond:str)->str:
	""" Create a command string for Updating SQLite.db with several columns

	Parameters
	----------
	action : str
		Action that should be done on the database, for example REPLACE OR UPDATE
	table : str
		Table that shoul be updated.
	keyval : dict
		Dictionary of pairs of column-name and the certain value
	*cond : str
		Condition for selective update

	Return
	-------
	sql_commanad : str
			Concantated SQL-Command for SQLite
	"""

	#keys of the given dictionary (names of columns)
	keys= ', '.join(list(keyval.keys()))


	#values of the given dictinary (values for each column)
	values= []
	for i in  keyval:
		values.append(keyval[i])

	#create SQL-command-strin
	sql_command=action + ' ' +  table + ' (' + keys + ')' + ' VALUES (' + str(values).strip('[]') + ')'

	#return SQL-command string
	return(sql_command)



def sql_keyvals(db_table:str, ts_utc_ux:int, l_row:list)->dict:
	""" Compose dictionary for key-values-pairs used in sql_com().

	Parameters
	----------
	db_table : str
		target table for data transfer
	ts_UTC_ux : int
		Unixoid timestamp for UTC
	l_row : list
		list of one row of the input csv-file

	Return
	-------
	sql_keyval : dict
			dictinoary of pairs of keys (column names) and referring values (values)
	"""

	import datetime as dt

	#mempty dictionary
	sql_keyval={}

	#key-value-constructor for the Table 'T_epex_da'
	if db_table=='T_epex_da':
		sql_keyval={
			'PK_ts_UTC': ts_utc_ux,
			'price': l_row[1]}

	#key-value-constructor for the Table 'T_loads'
	elif db_table=='T_loads':
		sql_keyval={
			'PK_ts_UTC': ts_utc_ux,
			'load': l_row[2]}

	#key-value-constructor for the Table 'T_gen'
	elif db_table=='T_gen':
		sql_keyval={
			'PK_ts_UTC': ts_utc_ux,
			'nuclear':l_row[16],
			'lignite':l_row[3],
			'hard_coal':l_row[6],
			'nat_gas':l_row[5],
			'oil':l_row[7],
			'other_fossil':l_row[17],
			'waste':l_row[20],
			'biomass': l_row[2],
			'geothermal':l_row[10],
			'hydro_river':l_row[13],
			'hydro_reservoir':l_row[14],
			'pv':l_row[19],
			'wind_offshore':l_row[21],
			'wind_onshore':l_row[22],
			'other_renewable':l_row[18],
			'hydro_storage_gen':l_row[11],
			'hydro_storage_con':l_row[11]}
	else:
		print('No predefined keyval dict availible!')
		sys.exit()

	#return key-value-dictionary
	return(sql_keyval)




## functions for importing intial datasets from csv-file

def smw_metadata(csv_dir, csv_names):
	"""
	Function for importing basic meta data for seasons, months and weekdays.
	"""
		

	
	#create list of DataFrames from csv-files
	DF_list = []

	for i, n in enumerate(csv_names):
		DF_list.append(pd.read_csv(os.path.join(csv_dir, csv_names[i]), sep=';'))

	
	#create database connection
	con 	= sq3.connect(db_path)
	con_c 	= con.cursor()
	
	
	#transfer data into the data base
	_SQL 	= ("""	INSERT OR REPLACE INTO {tn} ({cl}) VALUES({nv}) """)


	#looping through DF_list for create and fill tables
	for i, df in enumerate(DF_list):
		tab_name 	= csv_names[i][:-4]
		col_list 	= ', '.join(list(DF_list[i].columns))
		val_plhd 	= ','.join(len(DF_list[i].columns) * '?')
		
		con_c.executemany(_SQL.format(tn=tab_name, cl=col_list, nv=val_plhd), DF_list[i].values)
		con.commit()
	
	
	#status message
	print('Finish: Import metadata for seasons, months and weekdays from CSV to sqlite.db.')
	
	


	

def meta_gen_types(csv_path):
	"""
	Import meta data form subfolder csv/meta.


	Parameters
	----------
	csv_path : str
		directory and name of the csv-file, absolutely.
	"""


	#set database path
	db_path 	= os.path.join(aux.var_script_dir, aux.var_db_name)


	#status message for the beginning
	print('Start transfer of meta data (generation types) to data base.')


	#convert csv to DataFrame
	DF_meta 	= pd.read_csv(csv_path, sep=';', dtype=str)


	#convert DataFrame to nested list
	nl=[]
	for i in DF_meta.index:
		sublist = list(DF_meta.iloc[i,:])
		nl.append(sublist)

	#create data base connection
	con=sq3.connect(db_path)
	con_c = con.cursor()


	#transfer data into the data base
	sql=("""	INSERT OR REPLACE INTO T_gen_tps(ID, name_db, name_de, name_en, emissions_fuel, reference) VALUES(?, ?, ?, ?, ?, ?) """)
	con_c.executemany(sql, nl)
	con.commit()

	#status message
	print('Finish: Import metadata for generation types from CSV to sqlite.db.')



def opsd_csv2db(csv_path:str):
	""" Import initial dataset from csv-file (exportet from https://open-power-system-data.org/
	for EPEX-spot prices (hourly-values). Timestamps using UTC-format to avoid summer-time issues.

	Parameters
	----------
	csv_path : str
		directory and name of the csv-file, absolutely.
	"""


	#status message for the beginning
	print('Start: Import from CSV to sqlite.db')
	start 	= dt.datetime.now()
	tc0 	= dt.datetime.now()


	#support functions
	db_dir 	= aux.var_script_dir #recognize the directory of that script and set that for cwd


	#importing the csv-file
	df_csv 		= pd.read_csv(csv_path)
	df_csv 		= df_csv.rename(columns={	'utc_timestamp':'dt_UTC', \
									'cet_cest_timestamp':'dt_CET', \
									'DE_price_day_ahead':'price'}) 		#shortening column name
	df_csv['ts_UTC'] = (pd.to_datetime(df_csv['dt_UTC'].str[0:16].str.replace('T',' '))- dt.datetime(1970,1,1)).dt.total_seconds() 								#calculate timestamp


	#convert DataFrame into nested list for 'executemany'
	nl = []
	for i in df_csv.index:
		sublist = [df_csv['ts_UTC'][i], df_csv['price'][i]]
		nl.append(sublist)


	#Transfer data into the database
	con 	=sq3.connect(aux.var_db_name)			#Connect to database
	con_c 	= con.cursor() 							#Create cursor-object

	sql="""	INSERT OR REPLACE INTO T_epex_da (PK_ts_UTC, price)
			VALUES(?, ?)"""
	con_c.executemany(sql,nl)
	con.commit()

	#delete Data after 1st of April 2019 because primary data is coruppted
	sql 	= """	DELETE FROM T_epex_da
			WHERE PK_ts_UTC > strftime('%s', '2019-04-01 00:00') """
	con_c.execute(sql)
	con.commit()
	con.close()


	#status message for the finish
	dur = dt.datetime.now()-start
	print('Finish: Import from opsd CSV to sqlite.db. Duration (mm:ss): ' + str(dur)[2:-7])



def entsoE_csv2db_cap(csv_path):
	""" Import capacities from csv-file (exportet from https://open-power-system-data.org/
	for.

	Parameters
	----------
	csv_dir : str
		directory of the csv-file, absolutely.

	csv_name : str
		name of the csv-file without suffix
	"""


	#set db_path
	db_path = os.path.join(aux.var_script_dir, aux.var_db_name)

	#status message for the beginning
	print('Start: Import from entsoE-CSV to sqlite.db')


	#dictionary of all generation Types {'entsoE-Label' : 'data base label'}
	type_dict={	'Biomass' : 'biomass',
				'Fossil Brown coal/Lignite' : 'lignite',
				'Fossil Coal-derived gas' : 'NA',
				'Fossil Gas' : 'nat_gas' ,
				'Fossil Hard coal' : 'hard_coal',
				'Fossil Oil' : 'oil',
				'Fossil Oil shale' : 'NA',
				'Fossil Peat' : 'NA',
				'Geothermal' : 'geothermal',
				'Hydro Pumped Storage' : 'hydro_storage_gen',
				'Hydro Run-of-river and poundage' : 'hydro_river',
				'Hydro Water Reservoir' : 'hydro_reservoir',
				'Marine' : 'NA',
				'Nuclear' : 'nuclear',
				'Other' : 'other_fossil',
				'Other renewable':'other_renewable',
				'Solar' : 'pv',
				'Waste' : 'waste',
				'Wind Offshore' : 'wind_offshore',
				'Wind Onshore' : 'wind_onshore',
				'Total Grand capacity': 'NA'}

	sql_keyval={'year': 0,
				'nuclear':0,
				'lignite':0,
				'hard_coal':0,
				'nat_gas':0,
				'oil':0,
				'other_fossil':0,
				'waste':0,
				'biomass':0,
				'geothermal':0,
				'hydro_river':0,
				'hydro_reservoir':0,
				'pv':0,
				'wind_offshore':0,
				'wind_onshore':0,
				'other_renewable':0,
				'hydro_storage_gen':0}


	DF_cap 		= pd.read_csv(csv_path, sep=',')
	DF_cap.replace('N/A',0, inplace=True)
	DF_cap.fillna(0, inplace=True)
	DF_cols 	= DF_cap.columns.tolist()
	n_years 	= len(DF_cols)
	DF_index 	= DF_cap.index
	years 		= []
	for y in range(1, n_years):
		years.append(DF_cols[y][0:4])


	#Connect to database
	con 	= sq3.connect(db_path)
	con_c 	= con.cursor()


	#compute keyval dictionary
	sql_keyval = {}
	for yr in range(1, n_years):
		for ti in DF_index:
			sql_keyval[type_dict[DF_cap.iloc[ti,0]]]=DF_cap.iloc[ti, yr]

		sql_keyval['year'] = years[yr-1]

		del sql_keyval['NA']

		sql_command=sql_com('INSERT OR REPLACE INTO', 'T_caps', sql_keyval)
		con_c.execute(sql_command)
		con.commit()


	#status message for finish
	con.close() #Close database-Connection
	print('Finish: Import capacities from CSV to sqlite.db.')



def entsoE_csv2db(csv_path:str, db_table:str):
	""" Import initial dataset from csv-file (exportet from https://open-power-system-data.org/
	for EPEX-spot prices (hourly-values). Timestamps using UTC-format to avoid summer-time issues.

	Parameters
	----------
	csv_dir : str
		directory of the csv-file, absolutely.

	csv_name : str
		name of the csv-file without suffix

	db_table : str
		name of the target table in the database
	"""
	


	#set db_path
	db_path = os.path.join(aux.var_script_dir, aux.var_db_name)



	#status message for beginning
	print('Start: Import | ' + csv_path + ' | to -' + db_table + '- in ' + aux.var_db_name)
	start 	= dt.datetime.now()
	tc0 	= dt.datetime.now()


	#initial import of the csv-file
	df_csv 		= pd.read_csv(csv_path, na_values=0) 		#read csv-file to Dataframe
	df_csv.fillna(0, inplace=True)
	tsp 		= {'T_epex_da':0, 'T_exaa_da':0, 'T_loads':0, 'T_gen':1} 	#timestamp positions

	#calculate timestamp from datetime
	df_csv.iloc[:,tsp[db_table]] = (pd.to_datetime(df_csv.iloc[:,tsp[db_table]].str[:16], dayfirst=True) - dt.datetime(1970,1,1)).dt.total_seconds()


	#convert DataFrame into a nested list
	nc 			= len(aux.var_tc[db_table]) 						#number of columns
	placeholder = ', '.join(['?']*(nc))
	cp 			= [item[1] for item in aux.var_tc[db_table]] 	#column positions in the csv file
	nl 			= [] 											#nested list for 'executemany'
	for i in df_csv.index: 										#loop over rows
		sublist = []
		for j in range(0, nc): 									#loop over columns
			sublist.append(str(df_csv.iloc[i,cp[j]])) 			#sqlite didnt acept numpy.int64
		nl.append(sublist)


	tabcols = ', '.join([item[0] for item in aux.var_tc[db_table]]) # ,-sep. string of columns
	

	con=sq3.connect(db_path)#Connect to database
	con_c = con.cursor() #Create cursor-object

	sql="""	INSERT OR REPLACE INTO {tbl} ({tc})
			VALUES({ph})""".format(tbl=db_table, tc=tabcols, ph=placeholder)
	con_c.executemany(sql, nl)

	con.commit()
	con.close()


	#status message for finish
	dur = dt.datetime.now()-start
	print('Finish: Import |' + csv_path + ' | Duration (h:mm:ss): ' + str(dur)[:-7])



def eex_ets_2db(xl_path:str, db_table:str):
	
	#status message for the beginning
	print('Start: Import from EEX-ETS.xlsx to sqlite.db')
	t0 		= dt.datetime.now()

	
	#set db_path
	db_path = os.path.join(aux.var_script_dir, aux.var_db_name)
	
	
	#idetify index of year in the filename
	yi 	= xl_path.index('-report-') + 8
	
	#initial import of the xl-file
	if int(xl_path[yi:yi+4]) >= 2017:
		DF_xl 		= pd.read_excel(xl_path, skiprows=5) 	#2017 and later data sets with 5 skipped rows
	
	else:
		DF_xl 		= pd.read_excel(xl_path, skiprows=2) 	#pre 2017 data sets with 2 skipped rows
	
	
	#optional renaming of the Time column for the 2016 data set	
	if  int(xl_path[yi:yi+4]) == 2016:
		DF_xl = DF_xl.rename(columns={	'Auction Time' : 'Time', 
										'Auction Name' : 'Country', 
										'Auction Price EUR/tCO2' : 'Auction Price €/tCO2'})
	
	DF_xl_DE 	= pd.DataFrame(DF_xl[DF_xl['Country']=='DE'])
	
	ts_unix0  	=  dt.datetime.strptime('1970-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
	
	
	#initial import of the xl-file
	if int(xl_path[yi:yi+4]) >= 2016:
		DF_xl_DE['dt_UTC'] = DF_xl_DE['Time']
	
	else:
		DF_xl_DE['dt_UTC'] 		= DF_xl_DE['Time'] - ts_unix0 + DF_xl_DE['Date']
	
	
	DF_xl_DE['ts_UTC'] 		= round((DF_xl_DE['dt_UTC'] - dt.datetime(1970,1,1)).dt.total_seconds())
	

	#create nested list from DataFrame
	nl = DF_xl_DE[['ts_UTC', 'Auction Price €/tCO2']].values.tolist()
	
	
	_SQL = """INSERT OR REPLACE INTO T_ets (PK_ts_UTC, ets) VALUES(?, ?)"""
	
	con=sq3.connect(db_path)#Connect to database
	con_c = con.cursor() #Create cursor-object
	
	con_c.executemany(_SQL, nl)

	con.commit()
	con.close()


	#status message for finish
	dur = dt.datetime.now() - t0
	print('Finish: Import |' + xl_path + ' | Duration (h:mm:ss): ' + str(dur)[:-7])
	
	

def T_exaa_clean():
	""" Function to just clean the T_exxa_da table. """
	
	_SQL = """ 	UPDATE T_exaa_da
				SET price = NULL
				WHERE price == 'n/e'
				
			"""

	con=sq3.connect(db_path)
	con.executescript(_SQL)
	con.commit()
	con.close()
	
	

## functions for caluculation secondary values


def calc_res_load():
	"""
	Funtion to calculate residual load based on T_loads and T_gen. No Parameters or Returns.
	"""

	#set data base path
	db_path = aux.var_script_dir + '\\' + aux.var_db_name

	#status message for the beginning
	start=dt.datetime.now()
	print('Start calculation of residual loads by direct db-querry.')


	#Connect to database
	con 	= sq3.connect(db_path)
	con_c 	= con.cursor()

	#Execute SQL-query for compouting the residual load
	con_c.execute('UPDATE T_loads SET res_load=load-(SELECT wind_offshore + wind_onshore + pv FROM T_gen WHERE T_loads.PK_ts_UTC = T_gen.PK_ts_UTC)')

	con.commit() 	#commit recent changes
	con.close() 	#Close database-Connection


	#status message for finish
	dur=dt.datetime.now()-start
	print('Finish residual load calcluation after '  + str(dur)[:-7] + '(h:mm:ss).')



def carbon_mix():
	"""
	Calculate the average carbon footprint for each hour of the year. No Parameters or Returns.
	"""



	#status message for the beginning
	start=dt.datetime.now()
	print('Start calculation of carbon footprint for power generation mix')


	#set data base path
	db_path = aux.var_script_dir + '\\' + aux.var_db_name


	#Connect to database
	con 	= sq3.connect(db_path)
	con_c 	= con.cursor()


	#do querry on the data base
	con_c.execute('SELECT name_db, emissions_fuel FROM T_gen_tps')
	qry 		= con_c.fetchall()


	#create dictionary for generation types carbon footprint
	gen_types  	= [i[0] for i in qry][:-2] 		#exclude pumped hydro storage
	gen_carbon 	= [i[1] for i in qry][:-2] 		#exclude pumped hydro storage
	carbon_dict = dict(zip(gen_types, gen_carbon))


	#SQL query for first and last year of the timeseries
	sql 		= """SELECT min(year), max(year)
					FROM T_gen
					JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC"""
	con_c.execute(sql)
	yr_min_max 	= con_c.fetchall()[0]


	for yr in range(yr_min_max[0],yr_min_max[1]+1):
		sql 	= """SELECT T_gen.PK_ts_UTC, {t}
					FROM T_gen
					JOIN T_ts ON T_gen.PK_ts_UTC=T_ts.PK_ts_UTC
					WHERE year='{y}'""".format(t = ', '.join(gen_types), y=str(yr))

		#create and process DataFrames
		DF_gen 	= pd.read_sql( sql, con) 		#read into DataFram for a certain year by query
		DF_gen 	= DF_gen.replace({	'N/A'	: 0,
									'-'		: 0,
									'nan'	: 0,
									'n/e' 	: 0}) 		#replace non-float entries by "0"


		DF_gen['carbon_mix'] = 0 					#insert new column "carbon mix"
		DF_gen_sum = DF_gen.iloc[:,1:].sum(axis=1) 	#generation sum per quater hour


		#calculate the wighted emissions per generation type and add them hourly
		for t in gen_types:
			DF_gen['carbon_mix'] += DF_gen[t] * carbon_dict[t] / DF_gen_sum


		#select carbon emissions and UTC timestamp and convert to nested list
		DF_carbon_mix 	= DF_gen[['carbon_mix','PK_ts_UTC']]
		nl_carbon_mix 	= DF_carbon_mix.values.tolist()


		#transfer calculated data to data base
		sql="""UPDATE T_gen SET carbon_mix=? WHERE PK_ts_UTC=?"""
		con_c.executemany(sql, nl_carbon_mix)
		con.commit()
		print('create carbon footprint for power generation mix in ' + str(yr))



def nrdg_model(year:int, market = 'EPEX', plot=False, markers=False, save_png=False, lang='en', title=True):
	"""
	Function for calculating modelled generation for a weighted power plant portfolio
	of non-renewable dispatchable generation (nrdg). nrdg_model is then used for computing 
	price dependet emission intensity (pdei) and write that results into the database (T_epex_da).

	Parameter:
	---------
	year:int
		single year for calculation
		
	market:str
		Power exchange market place: 'EPEX' | 'EXAA'

	plot:bool
		positinoal argument if the results should be plotted (curves + parameter table)
		
	markers:bool
		positinoal argument if plottint markers onto the lines.
		
	save_png=False
		string with path for saving figure to png file, default=False
	
	lang='en'
		language for the used labels for the plot:
			'en' | englisch
			'de' | german
	
	title=True
		argument for supress the plotting of the title if title!=True
	"""


	#refer to gpm_tb_aux, the function y = Lmin + ((Lmax - Lmin ) / (1 + np.exp(-k*(x-x0))))
	#def logistic_S(x, Lmin, Lmax, x0, k):
	log_S = aux.logistic_S


	#refer to generation types color in gpm_tb_aux
	tc = aux.var_types_col


	#path to data base
	db_path = os.path.join(aux.var_script_dir, aux.var_db_name)

	#Connect to database
	con 	= sq3.connect(db_path)
	con_c 	= con.cursor()
	

	market_tab 	= 'T_epex_da'
	view 		= 'V_epex_da'
	
	#build temporary database table for hourly aggregatet power prices
	if market == 'EXAA':
		market_tab 	= 'T_exaa_da'
		view 		= 'V_exaa_da'

	#list of non-renewbale dispatchable power plant types
	nrdg 	= aux.var_nrdg
	fparam 	= ['Lmin', 'Lmax', 'x0', 'k']


	#create empty DataFrame for generation types data (DF_gt)
	DF_gt 	= pd.DataFrame(data=None, index=nrdg, columns=fparam)


	#curve fitting parameters and querry carbon footprint of generation types
	for gt in nrdg:
		pef 	= dia.price_response(year, gt, market=market, plot=plot, curve=True, show=False) 	#calculate curve fit
		DF_gt.loc[gt,fparam] 	= pef['popt'] 								#function parameters
		DF_gt.loc[gt,'cod'] 	= pef['cod']								#coeff. of determ.


		#query generation type specific carbon emissions
		sql="""SELECT emissions_fuel
				FROM T_gen_tps
				WHERE name_db='{g}' """.format(g=gt)
		con_c.execute(sql) 									#execute SQL query
		cfp = con_c.fetchone() 								#fetch carbon foot print
		DF_gt.loc[gt,'cfp'] = cfp[0] 						#transfer fetch into DataFrame


	

	#SQL-statement to query epex prices for the requested year
	_SQL 	=	""" SELECT T_ts.PK_ts_UTC AS UTC, T_ts.dt_CET AS CET, price
					FROM {market_tab}
					JOIN T_ts ON {market_tab}.PK_ts_UTC=T_ts.PK_ts_UTC
					WHERE substr(dt_CET,1,4) == '{y}'""".format(y=year, market_tab=market_tab)
	
	#read SQL to time-series (ts) DataFrame
	DF_ts 				= pd.read_sql(_SQL, con)
	DF_ts['price'] 		= DF_ts['price'].astype(float)

	#last valid data point for power prices
	DF_tsmx = pd.read_sql(f""" SELECT  	max(PK_ts_UTC) 
								FROM    {view}
								WHERE   price != '-' 
								AND     substr(dt_CET,1,4) == '{year}' """,con)
	tsmx    = DF_tsmx.values.tolist()[0][0]
	
	
	#use queried DataFrame to also process incomplete years
	DF_qry          		= pd.DataFrame(DF_ts.query(f"UTC < {tsmx}"))

	
	#computing modelled generation amount depending on price using log_S
	for gt in nrdg:
		popt 		= DF_gt.loc[gt,fparam].tolist()
		DF_ts[gt]	= log_S( DF_ts['price'], *popt)


	#computing the hourly marginal price emissions based on modelled generation amounts
	V_emix 			= np.array(DF_gt['cfp'])
	A_gen 			= DF_ts.iloc[:,3:]
	DF_ts['mem'] 	= (A_gen * V_emix).sum(axis=1)/A_gen.sum(axis=1)
	DF_ts['mem'] 	= round(DF_ts['mem'], 2)


	#create nested list for batch transfer to data base
	nl = []
	for i in DF_ts.index.tolist():
		sublist =[float(DF_ts.loc[i,'mem']), int(DF_ts.loc[i,'UTC'])]
		nl.append(sublist)



	#transfer pdei data into data base
	_SQL 	= """ 	UPDATE {tab} 
					SET pdei=? 
					WHERE {tab}.PK_ts_UTC=? """.format(tab = dia.market_tabs[market])
	con_c.executemany(_SQL, nl)
	con.commit()

	
	#print status message
	print(f'update modeled generation for {year} @{market}')
	

	#even if this is not the diagramming module ;-) data can be used again for plotting
	if plot==True:
		#calculate price range
		price_rng = np.linspace(int(min(DF_ts['price'])),int(max(DF_ts['price'])),60)


		#create figure as subplot, where 2nd subplot is used for the parameter table
		fig = plt.figure(figsize=(6, 8))
		gs 	= gsp.GridSpec(2,1)


		#create tex string for the function
		f_tex 	= r'$g(p) = G_{min} + \frac{G_{max} - G_{min}}{(1+e^{-k*(p-p_0)})} $'

		#bilingual labeling for the whole figure
		lbls 	= { 'ax1_title' : { 'en' : 'price response curves in ',
									'de' : 'Preisreaktionskurven in '},
					'ax1_xlbl' 	: { 'en' : 'price [€/MWh]',
									'de' : 'Preis [€/MWh]'},
					'ax1_ylbl' 	: { 'en' : 'generation [MW]',
									'de' : 'Erzeugung [MW]'},
					'ax2_txt' 	: { 'en' : 'parameters for ' + f_tex,
									'de' : 'Parameter für ' + f_tex},
					'ax2_ylbl' 	: { 'en' : 'power plant types',
									'de' : 'Kraftwerkstypen'}}

		#list of markers
		mrkrs 	= ['^', 'o', 'v', '*', 's', 'x', 'd', '<' ]


		#create axis 1 for generation types and a
		ax1 	= plt.subplot(gs[0:1, :])

		#stepwise plotting the modelled generation functions and construct legend
		legend 	= []
		for i, gt in enumerate(nrdg):
			mk 		= mrkrs[i]
			cl 		= tc[gt]
			popt 	= DF_gt.loc[gt,fparam].tolist()
			ax1.plot(price_rng, log_S(price_rng, *popt), color=cl, marker=mk, markevery=5)
			legend.append(gt)

		#change legend entries if language is set to german
		if lang=='de':
			for i in range(0, len(legend)):
				legend[i] = aux.var_types_en2de[legend[i]]

		#axis 1 decoration
		ax1.grid(linestyle=':',linewidth=0.75)
		if title==True:
			ax1.set_title( lbls['ax1_title'][lang] + str(year))
		
		ax1.set_xlabel(lbls['ax1_xlbl'][lang])
		ax1.set_ylabel(lbls['ax1_ylbl'][lang])
		ax1.legend(legend)


		#construct the table elements
		tab_col_lbls = ['$G_{min}$', '$G_{max}$', '$p_{0}$', '$k$', '$R^²$'] 	#column labels
		tab_col_wdt  = [0.9/len(tab_col_lbls)]*len(tab_col_lbls) 							#column width
		tab_row_lbls = legend 													#row labels


		#create nested list for table cells content
		tab_cells_nl = []
		for i in range(0, len(DF_gt.index)):
			sublist 	= DF_gt.iloc[i,:-1].tolist()
			sublist[0] 	= '{:,.0f}'.format(sublist[0]) 	#Lmin
			sublist[1] 	= '{:,.0f}'.format(sublist[1]) 	#Lmax
			sublist[2] 	= '{:,.2f}'.format(sublist[2]) 	#x0
			sublist[3] 	= '{:,.3f}'.format(sublist[3]) 	#k
			tab_cells_nl.append(sublist)

		#switch decimal and thousand separators if lang=='de'
		if lang=='de':
			for i in range(0, len(tab_cells_nl)):
				for s in range(0, len(tab_cells_nl[0])):
					c = str(tab_cells_nl[i][s])
					tab_cells_nl[i][s] = c.replace(',', 'X').replace('.', ',').replace('X', '.')


		#plotting axis 2 which just contain the table
		ax2 	= plt.subplot(gs[1, :])
		ax2.axis('off')
		plt.text(0.5, 0.875, lbls['ax2_txt'][lang], ha='center', fontsize=10)
		ax2 	= plt.table(cellText=tab_cells_nl, colLabels=tab_col_lbls, \
							rowLabels=tab_row_lbls, loc='center', colWidths=tab_col_wdt, \
							bbox=[0.2, 0, 0.8, 0.825], edges='open')

		if save_png==False:
			plt.show()
		
		else:
			png_path = save_png
			
			#conditional removing older version of the png-file before save
			if os.path.isfile(png_path):
				os.remove(png_path)
			
			
			plt.savefig(png_path)
			
			plt.close()
			
			#print status message for save png file
			print('save figure to: ' + png_path)
		

	return(DF_ts)



def export2xl(years:list, feb29th=False):
	"""Function to export anual pirce-lists to MS Excel Spread sheets (each per year).

	Parameters
	----------

	years:list
		List of years to export as integers.

	feb29th=False
		Optional argument to choose wahtever the 29th on February should be exportet or not,
		usefull for uniform price vector lenghts. Default=False --> without 29th of Feb
	"""



	db_dir=aux.script_dir() 			#db directory from auxiliary script
	db_name=aux.var_db_name 			#db name from auxiliary script
	db_path=db_dir + '\\' + db_name 	#db_path


	xl_dir=db_dir + '\\xls'				#excel file directory from auxiliary script + subfolder
	xl_name='price_export.xlsx'			#excel directory from auxiliary script
	xl_path=xl_dir + '\\' + xl_name 	#excel path


	con=sq3.connect(db_path)				#Connect to database
	con_c = con.cursor() 				#Create cursor-object

	#DataFrame for metadata
	md_df=pd.DataFrame(index=['Jahre', '29th February' ,'db_path','timestamp'], columns=['Metadaten'])
	md_df.loc['Jahre', 'Metadaten']=str(years)[1:-1]
	md_df.loc['29th February', 'Metadaten']=feb29th
	md_df.loc['db_path', 'Metadaten']=db_path
	md_df.loc['timestamp', 'Metadaten']=str(dt.datetime.now())[:16]

	xlwrtr=pd.ExcelWriter(xl_path,engine='openpyxl') 	#create xlwriter-object

	md_df.to_excel(xlwrtr, sheet_name='info')			#export metadata-DataFrame
	xlwrtr.save()

	for yr in years:									#loop for pricedata-Export
		print('export data of ' + str(yr))

		if feb29th==False: 								#exception handling for 29th of Feb
			feb29_cnd=""" AND substr(dt_CET, 6, 5) IS NOT '02-29'  """
		else:
			feb29_cnd=''

		sql=""" SELECT price, T_ts.PK_ts_UTC, dt_UTC, dt_CET
				FROM T_epex_da
				JOIN T_ts ON T_epex_da.PK_ts_UTC=T_ts.PK_ts_UTC
				WHERE strftime('%Y', dt_CET)='{y}'
				{fb} """.format(y=yr, fb=feb29_cnd)

		df_yr=pd.read_sql(sql, con)
		df_yr.to_excel(xlwrtr, sheet_name=str(yr))

		xlwrtr.save()
		xlwrtr.close()

	print('finish export to ' + xl_path)
	

## functions for price forecasting 

def ndf_nhf(first:int, last:int):
	""" Calculate the normalised day factors (ndf) and normalised hourly factors (nhf) 
	for a given period (start to end) of years for epex-timeseries.

	Parameters
	----------
	first: int
		first year of the observation period

	p_end : int
		last year of the observation period.

	"""
	
	#Connect to database and create cursor object
	con 	= sq3.connect(db_path)
	con_c 	= con.cursor()
	
	
	#define SQL-Skript (several querry separeted by semicolons)
	_SQL 	= """
	--% drop temporary table if exists
	DROP TABLE IF EXISTS tT_epex_da;

	--% create temporary table if exists
	CREATE TEMPORARY TABLE 'tT_epex_da' AS
		SELECT T_epex_da.price, T_ts.* 
		FROM T_epex_da INNER 
		JOIN T_ts ON T_ts.PK_ts_UTC=T_epex_da.PK_ts_UTC;
		
		
	--% add two columns for week_ and day_keys
	ALTER TABLE tT_epex_da ADD COLUMN week_key INTEGER;
	ALTER TABLE tT_epex_da ADD COLUMN day_key INTEGER;
	
		--% compute week_ and day_keys
	UPDATE tT_epex_da
	SET week_key=round(PK_ts_UTC/(3600*24*7)-0.5), day_key=round(PK_ts_UTC/(3600*24)-0.5);
	
	
	--% create table for normalised daily factors if not exists and write or replace values
	CREATE TABLE IF NOT EXISTS "T_epex_ndf"("FK_season"	INTEGER, 
											"FK_wday" 	INTEGER,
											"value"		INTEGER,
											FOREIGN KEY("FK_season") REFERENCES "T_seasons"("PK_season"),
											FOREIGN KEY("FK_wday") REFERENCES "T_wdays"("PK_wday"),
											PRIMARY KEY("FK_season","FK_wday"));
	
	
	--% compute ndf based on weekly averages (using a subquerry)
	INSERT OR REPLACE INTO T_epex_ndf (	FK_season, 	
										FK_wday, 
										value)
			SELECT 	FK_season, 
					FK_wday, 
					avg(price)/avg(avg_wp)
			FROM tT_epex_da
			JOIN (	SELECT week_key AS 'wk', avg(price) as 'avg_wp' 
					FROM tT_epex_da 
					GROUP BY week_key) ON wk=tT_epex_da.week_key
			WHERE year BETWEEN {fy} AND {ly}
			GROUP BY FK_season, FK_wday;
			
	
	--% create table for normalised hourly factors if not exists and write or replace values
	DROP TABLE IF EXISTS T_epex_nhf;
	CREATE TABLE IF NOT EXISTS "T_epex_nhf"("FK_season"	INTEGER,
											"FK_wday"	INTEGER,
											"hour"	INTEGER,
											"value"	INTEGER,
											FOREIGN KEY("FK_season") REFERENCES "T_seasons"("PK_season"),
											FOREIGN KEY("FK_wday") REFERENCES "T_wdays"("PK_wday"),
											PRIMARY KEY("FK_season","FK_wday","hour"));
	
	--% compute nhf based on daily averages (using a subquerry)
	INSERT OR REPLACE INTO T_epex_nhf (	FK_season, 
										FK_wday, 
										hour, 
										value)
			SELECT 	FK_season, 
					FK_wday, 
					hour, 
					avg(price)/avg(avg_dp)
			FROM tT_epex_da
			JOIN (	SELECT day_key AS 'dk', avg(price) as 'avg_dp' 
					FROM tT_epex_da
					GROUP BY day_key ) ON dk=tT_epex_da.day_key
			WHERE year BETWEEN {fy} AND {ly}
			GROUP BY FK_season, FK_wday, hour;
			
			
	--% create and inster meta information to log
	DROP TABLE IF EXISTS log;
	CREATE TABLE IF NOT EXISTS "log" ("Name", "Value");
	INSERT OR REPLACE INTO log VALUES ("first year", {fy});
	INSERT OR REPLACE INTO log VALUES ("last year", {ly});
	INSERT OR REPLACE INTO log VALUES ('ndf_nhf_update', (SELECT strftime('%d.%m.%Y - %H:%M:%S','now'))) 
	""".format(fy=first, ly=last)
	
	
	#execute _SQL command as script
	con_c.executescript(_SQL)
	con_c.close
	
	
	#print status message
	print('Calculate ndf and nhf values for ' + str(first) + ' to ' + str(last) + ' and save to database.')